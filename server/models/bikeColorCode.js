import mongoose from 'mongoose';

/**
 * Bike exshowroom price Schema
 */

const Schema = mongoose.Schema;
const BikeColorCodeSchema = new mongoose.Schema({
    CompanyID: { type: String, default: null },
    CompanyName: { type: String, default: null },
    ModelCode : { type: String, default: null },
    ModelName : { type: String, default: null },
    state : { type: String, default: null },
    ExshowRoomPrice : { type: String, default: null },
    Type : { type: String, default: null },
    BookingAmount : { type: String, default: 5000 },
    TankImage : { type: String, default: null }
});


export default mongoose.model('BikeColorCode', BikeColorCodeSchema);
