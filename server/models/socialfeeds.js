import mongoose from 'mongoose'

var socialfeedsschema = new mongoose.Schema({

	HashTag: { type: String },

	ListOfTags: [

	],
	UserName: {
		type: String,

	},
	UserId: {
		type: String,

	}, Metatag: [


	],
	Text:
	{
		type: String,
	},

	SocialFeedImage:
	{
		type: String,
	},

	Likes:
	{
		type: Number,


	},

	ReTweets:
	{
		type: Number,


	},
	SocialLinks: {
		type: String
	},
	SocialSource: {
		type: String
	},
	isApproved: {
		type: Boolean,
		default: true
	},
	isBrandPost: {
		type: Boolean,
		default: true
	},
	moderationReference: { type: String }
})

export default mongoose.model('socialfeeds', socialfeedsschema)