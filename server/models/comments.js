import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

import replies from './replies';
import User from './user';

/*** 
comments Schema 
***/

const moment = require('moment');
const Schema = mongoose.Schema;
const commentSchema = new mongoose.Schema({
  Replyid: [
    { type: mongoose.Schema.Types.ObjectId, ref: "replies", default: null }
  ],
  commentBody: { type: String, default: null },
  date: { type: Date, default: Date.now },
  userdetailscomments: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    default: null
  },
  previousUserId: { type: Number, default: null },
  perviousParentId: { type: Number, default: null },
   locality: {
        country: { type: String, default: null },
        language: { type: String, default: null }

    },
});

export default mongoose.model('comments', commentSchema);
