import mongoose from 'mongoose';

const Schema = mongoose.Schema;
/**
 * AppConfig Schema
 */
const dynamicFormSchema = new mongoose.Schema({
    type: { type: Schema.Types.Mixed },
    formId: { type: String, required: true, unique: true },
    formConfig:{ type: Schema.Types.Mixed },
    formData: [{ type: Schema.Types.Mixed }],
});

export default mongoose.model('DynamicForm', dynamicFormSchema);
