import mongoose from 'mongoose'

const tourIndonesiaSchema = new mongoose.Schema({
    formName: { type: String, default: null, trim: true },
    orderId: { type: String, trim: true, unique: true },
    regType: { type: String, default: null, trim: true },
    fname: { type: String, default: null, trim: true },
    lname: { type: String, default: null, trim: true },
    email: { type: String, default: null, trim: true },
    mobileNumber: { type: String, default: null, trim: true },
    address: { type: String, default: null, trim: true },
    country: { type: String, default: null, trim: true },
    state: { type: String, default: null, trim: true },
    city: { type: String, default: null, trim: true },
    postalCode: { type: String, default: null, trim: true },
    gender: { type: String, default: null, trim: true },
    tShirtSize: { type: String, default: null, trim: true },
    ridingWith: { type: String, default: null, trim: true },
    motorcycleOwned: { type: String, default: null, trim: true },
    manufacturedYear: { type: String, default: null, trim: true },
    registrationNumber: { type: String, default: null, trim: true },
    partfname: { type: String, default: null, trim: true },
    partlname: { type: String, default: null, trim: true },
    partemail: { type: String, default: null, trim: true },
    partmobile: { type: String, default: null, trim: true },
    partOrderId: { type: String, default: null, trim: true },
    partgender: { type: String, default: null, trim: true },
    partShirtSize: { type: String, default: null, trim: true },
    partdob: { type: String, default: null, trim: true },
    locality: {
        language: { type: String, default: null, trim: true },
        country: { type: String, default: null, trim: true }
    },
    tracking_id: { type: String, trim: true },
    bank_ref_no: { type: String, trim: true },
    order_status: { type: String, trim: true },
    eventPayment: { type: String, trim: true },
    payment_mode: { type: String, trim: true },
    paymentObject: {}
});

export default mongoose.model('tourIndonesia', tourIndonesiaSchema);