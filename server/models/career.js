import mongoose from 'mongoose';

var careerSchema = new mongoose.Schema({
      locality: {
        country: { type: String, default: null },
        language: { type: String, default: null }
    },   
    fname: { type: String, default: null },
    lname: { type: String, default: null },
    email: { type: String, default: null },
    phone: { type: String, default: null },
    phone: { type: String, default: null },
    age: { type: String, default: null },
    yearOfExperience: { type: String, default: null },
    highestQualification: { type: String, default: null },
    institute: { type: String, default: null },
    secondaryQualification: { type: String, default: null },
    currentDesignation: { type: String, default: null },
    keySkills: [{ type: String, default: null }],
    linkedIn: { type: String, default: null },
    industry: { type: String, default: null },
    functionApplyingFor: { type: String, default: null },
    cvPath: {type: String, default:null}

});

export default mongoose.model('CareerSchema', careerSchema);

