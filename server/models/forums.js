import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import comments from './comments';
import replies from './replies';
import user from './user';

/**
 * Forums Schema
 */

const Schema = mongoose.Schema;

const ForumSchema = new mongoose.Schema({
  locality: {
        country: { type: String, default: null },
        language: { type: String, default: null }

    },
  prevForumId: { type: Number, default: null },
  forumTitle: { type: String, default: null },
  forumBody: { type: String, default: null },
  categoryName: { type: String, default: null },
  postedOn: { type: Date, default: null },
  postedBy: { type: Schema.Types.ObjectId, ref: "User", default: null },
  postedByPrevUserId: { type: Number, default: null },
  forumUrl: { type: String, default: null },
  views: { type: Number, default: 0 },
  comment: [
    { type: mongoose.Schema.Types.ObjectId, ref: "comments", default: null }
  ],
  prevCommId: { type: Number, default: null },
  timeAgo: { type: String, default: null },
  forumAuthor: { type: String, default: null }
});


/**
 * Statics
 */



export default mongoose.model('Forum', ForumSchema);
