import mongoose from 'mongoose'

const tor = new mongoose.Schema({
    formName: { type: String, trim: true },
    fname: { type: String, trim: true },
    partnerfname: { type: String, trim: true },
    lname: { type: String, trim: true },
    partnerlname: { type: String, trim: true },
    email: { type: String, trim: true },
    partneremail: { type: String, trim: true },
    mobileNumber: { type: String, trim: true },
    partnermobile: { type: String, trim: true },
    gender: { type: String, trim: true },
    partnergender: { type: String, trim: true },
    rider_dob: { type: String, trim: true },
    partnerdob: { type: String, trim: true },
    address: { type: String, trim: true },
    country: { type: String, trim: true },
    state: { type: String, trim: true },
    city: { type: String, trim: true },
    partnercityname: { type: String, trim: true },
    postalCode: { type: String, trim: true },
    emergencyFname: { type: String, trim: true },
    partemergencyFname: { type: String, trim: true },
    emergencyMobileNumber: { type: String, trim: true },
    partemergencyMobileNumber: { type: String, trim: true },
    emergencyEmail: { type: String, trim: true },
    partemergencyEmail: { type: String, trim: true },
    preferedRiderNumber: { type: String, trim: true },
    partpreferedRiderNumber: { type: String, trim: true },
    motorcycleOwned: { type: String, trim: true },
    partmotorcycleOwned: { type: String, trim: true },
    manufacturedYear: { type: String, trim: true },
    partmanufacturedYear: { type: String, trim: true },
    registrationNumber: { type: String, trim: true },
    partregistrationNumber: { type: String, trim: true },
    modifactions: { type: String, trim: true },
    partmodifactions: { type: String, trim: true },
    distanceRiddenInKms: { type: String, trim: true },
    partdistanceRiddenInKms: { type: String, trim: true },
    ridingExperienceDetails: { type: String, trim: true },
    partridingExperienceDetails: { type: String, trim: true },
    order_id: { type: String, trim: true },
    partorder_id: { type: String, trim: true },
    registeringAs: { type: String, default: "Single", trim: true },
    eventPaymentAmount: { type: String, trim: true },
    paymnetStatus: { type: String, default: "Failed", trim: true },
    paymentObject: {}
});

export default mongoose.model('tor', tor);


