import mongoose from 'mongoose';

var twitterFeedSchema = new mongoose.Schema({
	HashTag: {

		type: String,
		required: true,
		minlength: 1,
	},

	ListOfTags: [{
		type: String,
	}],

	UserName: {
		type: String,

	},
	SocialFeedImage:
	{
		type: String,


	},
	Text:
	{
		type: String,
	},
	Likes:
	{
		type: Number,


	},

	Retweets:
	{
		type: Number,


	},


	SocialLink: {
		type: String
	},
	isApproved: {
		type: Boolean,
		default: true
	},
	moderationReference: { type: String }
});

export default mongoose.model('Twitterfeed', twitterFeedSchema); 
