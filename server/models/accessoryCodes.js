import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const AccessoryCodeSchema = new mongoose.Schema({
  PriceEffectiveFrom: { type: Date, default: null },
  CurrentMRP: { type: String, default: null },
  GMACode: { type: String, default: null },
  colour_id: { type: String, default: null },
  product_id: { type: String, default: null },
  title: { type: String, default: null },
  image: { type: String, default: null },
  default_price: { type: Number, default: 1500 },
  category: { type: String, default: null }
});


export default mongoose.model("AccessoryCode", AccessoryCodeSchema);
