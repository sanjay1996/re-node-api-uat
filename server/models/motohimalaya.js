import mongoose from "mongoose";

// const motohimalayaSchema = new mongoose.Schema({
//     formName: { type: String, default: null },
//     formId: { type: String, default: null },
//     formConfig: {
//         formThreshhold: { type: String, default: null },
//         formLocale: {
//             language: { type: String, default: null },
//             country: { type: String, default: null },
//         }
//     },
//     formData: [{
//         formId: { type: String, default: null },
//         order_id: { type: String, default: null },
//         regType: { type: String, default: null },
//         fname: { type: String, default: null },
//         lname: { type: String, default: null },
//         email: { type: String, default: null },
//         mobileNumber: { type: String, default: null },
//         countryName: { type: String, default: null },
//         gender: { type: String, default: null },
//         dob: { type: String, default: null },
//         emergencyContactFirstName: { type: String, default: null },
//         emergencyContactMobileNumber: { type: String, default: null },
//         emergencyContactEmail: { type: String, default: null },
//         facebook: { type: String, default: null },
//         twitterHandle: { type: String, default: null },
//         instagramHandle: { type: String, default: null },
//         distanceTravelled: { type: String, default: null },
//         clubs: { type: String, default: null },
//         expectFromRide: { type: String, default: null },
//         uploadDocLater: { type: String, default: null },
//         locality: {
//             language: { type: String, default: null },
//             country: { type: String, default: null },
//         },
//         my_files: [],
//         paymentObject: {}
//     }]
// });

const motohimalayaSchema = new mongoose.Schema({
  formName: { type: String, default: null },
  order_id: { type: String, default: null },
  regType: { type: String, default: null },
  fname: { type: String, default: null },
  lname: { type: String, default: null },
  email: { type: String, default: null },
  mobileNumber: { type: String, default: null },
  countryName: { type: String, default: null },
  gender: { type: String, default: null },
  dob: { type: String, default: null },
  emergencyContactFirstName: { type: String, default: null },
  emergencyContactMobileNumber: { type: String, default: null },
  emergencyContactEmail: { type: String, default: null },
  facebook: { type: String, default: null },
  twitterHandle: { type: String, default: null },
  instagramHandle: { type: String, default: null },
  distanceTravelled: { type: String, default: null },
  clubs: { type: String, default: null },
  expectFromRide: { type: String, default: null },
  uploadDocLater: { type: String, default: null },
  tracking_id: { type: String, trim: true },
  bank_ref_no: { type: String, trim: true },
  order_status: { type: String, trim: true },
  eventPayment: { type: String, trim: true },
  payment_mode: { type: String, trim: true },
  moreinfo:{type: String, trim: true},
  successUrl: { type: String, default: null },
  failureUrl: { type: String, default: null },
  thankyouUrl: { type: String, default: null },
  locality: {
    language: { type: String, default: null },
    country: { type: String, default: null }
  },
  my_files: [],
  paymentObject: {}
});

export default mongoose.model("motohimalaya", motohimalayaSchema);
