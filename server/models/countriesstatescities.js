import mongoose from 'mongoose';

var Schema = mongoose.Schema;

var countriesstatescities = new Schema({
    countryCode: { type: String, default: null, trim: true },
    countryName: { type: String, default: null, trim: true },
    oldCountryId: { type: String, default: null, trim: true },
    language: { type: String, default: null, trim: true },
    currency: { type: String, default: null, trim: true },
    states: [{}]
});

export default mongoose.model("countriesstatescities", countriesstatescities);