import mongoose from 'mongoose';

var gmaFormSchema = new mongoose.Schema({
    firstName: {type: String, default:null},
    email : {type: String, default:null},
    mobile: {type: String, default:null},
    city: {type: String, default:null},
    pinCode: {type: String, default:null},
    productSKU : {type: String, default:null},
    enquiry: {type: String, default:null}
})



export default mongoose.model('gmaForm',gmaFormSchema);
