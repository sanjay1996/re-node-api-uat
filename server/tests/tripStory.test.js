/* eslint-disable */
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai from 'chai';
import { expect } from 'chai';
import app from '../../index';

chai.config.includeStack = true;

describe('## Trip Story APIs', () => {
    let story = {
        storyTitle: "Second ride ever to goa for a weekend !",
        storyBody: "It was a regular friday, just like all other regular 9 to 5 corporate workers i was in a day shift in my office but for me the day was full with excitement and thrilling feeling because after the work i was ready to go to the paradise GOA to meet my friend i have never met before personally and to explore the places around.",
        postedOn: "2013-08-31T18:30:00Z",
        postedBy: "5a585bc036d5800618f3b67c",
        tripImages: ["http://stat.overdrive.in/wp-content/uploads/2016/12/2016-Royal-Enfield-Himalayan-first-ride-review.jpg"],
        storyUrl: "http://google.com",
        comments: [{
            commentId: "1234654654",
            commentedOn: "2013-08-31T18:30:00Z",
            commentedBy: "5a585bc036d5800618f3b67c",
            commentBody: "hi",
            replies: [{
                repliedOn: "2013-08-31T18:30:00Z",
                repliedBy: "5a585bc036d5800618f3b67c",
                replyBody: "hey",
            }],
        }]
    };

    let story1 = {
        storyTitle: "Second ride ever to goa for a weekend !",
        storyBody: "It was a regular friday, just like all other regular 9 to 5 corporate workers i was in a day shift in my office but for me the day was full with excitement and thrilling feeling because after the work i was ready to go to the paradise GOA to meet my friend i have never met before personally and to explore the places around.",
        postedOn: "2013-08-31T18:30:00Z",
        postedBy: "5a585bc036d5800618f3b67c",
    };

    let story2 = {
        storyTitle: "Third ride ever to goa for a weekend !",
        storyBody: "It was a regular friday, just like all other regular 9 to 5 corporate workers i was in a day shift in my office but for me the day was full with excitement and thrilling feeling because after the work i was ready to go to the paradise GOA to meet my friend i have never met before personally and to explore the places around.",
        postedOn: "2014-08-31T18:30:00Z",
        postedBy: "5a585bc036d5800618f3b67c",
    };

    describe('# POST /api/stories/create', (done) => {
        it('should create a new story', (done) => {
            request(app)
                .post('/api/stories/create')
                .send(story)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.success).to.equal(true);
                    expect(res.body.data).to.have.all.keys('story');
                    done();
                });
        })
    });

    describe('# POST /api/stories/create', (done) => {
        it('should create a new story2', (done) => {
            request(app)
                .post('/api/stories/create')
                .send(story1)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.success).to.equal(true);
                    expect(res.body.data).to.have.all.keys('story');
                    done();
                });
        })
    });

    describe('# POST /api/stories/create', (done) => {
        it('should create a new story3', (done) => {
            request(app)
                .post('/api/stories/create')
                .send(story2)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.success).to.equal(true);
                    expect(res.body.data).to.have.all.keys('story');
                    done();
                });
        })
    });

    let storyId = {
        _id: "5a5c7bc7d87c0c2a4069b397"
    }
    describe('# POST /api/stories', () => {
        it('should get the story details', (done) => {
            request(app)
                .post('/api/stories')
                .send(storyId)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.success).to.equal(true);
                    expect(res.body.message).to.equal("story retrieved successfully");
                    done();
                });
        });
    });

    let constraints = {
        limit: "1",
        skip: "2",
    };

    describe('# POST /api/stories/all', (done) => {
        it('should return list of stories', (done) => {
            request(app)
                .post('/api/stories/all')
                .send(constraints)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.success).to.equal(true);
                    expect(res.body.message).to.equal('stories retrieved successfully');
                    done();
                });
        })
    });
});
