import Twit from "twitter";
import util from "util";
import request from "request";
import sortJsonArray from "sort-json-array";
import express from "express";
import axios from "axios";
import CircularJSON from "circular-json-es6";
import Socialfeeds from "../models/socialfeeds";
var asyncc = require("asyncawait/async");
var awaitt = require("asyncawait/await");
import config from "../../config/env";

function twitterdata(req, res, next) {
  var arr = [];
  var hashtag = req.body.hashtag;

  Socialfeeds.find({ ListOfTags: { $regex: `^${req.body.hashtag}$`, $options: "i" } })
    .limit(parseInt(req.body.count))
    .then(docs => {
      // var foo = asyncc(function () {

      // for (const item of docs) {
      // if (item.ListOfTags != null) {
      // for (var i = 0; i < item.ListOfTags.length; i++) {

      //   if (item.ListOfTags[i].toLowerCase() == hashtag.toLowerCase()){

      //       arr.push(awaitt(savingFunction(item)))
      //      }
      //     }
      //    }
      //  }

      res.send(docs);
      //  })
      //  foo();
    })
    .catch(err => {
      res.status(500).send({ msg: "error" });
    });
}

var savingFunction = item => {
  return new Promise((resolve, reject) => {
    resolve(item);
  });
};

function dealerReviews(req, res, next) {
  if (req.body.obj.googlePlaceId == "" || req.body.obj.googlePlaceId == undefined) {
    res.send("Googlepalce id not found or something went wrong");
  } else {
    var placeid = req.body.obj.googlePlaceId;
    var googlekey = config.googlereviewkey;
    var verifyUrl = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeid}&key=${googlekey}`;
    request(verifyUrl, (err, response, body) => {
      var stringbody = JSON.parse(body);
      if (stringbody.result != undefined) {
        if (stringbody.result.reviews) {
          var avReview = 0;
          for (var i = 0; i < stringbody.result.reviews.length; i++) {
            avReview = avReview + stringbody.result.reviews[i].rating;
          }
          var finalReview = avReview / stringbody.result.reviews.length;
          var reviewPercentage = finalReview / 5;
          var finalPercentage = reviewPercentage * 100;
          var location = stringbody.result.url;
          var obj = {
            ratingPercentage: finalPercentage,
            rating: parseFloat(finalReview).toFixed(2),
            directionUrl: location
          };
          res.send(obj);
        } else {
          res.send({ directionUrl: stringbody.result.url });
        }
      } else {
        res.status(500);
        res.send("Googlepalce id not found or something went wrong");
      }
    });
  }
}

function fetch(req, res, next) {
  var googlekey = config.googlereviewkey;
  var placeId = req.body.obj.entityId;
  var limit = parseInt(req.body.obj.resultLimit);
  var verifyUrl = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeId}&key=${googlekey}`;
  var arr = [];
  try {
    request(verifyUrl, (err, response, body) => {
      body = JSON.parse(body);
      if (body.result != undefined) {
        if (body.result.reviews) {
          var Address = body.result.formatted_address;
          var phonenumber = body.result.formatted_phone_number;
          var name = body.result.name;
          var location = body.result.url;
          var ratings = body.result.rating;
          var website = body.result.website;
          var unsortedArray = body.result.reviews;
          var sortedArray = sortJsonArray(unsortedArray, "rating", "des");
          if (limit <= sortedArray.length) {
            limit = limit;
          } else {
            limit = sortedArray.length;
          }
          for (var i = 0; i < limit; i++) {
            var reviewername = sortedArray[i].author_name;
            var authorurl = sortedArray[i].author_url;
            var profilephoto = sortedArray[i].profile_photo_url;
            var authorating = sortedArray[i].rating;
            var text = sortedArray[i].text;
            var timedescription = sortedArray[i].relative_time_description;
            var obj = {
              reviewText: text,
              reviewRating: authorating,
              reviewByUser: {
                firstName: reviewername,
                address: { addressAsText: Address },
                profilePicture: { srcPath: profilephoto }
              },
              reviewDateText: timedescription,
              reviewRatingPercentage: authorating * 20
            };
            arr.push(obj);
          }
          res.send({ reviewList: arr });
        } else {
          res.send({ dealerUrl: body.result.url });
        }
      } else {
        res.status(500);
        res.send("Googlepalce id not found or something went wrong");
      }
    });
  } catch (ex) {
    res.status(500);
    res.send("Googlepalce id not found or something went wrong");
  }
}

function googleplusdata(req, res, next) {
  const apkey = "AIzaSyA3LipyK7I_HaCEgW4jZl59dTlY0-gJvTA";
  var GUrl = `https://www.googleapis.com/plus/v1/activities?query=Royal+Enfield&maxResults=20&orderBy=recent&key=${apkey}`;
  axios
    .get(GUrl)
    .then(response => {
      var clone = CircularJSON.parse(CircularJSON.stringify(response));
      res.send(clone.data);
    })
    .catch(e => {
      console.log(e);
    });
}

function getWeather(req, res, next) {
  //console.log(req.query);
  var lat = req.query.lat;
  var lng = req.query.lng;
  axios
    .get(
      "https://api.openweathermap.org/data/2.5/weather?lat=" +
        lat +
        "&lon=" +
        lng +
        "&appid=cbf1b58e3415fa3b236b9e52d76e1bc4"
    )
    .then(data => {
      res.send(data.data);
    })
    .catch(err => {
      //console.log(err)
    });
}

export default {
  twitterdata,
  fetch,
  googleplusdata,
  dealerReviews,
  getWeather
};
