import solrClient from 'solr-client';
import config from '../../config/env';
import request from 'request';
import Forums from '../models/forums';

var indexer = solrClient.createClient({
    host: config.solr,
    port: 8983,
    path: "/solr",
    core: "new_core",
    solrVersion: 721
});

function searchSolr(req, res, next) {
    var query2 = indexer.createQuery()
        .q(`(entity : motorcycles AND (title : ${req.query.query})) OR  (entity : user AND (title : ${req.query.query})) OR (entity : trip-story AND (summary : ${req.query.query} OR title : ${req.query.query})) OR (entity : news AND (summary : ${req.query.query} OR title : ${req.query.query})) OR (entity : forum-topic AND (summary : ${req.query.query} OR title : ${req.query.query})) OR (entity : dealers AND (summary : ${req.query.query} OR title : ${req.query.query})) OR (entity : user-ride AND (rideDescription : ${req.query.query} OR title : ${req.query.query})) OR (entity : ride-out AND (rideDescription : ${req.query.query} OR title : ${req.query.query})) OR (entity : marquee-rides AND (rideDescription : ${req.query.query} OR title : ${req.query.query})) `)
        .matchFilter('country', req.headers['x-custom-country'])
        .matchFilter('language', req.headers['x-custom-language'])
        .start(0)
        .rows(10000)
        .fl('name,url,title,body,summary,entity,rideDescription,description,thumbnailImagePath');
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            res.send(obj.response.docs);
        }
    });
}

function searchSolrCategory(req, res, next) {
    var query2;
    if (req.body.category == 'rides') {
        query2 = indexer.createQuery()
            .q(`(entity : user-ride AND (rideDescription : ${req.body.query} OR title : ${req.body.query})) OR (entity : ride-out AND (rideDescription : ${req.body.query} OR title : ${req.body.query})) OR (entity : marquee-rides AND (rideDescription : ${req.body.query} OR title : ${req.body.query})) `)
            .matchFilter('country', req.headers['x-custom-country'])
            .matchFilter('language', req.headers['x-custom-language'])
            .start(0)
            .rows(10000)
            .fl('url,title,body,summary,entity,rideDescription,description,thumbnailImagePath');
    } else {
        query2 = indexer.createQuery()
            .q(`title : ${req.body.query} OR summary:${req.body.query} OR name : ${req.body.query} `)
            .matchFilter('entity', req.body.category)
            .matchFilter('country', req.headers['x-custom-country'])
            .matchFilter('language', req.headers['x-custom-language'])
            .start(0)
            .rows(10000)
            .fl('name,url,title,body,summary,entity,rideDescription,thumbnailImagePath');
    }
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            res.send(obj.response.docs);
        }
    });

}



function sendSms(req, res, next) {
    // var phoneNo = 9599914762;
    var verifyUrl = `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${req.body.phoneNo}&sender=RECARE&msgtext=Dear user your one time password is 123456. Please don't share this with any one. Thanks RE team.&intflag=fasle`

    request(verifyUrl, (err, response, body) => {
        console.log(err)
        var rep = {
            body: body,
            abc: req.headers['x-custom-country']
        }
        res.send(rep);
    });
}

function searchRide(req, res, next) {
    var resultFromSolr = [];
    var queryForSearch = "";
    if (req.body.placeName === '') {
        queryForSearch = 'startDate:' + req.body.date
    } else if (req.body.date === "") {
        queryForSearch = 'endPointPlaceName:' + req.body.placeName;
    } else if (req.body.placeName !== '' && req.body.date !== '') {
        queryForSearch = 'startDate:' + req.body.date + ' OR startPointPlaceName:' + req.body.placeName;
    }
    var query2 = indexer.createQuery()
        .q(queryForSearch)
        .matchFilter('entity', req.body.category)
        .matchFilter('country', req.headers['x-custom-country'])
        .matchFilter('language', req.headers['x-custom-language'])
        .start(0)
        .rows(10000)
        .fl('duration,startPointPlaceName,endPointPlaceName,title,author,thumbnailImagePath,rideDescription,url,startDate')
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            for (var i = 0; i < obj.response.docs.length; i++) {
                resultFromSolr.push({ durationInDays: obj.response.docs[i].duration, startPoint: { name: obj.response.docs[i].startPointPlaceName }, endPoint: { name: obj.response.docs[i].endPointPlaceName }, startDate: obj.response.docs[i].startDate, rideusername: obj.response.docs[i].author[0], rideName: obj.response.docs[i].title[0], url: obj.response.docs[i].url[0], rideDetails: obj.response.docs[i].rideDescription[0], rideImage: obj.response.docs[i].thumbnailImagePath[0] })
            }
            res.send({ object: resultFromSolr });
        }
    });
}



function tripStorySearchCount(req, res, next) {
    var categoryObj = [];
    var resultFromSolr = [];
    var query2 = indexer.createQuery()
        .q('entity:trip-story')
        .matchFilter('country', req.headers['x-custom-country'])
        .matchFilter('language', req.headers['x-custom-language'])
        .start(0)
        .rows(100)
        .fl('category,title,author,thumbnailImagePath,summary,url')
        .facet({ field: 'cat' })

    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            for (var i = 0; i < obj.facet_counts.facet_fields.cat.length; i++) {
                categoryObj.push({
                    name: obj.facet_counts.facet_fields.cat[i],
                    value: obj.facet_counts.facet_fields.cat[i += 1]
                })
            }
            res.send({ categories: categoryObj });
        }
    });
}


function tripStorySearch(req, res, next) {
    var resultFromSolr = [];
    var dataSent = "";
    if (req.body.category) {
        if (req.body.category == "*:*") {
            dataSent = "*:*"
        } else {
            dataSent = 'category:' + req.body.category;
        }
    } else if (req.body.title) {
        dataSent = `title:${req.body.title} OR author:${req.body.title} OR summary : ${req.body.title}`;
    }

    var query2 = indexer.createQuery()
        .q(dataSent)
        .matchFilter('entity', 'trip-story')
        .matchFilter('country', req.headers['x-custom-country'])
        .matchFilter('language', req.headers['x-custom-language'])
        .start(0)
        .rows(req.body.count ? req.body.count : 100)
        .sort({ dateSort: 'desc' })
        .fl('category,title,author,thumbnailImagePath,summary,url')
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            res.send({ objects: obj.response.docs, count: obj.response.numFound });
        }
    });
}


function tripStorySearchOnClick(req, res, next) {
    var i = 0;
    var resultFromSolr = [];
    var query2 = indexer.createQuery()
        .q(`category:${req.body.category}`)
        .matchFilter('entity', 'trip-story')
        .matchFilter('country', req.headers['x-custom-country'])
        .matchFilter('language', req.headers['x-custom-language'])
        .start(0)
        .rows(req.body.count)
        .sort({ dateSort: 'desc' })
        .fl('category,title,summary,thumbnailImagePath,author,url');
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            res.send({ objects: obj.response.docs, count: obj.response.numFound });
        }
    });
}

function forumTopicSearchOnClick(req, res, next) {
    var i = 0;
    var categoryObj = [];
    var resultFromSolr = [];

    var query2 = indexer.createQuery()
        .q('*:*')
        .matchFilter('entity', 'forum-topic')
        .matchFilter('country', req.headers['x-custom-country'])
        .matchFilter('language', req.headers['x-custom-language'])
        .facet({ field: 'cat' })
        .start(0)
        .rows(100)
        .fl('category,views,summary,author,url,postedOn,replies')
        .facet({ field: 'cat' });
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            for (var i = 0; i < obj.facet_counts.facet_fields.cat.length; i++) {
                categoryObj.push({
                    name: obj.facet_counts.facet_fields.cat[i],
                    value: obj.facet_counts.facet_fields.cat[i += 1]
                })
            }
            res.send({ categories: categoryObj });
        }
    });
}


function forumTopicSearchOnEnter(req, res, next) {
    var i = 0;
    var resultFromSolr = [];
    var dataSent = '';
    var obj = {};
    if (req.body.category == "*:*") {
        obj = obj
    }


    else if (req.body.category) {
        obj = {
            categoryName: { $regex: new RegExp(req.body.category), $options: 'i' }, locality: {
                country: req.headers['x-custom-country'],
                language: req.headers['x-custom-language']
            }
        }
    }
    else if (req.body.title) {
        obj = {
            $or: [{ forumTitle: { $regex: new RegExp(req.body.title), $options: 'i' } }, { forumBody: { $regex: new RegExp(req.body.title), $options: 'i' } }, { forumAuthor: { $regex: new RegExp(req.body.title), $options: 'i' } }], 'locality.country': req.headers['x-custom-country'],
            'locality.language': req.headers['x-custom-language']

        }
    }
    Forums.find(obj).populate({
        path: 'postedBy comment'

    }).sort({ postedOn: -1 }).limit(100).then((docs) => {
        docs.forEach(element => {
            resultFromSolr.push({ categoryUnparsed: element.categoryName.replace(/-/g, ' ').charAt(0).toUpperCase() + element.categoryName.replace(/-/g, ' ').slice(1), author: element.postedBy.fname ? element.postedBy.fname : '', userUrl: element.postedBy.userUrl, title: element.forumTitle, category: element.categoryName, postedOn: element.postedOn, summary: element.forumBody, views: element.views, replyCount: element.comment.length ? element.comment.length : 0, url: element.forumUrl })
        })
        res.send({ objects: resultFromSolr });
    }).catch((err) => {
        console.log(err);
        res.status(500).send("Error");
    });
}


function forumTopicSearch(req, res, next) {
    if (req.body.count == "true") {
        var i = 0;
        var resultFromSolr = [];

        var dataSent = '';
        var obj = {};
        if (req.body.category == "*:*") {
            obj = obj
        }


        else if (req.body.category) {
            obj = {
                categoryName: req.body.category,
                'locality.country': req.headers['x-custom-country'],
                'locality.language': req.headers['x-custom-language']
            }
        }
        else if (req.body.title) {
            obj = {
                $or: [{ forumTitle: { $regex: new RegExp(req.body.title), $options: 'i' } }, { forumBody: { $regex: new RegExp(req.body.title), $options: 'i' } }, { forumAuthor: { $regex: new RegExp(req.body.title), $options: 'i' } }],
                'locality.country': req.headers['x-custom-country'],
                'locality.language': req.headers['x-custom-language']

            }
        }
        Forums.find(obj).count().then((count) => {
            res.send({ count: count })

        }).catch((err) => {
            console.log(err);
            res.status(500).send("Error");
        });

    } else {

        var i = 0;
        var resultFromSolr = [];
        var skip;
        var limit;

        if (typeof (req.body.pageNumber) == "undefined") {
            skip = 0;
            limit = req.body.limit;
        }
        else {
            skip = (parseInt(req.body.pageNumber) - 1) * 10
            limit = req.body.limit
        }

        var dataSent = '';
        var obj = {};
        if (req.body.category == "*:*") {
            obj = obj
        }


        else if (req.body.category) {
            obj = {
                categoryName: { $regex: new RegExp(req.body.category), $options: 'i' }, 'locality.country': req.headers['x-custom-country'], 'locality.language': req.headers['x-custom-language']
            }
        }
        else if (req.body.title) {
            obj = {
                $or: [{ forumTitle: { $regex: new RegExp(req.body.title), $options: 'i' } }, { forumBody: { $regex: new RegExp(req.body.title), $options: 'i' } }, { forumAuthor: { $regex: new RegExp(req.body.title), $options: 'i' } }], locality: {
                    country: req.headers['x-custom-country'],
                    language: req.headers['x-custom-language']
                }
            }
        }
        Forums.find(obj).populate({
            path: 'postedBy comment'

        }).sort({ postedOn: -1 }).skip(parseInt(skip)).limit(parseInt(limit)).then((docs) => {
            docs.forEach(element => {
                resultFromSolr.push({ categoryUnparsed: element.categoryName.replace(/-/g, ' ').charAt(0).toUpperCase() + element.categoryName.replace(/-/g, ' ').slice(1), author: element.postedBy.fname ? element.postedBy.fname : '', userUrl: element.postedBy.userUrl, title: element.forumTitle, category: element.categoryName, postedOn: element.postedOn, summary: element.forumBody, views: element.views, replyCount: element.comment.length ? element.comment.length : 0, url: element.forumUrl })
            })
            res.send({ objects: resultFromSolr });
        }).catch((err) => {
            console.log(err);
            res.status(500).send("Error");
        });
    }
}


function NewsSearch(req, res, next) {
    var resultFromSolr = [];
    var dataSent = '';
    if (req.body.category) {
        dataSent = 'category:' + req.body.category
    }
    else if (req.body.title) {
        dataSent = `title:${req.body.title} OR author:${req.body.title} OR summary:${req.body.title}`
    }
    var query2 = indexer.createQuery()
        .q(dataSent)
        .matchFilter('entity', 'news')
        .matchFilter('country', req.headers['x-custom-country'])
        .matchFilter('language', req.headers['x-custom-language'])
        .start(0)
        .rows(req.body.count ? req.body.count : 100)
        .sort({ dateSort: 'desc' })
        .fl('category,title,author,thumbnailImagePath,summary,url')

    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            for (var i = 0; i < obj.response.docs.length; i++) {
                resultFromSolr.push({ createdBy: obj.response.docs[i].author[0], category: obj.response.docs[i].category[0], title: obj.response.docs[i].title[0], url: obj.response.docs[i].url[0], summary: obj.response.docs[i].summary[0], newsImage: obj.response.docs[i].thumbnailImagePath[0] })
            }
            res.send({ objects: resultFromSolr, count: obj.response.numFound });
        }
    });
}

function TestingSearch(req, res, next) {

    var resultFromSolr = [];
    var dataSent = '';


    var query2 = indexer.createQuery()
        .q('*:*')
        .matchFilter('entity', 'user-ride')
        .rangeFilter({ field: 'startDate', start: '30-05-2018', end: '31-05-2018' })
        .start(0)
        .rows(10)
        .fl('category,title,author,thumbnailImagePath,summary,url')


    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            for (var i = 0; i < obj.response.docs.length; i++) {

                resultFromSolr.push({ createdBy: obj.response.docs[i].author[0], category: obj.response.docs[i].category[0], title: obj.response.docs[i].title[0], url: obj.response.docs[i].url[0], summary: obj.response.docs[i].summary[0], newsImage: obj.response.docs[i].thumbnailImagePath[0] })
            }
            res.send({ objects: resultFromSolr });
        }
    });
}


function CareerSearch(req, res, next) {

    var query2;
    var dataSent = '';
    if (req.body.Department != "Department" && req.body.city != "City") {
        query2 = indexer.createQuery()
            .q("*:*")
            .matchFilter('entity', 'career')
            .matchFilter('country', req.headers['x-custom-country'])
            .matchFilter('language', req.headers['x-custom-language'])
            .matchFilter('department', req.body.Department)
            .matchFilter('jobCity', req.body.city)
            .start(0)
            .rows(100)
            .sort({ dateSort: 'desc' })
            .fl('jobTitle,jobDescription,jobCountry, jobCity,url')

    }
    else if (req.body.Department && req.body.Department != "Department") {
        query2 = indexer.createQuery()
            .q("*:*")
            .matchFilter('entity', 'career')
            .matchFilter('country', req.headers['x-custom-country'])
            .matchFilter('language', req.headers['x-custom-language'])
            .matchFilter('department', req.body.Department)
            .start(0)
            .rows(100)
            .sort({ dateSort: 'desc' })
            .fl('jobTitle,jobDescription,jobCountry, jobCity,url')

    }



    else if (req.body.city && req.body.city != "City") {
        query2 = indexer.createQuery()
            .q("*:*")
            .matchFilter('entity', 'career')
            .matchFilter('country', req.headers['x-custom-country'])
            .matchFilter('language', req.headers['x-custom-language'])
            .matchFilter('jobCity', req.body.city)
            .start(0)
            .rows(100)
            .sort({ dateSort: 'desc' })
            .fl('jobTitle,jobDescription,jobCountry, jobCity,url')
    }



    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            res.send({ objects: obj.response.docs, count: obj.response.numFound });
        }
    });
}


function updateJoinRide(rideId) {
    var query3 = indexer.add({
        pageId: "5b80f9c72d645040e78c0532",
        email: "divanshu@gmail.com"
    }, (err, obj) => {
        if (err) {
            console.log(err)
        } else {
        }
    })

    var options = {
        waitSearcher: false
    };
    indexer.commit(options, function (err, obj) {
        if (err) {
            console.log(err);
        }
    })

}

function newsEvent(req, res, next) {
    var resultFromSolr = [];
    var dataSent = '';
    if (req.body.category) {
        dataSent = 'category:' + req.body.category
    }
    else if (req.body.title) {
        dataSent = `title:${req.body.title} OR author:${req.body.title} OR summary:${req.body.title}`
    }
    var query2 = indexer.createQuery()
        .q(dataSent)
        .matchFilter('entity', 'news-event')
        .matchFilter('country', req.headers['x-custom-country'])
        .matchFilter('language', req.headers['x-custom-language'])
        .start(0)
        .rows(req.body.count ? req.body.count : 100)
        .sort({ dateSort: 'desc' })
        .fl('category,title,author,thumbnailImagePath,summary,url')

    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            for (var i = 0; i < obj.response.docs.length; i++) {
                resultFromSolr.push({ createdBy: obj.response.docs[i].author[0], category: obj.response.docs[i].category[0], title: obj.response.docs[i].title[0], url: obj.response.docs[i].url[0], summary: obj.response.docs[i].summary[0], newsImage: obj.response.docs[i].thumbnailImagePath[0] })
            }
            res.send({ objects: resultFromSolr, count: obj.response.numFound });
        }
    });
}

function PressReleaseSearch(req, res, next) {
    var resultFromSolr = [];
    var dataSent = '';
    if (req.body.category) {
        dataSent = 'category:' + req.body.category
    }
    else if (req.body.title) {
        dataSent = `title:${req.body.title} OR author:${req.body.title} OR summary:${req.body.title}`
    }
    var query2 = indexer.createQuery()
        .q(dataSent)
        .matchFilter('entity', 'press-release')
        .matchFilter('country', req.headers['x-custom-country'])
        .matchFilter('language', req.headers['x-custom-language'])
        .start(0)
        .rows(req.body.count ? req.body.count : 100)
        .sort({ dateSort: 'desc' })
        .fl('category,title,author,thumbnailImagePath,summary,url')

    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            for (var i = 0; i < obj.response.docs.length; i++) {
                resultFromSolr.push({ createdBy: obj.response.docs[i].author[0], category: obj.response.docs[i].category[0], title: obj.response.docs[i].title[0], url: obj.response.docs[i].url[0], summary: obj.response.docs[i].summary[0], newsImage: obj.response.docs[i].thumbnailImagePath[0] })
            }
            res.send({ objects: resultFromSolr, count: obj.response.numFound });
        }
    });
}

function bannerDataSearch(req, res, next) {

    var resultFromSolr = [];
    var dataSent = 'pageId:' + req.body.pageId;

    var query2 = indexer.createQuery()
        .q(dataSent)
        .matchFilter('entity', 'motorcycles')
        .matchFilter('country', req.headers['x-custom-country'])
        .matchFilter('language', req.headers['x-custom-language'])
        .start(0)
        .rows(100)
        .fl('mobileImagePath,bikeDescription,backgroundImagePath,thumbnailImageAltText,title');

    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            res.send(obj.response.docs)
        }
    })

}

function GMAProductSearch(req, res, next) {
    var resultFromSolr = [];
    var dataSent = '';
    if (req.body.motorcycleCodes && (!req.body.category)) {
        dataSent = 'motorcycleCodes:' + req.body.motorcycleCodes
    } else if (req.body.category && !req.body.motorcycleCodes) {
        dataSent = 'category:' + req.body.category
    } else if (req.body.motorcycleCodes && req.body.category) {
        dataSent = `motorcycleCodes: ${req.body.motorcycleCodes} AND category: ${req.body.category}`
    }
    var query2 = indexer.createQuery()
        .q(dataSent)
        .matchFilter('entity', 'GMA')
        .matchFilter('country', req.headers['x-custom-country'])
        .matchFilter('language', req.headers['x-custom-language'])
        .start(0)
        .rows(req.body.count)
        .fl('price,url,productName,thumbnailImagePath,thumbnailImagePathAltText')

    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err)
        } else {

            res.send({ results: obj.response.docs, docsFound: obj.response.numFound });
        }
    })
}

export default {
    sendSms,
    searchSolr,
    searchRide,
    tripStorySearch,
    tripStorySearchOnClick,
    forumTopicSearchOnClick,
    tripStorySearchCount,
    forumTopicSearch,
    NewsSearch,
    searchSolrCategory,
    CareerSearch,
    PressReleaseSearch,
    GMAProductSearch,
    bannerDataSearch,
    newsEvent,
    forumTopicSearchOnEnter
};

