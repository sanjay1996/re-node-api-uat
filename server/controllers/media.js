import emailApi from "../service/emailApi";
import { EmailTemplate } from "email-templates";
import config from "../../config/env";
import path from "path";
import User from "../models/user";

var resObj = {
  message: "submited successfully",
  data: {},
  status: ""
};

function submit(req, res, next) {
  const emailDir = path.resolve(__dirname, "../templates/mailers", "media-kit-for-re-team");
  const emailDir2 = path.resolve(__dirname, "../templates/mailers", "media-kit-registration");

  const emailMediaKitReg = new EmailTemplate(path.join(emailDir));
  const emailMediaKitForReTeam = new EmailTemplate(path.join(emailDir2));

  var datahtml = {
    Name: req.body.fname,
    OfficalEmailId: req.body.email,
    MobileNo: req.body.mnumber,
    Designation: req.body.designation,
    Organization: req.body.organization,
    Country: req.body.countrySelect,
    PublicationOrMediaHouse: req.body.publication,
    Media: req.body.mediaSelect,
    EstReadershipViewership: req.body.viewershipSelect,
    Website: req.body.website,
    url: config.websiteUrl
  };

  const locals = Object.assign({}, { data: datahtml });
  console.log(req.body.userId);
  User.findOne({ _id: req.body.userId })
    .then(userFound => {
      console.log(userFound);
      userFound.mediaRole.status = "Pending";
      userFound.save().then(() => {
        (resObj.status = "200"), res.send(resObj);
      });
    })
    .catch(err => {
      console.log(err);
    });

  emailMediaKitReg.render(locals, (err, results) => {
    emailApi.sendEmailApi(
      "Your Royal Enfield Media Kit query",
      "Thank you for submitting Media kit query",
      results.html,
      req.body.authorEmail,
      "error sending mail",
      "mail sent successfully"
    );
  });
  emailMediaKitForReTeam.render(locals, (err, results) => {
    emailApi.sendEmailApi(
      "Your Royal Enfield Media Kit query",
      "Thank you for submitting Media kit query",
      results.html,
      req.body.email,
      "error sending mail",
      "mail sent successfully"
    );
  });
}

//Route for verification of user media role
function verifyingUserMediaRole(req, res, next) {
  User.findOne({ _id: req.body.userId })
    .then(foundUser => {
      console.log(foundUser);
      if (foundUser.mediaRole.status == "Approved") {
        res.send({
          status: 200,
          msg: "User Found"
        });
      } else if (foundUser.mediaRole.status == "Pending") {
        res.send({
          status: 302,
          msg: "User status pending"
        });
      } else if (foundUser.mediaRole.status == "Declined") {
        res.send({
          status: 305,
          msg: "User request for media roal is declined"
        });
      } else {
        res.send({
          status: 404,
          msg: "User not requested the media role"
        });
      }
    })
    .catch(err => {
      console.log(err);
    });
}

//Route for approving the user from royalenfield for media role
function verifyingUserFromUserDashboard(req, res, next) {
  User.findOne({ _id: req.body.userId })
    .then(userFound => {
      userFound.mediaRole.status = req.body.status;
      userFound.save().then(statusSavedUser => {
        res.send({ message: "Successfully Approved", status: 200 });

        //Send email to user that his status has been approved
      });
    })
    .catch(err => {
      console.log(err);
    });
}

export default { submit, verifyingUserMediaRole, verifyingUserFromUserDashboard };
