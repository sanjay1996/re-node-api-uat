import qs from "querystring";
import crypto from "crypto";
import emailApi from "../service/emailApi";
import { EmailTemplate } from "email-templates";
import config from "../../config/env";
import path from "path";
import EscapadeWayanad from "../models/escapadeWayanad";
import TorSchema from "../models/tor";

const emailDir = path.resolve(__dirname, "../templates/mailers", "join-ride-events");
const emailDirforCouple = path.resolve(__dirname, "../templates/mailers", "join-ride-event-group");
const emailDirforTOR = path.resolve(__dirname, "../templates/mailers", "tour-of-rajasthan-single");
const emailDirforTORCouple = path.resolve(__dirname, "../templates/mailers", "tour-of-rajasthan-couple");



const emailJoinRide = new EmailTemplate(path.join(emailDir));
const emailJoinRideforCouple = new EmailTemplate(path.join(emailDirforCouple));
const emailJoinRideforTORSingle = new EmailTemplate(path.join(emailDirforTOR));
const emailJoinRideforTORCouple = new EmailTemplate(path.join(emailDirforTORCouple));





function escapadeWayanadRequestPayment(req, res) {
  var orderId = Math.floor(1000000000 + Math.random() * 9000000000).toString();
  var amt = 3000;
  req.body.orderId = "EW" + orderId;
  if (req.body.regType == "Couple") {
    req.body.eventPayment = amt * 2;
    var partOrderId = Math.floor(1000000000 + Math.random() * 9000000000).toString();
    req.body.partOrderId = "EW" + partOrderId;
  } else {
    req.body.eventPayment = amt;
  }
  const newEscapadeWayanad = new EscapadeWayanad(req.body);
  newEscapadeWayanad.save().then(savedForm => {
    var payObj = {
      merchant_id: "10292",
      order_id: savedForm.orderId,
      currency: "INR",
      amount: req.body.eventPayment,
      redirect_url: `${config.websiteUrl}/node/api/payment/responseHandlerEscapadeWayanad`,
      cancel_url: `${config.websiteUrl}/node/api/payment/responseHandlerEscapadeWayanad`,
      language: "EN",
      billing_name: savedForm.fname + " " + savedForm.lname,
      billing_address: savedForm.address,
      billing_city: savedForm.city,
      billing_state: savedForm.state,
      billing_zip: savedForm.postalCode,
      billing_country: savedForm.country,
      billing_tel: savedForm.mobileNumber,
      billing_email: savedForm.email,
      delivery_name: savedForm.fname + " " + savedForm.lname,
      delivery_address: savedForm.address,
      delivery_city: savedForm.city,
      delivery_state: savedForm.state,
      delivery_zip: savedForm.postalCode,
      delivery_country: savedForm.country,
      delivery_tel: savedForm.mobileNumber,
      merchant_param1: savedForm.formName,
      merchant_param2: savedForm.email,
      merchant_param3: "additional + Info",
      merchant_param4: "additional + Info",
      merchant_param5: "additional + Info",
      promo_code: "",
      customer_identifier: ""
    };
    var body = "",
      workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
      accessCode = config.paymentGatewayAccessCode, //Put in the Access Code shared by CCAvenues.
      encRequest = "",
      formbody = "";

    body = `merchant_id=${payObj.merchant_id}&order_id=${payObj.order_id}&currency=${payObj.currency}&amount=${payObj.amount}&redirect_url=${payObj.redirect_url}&cancel_url=${payObj.cancel_url}&language=${payObj.language}&billing_name=${payObj.billing_name}&billing_address=${payObj.billing_address}&billing_city=${payObj.billing_city}&billing_state=${payObj.billing_state}&billing_zip=${payObj.billing_zip}&billing_country=${payObj.billing_country}&billing_tel=${payObj.billing_tel}&billing_email=${payObj.billing_email}&delivery_name=${payObj.delivery_name}&delivery_address=${payObj.delivery_address}&delivery_city=${payObj.delivery_city}&delivery_state=${payObj.delivery_state}&delivery_zip=${payObj.delivery_zip}&delivery_country=${payObj.delivery_country}&delivery_tel=${payObj.delivery_tel}&merchant_param1=${payObj.merchant_param1}&merchant_param2=${payObj.merchant_param2}&merchant_param3=${payObj.merchant_param3}&merchant_param4=${payObj.merchant_param4}&merchant_param5=${payObj.merchant_param5}&promo_code=${payObj.promo_code}&customer_identifier=${payObj.customer_identifier}`;
    encRequest = encrypt(body, workingKey);
    formbody = `<form id="nonseamless" method="POST" name="redirect" action=${config.paymentGatewayURL}> <input type="hidden" id="encRequest" name="encRequest" value=${encRequest}><input type="hidden" name="access_code" id="access_code" value=${accessCode}><script language="javascript">document.redirect.submit();</script></form>`;
    res.writeHeader(200, { "Content-Type": "text/html" });
    res.write(formbody);
    res.end();
  }).catch(err => {
    console.log("====================================");
    console.log(err);
    console.log("====================================");
    res.send("Something Went Wrong!!");
  });
}

function escapadeWayanadResponseHandler(req, res) {
  var ccavEncResponse = "",
    ccavResponse = "",
    workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
    ccavPOST = "";
  var encryption = req.body.encResp;
  ccavResponse = decrypt(encryption, workingKey);
  var pData = qs.parse(ccavResponse);
  EscapadeWayanad.findOneAndUpdate({ orderId: pData.order_id },
    {
      $set: {
        tracking_id: pData.tracking_id,
        bank_ref_no: pData.bank_ref_no,
        order_status: pData.order_status,
        payment_mode: pData.payment_mode,
        payment_resp: pData
      }
    }, { new: true }).then(updatedForm => {
      if (updatedForm.order_status == "Success" && updatedForm.orderId == pData.order_id) {
        var emailDataDT = {
          riderName: pData.billing_name,
          partName: updatedForm.partfname + " " + updatedForm.partlname,
          eventName: "Escapade Wayanad",
          eventEmail: "escapadewayanad@royalenfield.com",
          phoneNo: pData.billing_tel,
          partPhoneNo: updatedForm.partmobile,
          address: updatedForm.address + "," + updatedForm.city + "," + updatedForm.state + "," + updatedForm.country + "," + updatedForm.postalCode,
          amountPaid: pData.currency + " " + pData.amount,
          orderId: updatedForm.orderId,
          partOrderId: updatedForm.partOrderId,
          riderEmail: updatedForm.email,
          partRiderEmail: updatedForm.partemail,
          url: config.websiteUrl
        };
        if (updatedForm.regType == "Couple") {
          const localsDT = Object.assign({}, { data: emailDataDT });

          emailJoinRideforCouple.render(localsDT, (err, results) => {
            console.log(err);
            emailApi.sendEmailApi(
              "Join Ride Confirmation",
              "Thank you for Joining a ride",
              results.html,
              updatedForm.email,
              "error sending mail",
              "mail sent successfully"
            );
          });
          res.redirect(`${config.websiteUrl}/in/en/home/payment-successful.html?paymentSuccess=true&orderId=${updatedForm.orderId}&amountPaid=${pData.currency} ${updatedForm.eventPayment}&regType=${updatedForm.regType}&riderName=${updatedForm.fname} ${updatedForm.lname}&partOrderId=${updatedForm.partOrderId}&partName=${updatedForm.partfname} ${updatedForm.partlname}`);
        } else {
          const localsDT = Object.assign({}, { data: emailDataDT });
          emailJoinRide.render(localsDT, (err, results) => {
            console.log(err);
            emailApi.sendEmailApi(
              "Join Ride Confirmation",
              "Thank you for Joining a ride",
              results.html,
              updatedForm.email,
              "error sending mail",
              "mail sent successfully"
            );
          });
          res.redirect(`${config.websiteUrl}/in/en/home/payment-successful.html?paymentSuccess=true&orderId=${updatedForm.orderId}&amountPaid=${pData.currency} ${updatedForm.eventPayment}&regType=${updatedForm.regType}&riderName=${updatedForm.fname} ${updatedForm.lname}`);
        }
      } else {
        res.redirect(`${config.websiteUrl}/in/en/home/payment-failed.html`);
      }
    })
    .catch(error => {
      res.redirect(`${config.websiteUrl}/in/en/home/payment-failed.html`);
      console.log(error);
    });
}

function reUnionNepalRequestPayment(req, res) {
  var orderId = Math.floor(1000000000 + Math.random() * 9000000000).toString();
  var amt = 4000;
  req.body.orderId = "RN" + orderId;
  if (req.body.regType == "Couple") {
    req.body.eventPayment = amt * 2;
    var partOrderId = Math.floor(1000000000 + Math.random() * 9000000000).toString();
    req.body.partOrderId = "RN" + partOrderId;
  } else {
    req.body.eventPayment = amt;
  }
  const newEscapadeWayanad = new EscapadeWayanad(req.body);
  newEscapadeWayanad.save().then(savedForm => {
    var payObj = {
      merchant_id: "10292",
      order_id: savedForm.orderId,
      currency: "INR",
      amount: req.body.eventPayment,
      redirect_url: `${config.websiteUrl}/node/api/payment/response-handler-reunion-nepal`,
      cancel_url: `${config.websiteUrl}/node/api/payment/response-handler-reunion-nepal`,
      language: "EN",
      billing_name: savedForm.fname + " " + savedForm.lname,
      billing_address: savedForm.address,
      billing_city: savedForm.city,
      billing_state: savedForm.state,
      billing_zip: savedForm.postalCode,
      billing_country: savedForm.country,
      billing_tel: savedForm.mobileNumber,
      billing_email: savedForm.email,
      delivery_name: savedForm.fname + " " + savedForm.lname,
      delivery_address: savedForm.address,
      delivery_city: savedForm.city,
      delivery_state: savedForm.state,
      delivery_zip: savedForm.postalCode,
      delivery_country: savedForm.country,
      delivery_tel: savedForm.mobileNumber,
      merchant_param1: savedForm.formName,
      merchant_param2: savedForm.email,
      merchant_param3: "additional + Info",
      merchant_param4: "additional + Info",
      merchant_param5: "additional + Info",
      promo_code: "",
      customer_identifier: ""
    };
    var body = "",
      workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
      accessCode = config.paymentGatewayAccessCode, //Put in the Access Code shared by CCAvenues.
      encRequest = "",
      formbody = "";

    body = `merchant_id=${payObj.merchant_id}&order_id=${payObj.order_id}&currency=${payObj.currency}&amount=${payObj.amount}&redirect_url=${payObj.redirect_url}&cancel_url=${payObj.cancel_url}&language=${payObj.language}&billing_name=${payObj.billing_name}&billing_address=${payObj.billing_address}&billing_city=${payObj.billing_city}&billing_state=${payObj.billing_state}&billing_zip=${payObj.billing_zip}&billing_country=${payObj.billing_country}&billing_tel=${payObj.billing_tel}&billing_email=${payObj.billing_email}&delivery_name=${payObj.delivery_name}&delivery_address=${payObj.delivery_address}&delivery_city=${payObj.delivery_city}&delivery_state=${payObj.delivery_state}&delivery_zip=${payObj.delivery_zip}&delivery_country=${payObj.delivery_country}&delivery_tel=${payObj.delivery_tel}&merchant_param1=${payObj.merchant_param1}&merchant_param2=${payObj.merchant_param2}&merchant_param3=${payObj.merchant_param3}&merchant_param4=${payObj.merchant_param4}&merchant_param5=${payObj.merchant_param5}&promo_code=${payObj.promo_code}&customer_identifier=${payObj.customer_identifier}`;
    encRequest = encrypt(body, workingKey);
    formbody = `<form id="nonseamless" method="POST" name="redirect" action=${config.paymentGatewayURL}> <input type="hidden" id="encRequest" name="encRequest" value=${encRequest}><input type="hidden" name="access_code" id="access_code" value=${accessCode}><script language="javascript">document.redirect.submit();</script></form>`;
    res.writeHeader(200, { "Content-Type": "text/html" });
    res.write(formbody);
    res.end();
  })
    .catch(err => {
      console.log("====================================");
      console.log(err);
      console.log("====================================");
      res.redirect(`${config.websiteUrl}/in/en/home/payment-failed.html`);
    });
}

function reUnionNepalResponseHandeler(req, res) {
  var ccavEncResponse = "",
    ccavResponse = "",
    workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
    ccavPOST = "";
  var encryption = req.body.encResp;
  ccavResponse = decrypt(encryption, workingKey);
  var pData = qs.parse(ccavResponse);
  EscapadeWayanad.findOneAndUpdate({ orderId: pData.order_id },
    {
      $set: {
        tracking_id: pData.tracking_id,
        bank_ref_no: pData.bank_ref_no,
        order_status: pData.order_status,
        payment_mode: pData.payment_mode,
        payment_resp: pData
      }
    }, { new: true }).then(updatedForm => {
      if (updatedForm.order_status == "Success" && updatedForm.orderId == pData.order_id) {
        var emailDataDT = {
          riderName: pData.billing_name,
          partName: updatedForm.partfname + " " + updatedForm.partlname,
          eventName: "Reunion Nepal",
          eventEmail: "reunion@royalenfield.com",
          phoneNo: pData.billing_tel,
          partPhoneNo: updatedForm.partmobile,
          address: updatedForm.address + "," + updatedForm.city + "," + updatedForm.state + "," + updatedForm.country + "," + updatedForm.postalCode,
          amountPaid: pData.currency + " " + pData.amount,
          orderId: updatedForm.orderId,
          partOrderId: updatedForm.partOrderId,
          riderEmail: updatedForm.email,
          partRiderEmail: updatedForm.partemail,
          url: config.websiteUrl
        };
        if (updatedForm.regType == "Couple") {
          const localsDT = Object.assign({}, { data: emailDataDT });
          emailJoinRideforCouple.render(localsDT, (err, results) => {
            emailApi.sendEmailApi(
              "Join Ride Confirmation",
              "Thank you for Joining a ride",
              results.html,
              updatedForm.email,
              "error sending mail",
              "mail sent successfully"
            );
          });
          res.redirect(`${config.websiteUrl}/in/en/home/payment-successful.html?paymentSuccess=true&orderId=${updatedForm.orderId}&amountPaid=${pData.currency} ${updatedForm.eventPayment}&regType=${updatedForm.regType}&riderName=${updatedForm.fname} ${updatedForm.lname}&partOrderId=${updatedForm.partOrderId}&partName=${updatedForm.partfname} ${updatedForm.partlname}`
          );
        } else {
          const localsDT = Object.assign({}, { data: emailDataDT });
          emailJoinRide.render(localsDT, (err, results) => {
            emailApi.sendEmailApi(
              "Join Ride Confirmation",
              "Thank you for Joining a ride",
              results.html,
              updatedForm.email,
              "error sending mail",
              "mail sent successfully"
            );
          });
          res.redirect(`${config.websiteUrl}/in/en/home/payment-successful.html?paymentSuccess=true&orderId=${updatedForm.orderId}&amountPaid=${pData.currency} ${updatedForm.eventPayment}&regType=${updatedForm.regType}&riderName=${updatedForm.fname} ${updatedForm.lname}`
          );
        }
      } else {
        res.redirect(`${config.websiteUrl}/in/en/home/payment-failed.html`);
      }
    }).catch(error => {
      res.redirect(`${config.websiteUrl}/in/en/home/payment-failed.html`);
      console.log(error);
    });
}

function tourOfRajasthanRequestPayment(req, res) {

  var returnObj = {
    status: "Failed",
    message: "Something Went Wrong",
    data: {}
  }
  var amt = 25000;

  if (req.body.registeringAs == "Couple") {
    var newAmt = amt * 2;
    var orderId = "TOR" + Math.floor(1000000000 + Math.random() * 9000000000).toString();
    var partOrderId = "TOR" + Math.floor(1000000000 + Math.random() * 9000000000).toString();
    amt = newAmt;
  } else {
    var orderId = "TOR" + Math.floor(1000000000 + Math.random() * 9000000000).toString();
    amt = 25000;
  }

  var newRide = new TorSchema({
    formName: req.body.formName,
    registeringAs: req.body.registeringAs,
    fname: req.body.fname,
    lname: req.body.lname,
    email: req.body.email,
    mobileNumber: req.body.mobileNumber,
    eventPaymentAmount: amt,
    address: req.body.address,
    country: req.body.country,
    state: req.body.state,
    city: req.body.city,
    postalCode: req.body.postalCode,
    gender: req.body.gender,
    rider_dob: req.body.rider_dob,
    emergencyFname: req.body.emergencyFname,
    emergencyMobileNumber: req.body.emergencyMobileNumber,
    emergencyEmail: req.body.emergencyEmail,
    preferedRiderNumber: req.body.preferedRiderNumber,
    motorcycleOwned: req.body.motorcycleOwned,
    manufacturedYear: req.body.manufacturedYear,
    registrationNumber: req.body.registrationNumber,
    modifactions: req.body.modifactions,
    distanceRiddenInKms: req.body.distanceRiddenInKms,
    ridingExperienceDetails: req.body.ridingExperienceDetails,
    order_id: orderId,

    partorder_id: partOrderId,
    partnerfname: req.body.partnerfname,
    partnerlname: req.body.partnerlname,
    partneremail: req.body.partneremail,
    partnermobile: req.body.partnermobile,
    partnergender: req.body.partnergender,
    partnerdob: req.body.partnerdob,
    partnercityname: req.body.partnercityname,
    partemergencyFname: req.body.partemergencyFname,
    partemergencyMobileNumber: req.body.partemergencyMobileNumber,
    partemergencyEmail: req.body.partemergencyEmail,
    partpreferedRiderNumber: req.body.partpreferedRiderNumber,
    partmotorcycleOwned: req.body.partmotorcycleOwned,
    partmanufacturedYear: req.body.partmanufacturedYear,
    partregistrationNumber: req.body.partregistrationNumber,
    partmodifactions: req.body.partmodifactions,
    partdistanceRiddenInKms: req.body.partdistanceRiddenInKms,
    partridingExperienceDetails: req.body.partridingExperienceDetails,
  });
  newRide.save().then((savedForm) => {
    console.log('=================savedForm===================');
    console.log(savedForm);
    console.log('===============savedForm=====================');
    var payObj = {
      merchant_id: "10292",
      order_id: savedForm.order_id,
      currency: "INR",
      amount: amt,
      redirect_url: `${config.websiteUrl}/node/api/payment/response-handler-tour-of-rajasthan`,
      cancel_url: `${config.websiteUrl}/node/api/payment/response-handler-tour-of-rajasthan`,
      language: "EN",
      billing_name: savedForm.fname + " " + savedForm.lname,
      billing_address: savedForm.address,
      billing_city: savedForm.city,
      billing_state: savedForm.state,
      billing_zip: savedForm.postalCode,
      billing_country: savedForm.country,
      billing_tel: savedForm.mobileNumber,
      billing_email: savedForm.email,
      delivery_name: savedForm.fname + " " + savedForm.lname,
      delivery_address: savedForm.address,
      delivery_city: savedForm.city,
      delivery_state: savedForm.state,
      delivery_zip: savedForm.postalCode,
      delivery_country: savedForm.country,
      delivery_tel: savedForm.mobileNumber,
      merchant_param1: savedForm.formName,
      merchant_param2: savedForm.email,
      promo_code: "",
      customer_identifier: ""
    };
    var body = "",
      workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
      accessCode = config.paymentGatewayAccessCode, //Put in the Access Code shared by CCAvenues.
      encRequest = "",
      formbody = "";

    body = `merchant_id=${payObj.merchant_id}&order_id=${payObj.order_id}&currency=${payObj.currency}&amount=${payObj.amount}&redirect_url=${payObj.redirect_url}&cancel_url=${payObj.cancel_url}&language=${payObj.language}&billing_name=${payObj.billing_name}&billing_address=${payObj.billing_address}&billing_city=${payObj.billing_city}&billing_state=${payObj.billing_state}&billing_zip=${payObj.billing_zip}&billing_country=${payObj.billing_country}&billing_tel=${payObj.billing_tel}&billing_email=${payObj.billing_email}&delivery_name=${payObj.delivery_name}&delivery_address=${payObj.delivery_address}&delivery_city=${payObj.delivery_city}&delivery_state=${payObj.delivery_state}&delivery_zip=${payObj.delivery_zip}&delivery_country=${payObj.delivery_country}&delivery_tel=${payObj.delivery_tel}&merchant_param1=${payObj.merchant_param1}&merchant_param2=${payObj.merchant_param2}&merchant_param3=${payObj.merchant_param3}&merchant_param4=${payObj.merchant_param4}&merchant_param5=${payObj.merchant_param5}&promo_code=${payObj.promo_code}&customer_identifier=${payObj.customer_identifier}`;
    encRequest = encrypt(body, workingKey);
    formbody = `<form id="nonseamless" method="POST" name="redirect" action=${config.paymentGatewayURL}> <input type="hidden" id="encRequest" name="encRequest" value=${encRequest}><input type="hidden" name="access_code" id="access_code" value=${accessCode}><script language="javascript">document.redirect.submit();</script></form>`;
    res.writeHeader(200, { "Content-Type": "text/html" });
    res.write(formbody);
    res.end();
  }).catch((err) => {
    console.log('====================================');
    console.log(err);
    console.log('====================================');
    returnObj.data = err;
    res.send(returnObj)
  });
}


function tourOfRajasthanResponseHandeler(req, res) {
  var ccavEncResponse = "",
    ccavResponse = "",
    workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
    ccavPOST = "";
  var encryption = req.body.encResp;
  ccavResponse = decrypt(encryption, workingKey);
  var pData = qs.parse(ccavResponse);
  console.log('=============pData=======================');
  console.log(pData);
  console.log('================pData====================');
  TorSchema.findOneAndUpdate({ order_id: pData.order_id },
    {
      $set: {
        paymentObject: pData
      }
    }, { new: true }).then(updatedForm => {
      console.log('================updatedForm====================');
      console.log(updatedForm);
      console.log('==============updatedForm======================');
      if (updatedForm.paymentObject.order_status == "Success" && updatedForm.paymentObject.order_id == pData.order_id) {
        var emailDataDT = {
          riderName: pData.billing_name,
          partName: updatedForm.partnerfname + " " + updatedForm.partnerlname,
          eventName: "Tour of Rajasthan",
          eventEmail: "tourofrajasthan@royalenfield.com",
          phoneNo: pData.billing_tel,
          partPhoneNo: updatedForm.partnermobile,
          address: updatedForm.address + "," + updatedForm.city + "," + updatedForm.state + "," + updatedForm.country + "," + updatedForm.postalCode,
          amountPaid: pData.currency + " " + pData.amount,
          order_id: updatedForm.order_id,
          partOrderId: updatedForm.partorder_id,
          riderEmail: updatedForm.email,
          partRiderEmail: updatedForm.partneremail,
          paymnetStatus: pData.order_status,
          url: config.websiteUrl
        };

        if (updatedForm.registeringAs == "Couple") {
          const localsDT = Object.assign({}, { data: emailDataDT });

          emailJoinRideforTORCouple.render(localsDT, (err, results) => {
            console.log(err);
            emailApi.sendEmailApi(
              "Join Ride Confirmation",
              "Thank you for Joining a ride",
              results.html,
              updatedForm.email,
              "error sending mail",
              "mail sent successfully"
            );
          });
          res.redirect(`${config.websiteUrl}/in/en/home/payment-successful.html?paymentSuccess=true&orderId=${updatedForm.order_id}&amountPaid=${pData.currency} ${updatedForm.paymentObject.amount}&regType=${updatedForm.registeringAs}&riderName=${updatedForm.fname} ${updatedForm.lname}&partOrderId=${updatedForm.partorder_id}&partName=${updatedForm.partnerfname} ${updatedForm.partnerlname}`
          );
        } else {
          const localsDT = Object.assign({}, { data: emailDataDT });
          emailJoinRideforTORSingle.render(localsDT, (err, results) => {
            console.log(err);
            emailApi.sendEmailApi(
              "Join Ride Confirmation",
              "Thank you for Joining a ride",
              results.html,
              updatedForm.email,
              "error sending mail",
              "mail sent successfully"
            );
          });
          res.redirect(`${config.websiteUrl}/in/en/home/payment-successful.html?paymentSuccess=true&orderId=${updatedForm.order_id}&amountPaid=${pData.currency} ${updatedForm.paymentObject.amount}&regType=${updatedForm.registeringAs}&riderName=${updatedForm.fname} ${updatedForm.lname}`);
        }
      } else {
        res.redirect(`${config.websiteUrl}/in/en/home/payment-failed.html`);
      }
    }).catch(error => {
      res.redirect(`${config.websiteUrl}/in/en/home/payment-failed.html`);
      console.log(error);
    });
}





function encrypt(plainText, workingKey) {
  var m = crypto.createHash("md5");
  m.update(workingKey);
  var key = m.digest("binary");
  var iv = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f";
  var keyBuffer = new Buffer(key, "binary");
  var cipher = crypto.createCipheriv("aes-128-cbc", keyBuffer, iv);
  var encoded = cipher.update(plainText, "utf8", "hex");
  encoded += cipher.final("hex");
  return encoded;
}

function decrypt(encText, workingKey) {
  var m = crypto.createHash("md5");
  m.update(workingKey);
  var key = m.digest("binary");
  var iv = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f";
  var keyBuffer = new Buffer(key, "binary");
  var decipher = crypto.createDecipheriv("aes-128-cbc", keyBuffer, iv);
  var decoded = decipher.update(encText, "hex", "utf8");
  decoded += decipher.final("utf8");
  return decoded;
}

export default {
  escapadeWayanadRequestPayment,
  escapadeWayanadResponseHandler,
  reUnionNepalRequestPayment,
  reUnionNepalResponseHandeler,
  tourOfRajasthanRequestPayment,
  tourOfRajasthanResponseHandeler
};
