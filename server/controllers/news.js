
import News from '../models/news';

function getNews(req, res, next) {
    var id = req.body.newsUrl;

    News
        .findOne({ _id: id })
        .populate(
            {
                path: 'comment',
                model: 'comments',
                populate: {
                    path: 'Replyid userdetailscomments',
                    populate: { path: 'userdetailsreplies' }
                },
            })
        .then((news) => {
            res.send(news);
        })
        .catch(err => res.send(err))
}

function create(req, res, next) {
    const news = new News({
        newsUrl: req.body.newsUrl
    });
    news
        .saveAsync()
        .then((savedNews) => {
            res.send(savedNews._id);
        })
        .error(e => next(e));
}


function update(req, res, next) {
    News
        .findOne({ _id: req.body._id })
        .then((foundNews) => {
            foundNews.comment = req.body.comment ? req.body.comment : foundNews.comment
        })
    News
        .saveAsync()
        .then((updatedNews) => {
            res.send(updatedNews + "updated Successfully");
        })
        .catch(err => res.send(err));
}

export default {
    create,
    getNews,
    update
}