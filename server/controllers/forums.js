import Forum from '../models/forums';
import ForumCategory from "../models/forumCategory";
import request from "request";
import config from '../../config/env';
import mongoose from 'mongoose';
import moment from "moment";
import User from '../models/user';

var solrClient = require('solr-client');

var indexer = solrClient.createClient({
    host: config.solr,
    port: 8983,
    path: "/solr",
    core: "new_core",
    solrVersion: 721
});



function timeStampToTime(date) {
    var result = moment(date).fromNow();
    var tempDate = result.split(" ");
    if (tempDate[1] === 'hour' || tempDate[1] === 'year' || tempDate[1] === 'days') {
        result = moment(date).format("DD MMM YYYY");
    }
    return result;
}

function incrementTopicViews(req, res, next) {
    Forum
        .findOneAndUpdate({ _id: req.body.forumId }, { $inc: { "views": 1 } }, { new: true })
        .then((updatedForumPost) => {
            ForumCategory.findOneAndUpdate({ categoryName: req.body.categoryName }, { $inc: { "totalViewsCount": 1 } }).then((doc) => {
                res.send(updatedForumPost.views.toString());
            }, (err) => {
                console.log(err);
            })
        }, (err) => {
            console.log(err);
        })
}

function incrementCategoryTopicsCounter(topicCategoryName) {
    ForumCategory
        .findOneAndUpdate({ categoryName: topicCategoryName }, { $inc: { "totalTopicsCount": 1 } })
        .then((doc) => {
            return true;
        }, (err) => {
            console.log(err);
        })
}

function getFourmDetail(req, res, next) {
    var comments = [];
    Forum
        .findOne({ _id: req.body.obj.pageId })
        .populate(
            {
                path: 'postedBy comment',
                populate: { path: 'userdetailscomments' }

            }).then((foundForum) => {
		if(foundForum){
                var cat = foundForum.categoryName.replace(/-/g, ' ').charAt(0).toUpperCase() + foundForum.categoryName.replace(/-/g, ' ').slice(1);
                foundForum.comment.forEach(comment => {
                    var singleComment = {
                        "commentText": comment.commentBody,
                        "userInfo": {
                            "profilePicture": {
                                "srcPath": comment.userdetailscomments.profilePicture,

                            },
                            "fullName": comment.userdetailscomments.fname + ' ' + comment.userdetailscomments.lname,
                            "profilePageUrl": comment.userdetailscomments.userUrl
                        },
                        "commentTime": timeStampToTime(comment.date),
                    }
                    comments.push(singleComment);
                });
                if (comments.length > 0) {
                    var returnObj =
                    {
                        forumCategory: cat,
                        topicViews: foundForum.views,
                        replyCount: foundForum.comment.length ? foundForum.comment.length : 0,
                        forumTopicId: foundForum._id,
                        heading: foundForum.forumTitle,
                        content: foundForum.forumBody,
                        categoryName: foundForum.categoryName,
                        postedOn: timeStampToTime(foundForum.postedOn),
                        postedByUser: {
                            fullName: foundForum.postedBy.fname + " " + foundForum.postedBy.lname,
                            profilePicture: { srcPath: foundForum.postedBy.profilePicture },
                            profilePageUrl: foundForum.postedBy.userUrl
                        },
                        comments: comments,
                    };
                } else {
                    var returnObj =
                    {
                        forumCategory: cat,
                        topicViews: foundForum.views,
                        replyCount: foundForum.comment.length ? foundForum.comment.length : 0,
                        forumTopicId: foundForum._id.toString(),
                        heading: foundForum.forumTitle,
                        content: foundForum.forumBody,
                        categoryName: foundForum.categoryName,
                        postedOn: timeStampToTime(foundForum.postedOn),
                        postedByUser: {
                            fullName: foundForum.postedBy.fname + " " + foundForum.postedBy.lname,
                            profilePicture: { srcPath: foundForum.postedBy.profilePicture },
                            profilePageUrl: foundForum.postedBy.userUrl,
                        }
                    };
                }
                res.send(returnObj);
		}else {
		 res.send({forumFoundMessage : "Not Found"})
		
		}
            }, (err) => {
                              res.send({forumFoundMessage : "Not Found"});
            });
}

function update(req, res, next) {
    var responseObj = {};
    Forum
        .findOne({ _id: req.body.id })
        .then((foundDoc) => {
            foundDoc.forumTitle = req.body.forumTitle ? req.body.forumTitle : foundDoc.forumTitle,
                foundDoc.forumBody = req.body.forumBody ? req.body.forumBody : foundDoc.forumBody,
                foundDoc.categoryName = req.body.categoryName ? req.body.categoryName : foundDoc.categoryName
        })
        .catch((err) => {
            console.log(err);
        })
    Forum
        .saveAsync()
        .then((updatedForum) => {
            res.send(updatedForum);
        })
        .catch((err) => {
            responseObj.success = false;
            responseObj.message = "error updating";
            responseObj.data = err
        })
}


function create(req, res, next) {
    var language = req.headers['x-custom-language'];
    var country = req.headers['x-custom-country'];
    const returnObj = {
        success: true,
        message: '',
        data: {},
    };
    const forum = new Forum({
        forumTitle: req.body.title,
        forumBody: req.body.Content,
        categoryName: req.body.category,
        postedOn: Date.now(),
        postedBy: req.body.postedBy,
        forumAuthor: req.body.postedByUserName,
        locality: {
            country: country,
            language: language
        }
    });
    forum
        .saveAsync()
        .then((savedPost) => {
            var userUrl;
            User
                .findOne({ _id: req.body.postedBy })
                .then((foundUser) => {
                    userUrl = foundUser.userUrl;
                    foundUser.discussionJoined.push(savedPost._id);
                    foundUser.save();
                })
            incrementCategoryTopicsCounter(req.body.category);


            savedPost.forumUrl = "/content/royal-enfield/" + savedPost.locality.country + "/" + savedPost.locality.language + "/" + "home/forum/" + savedPost.categoryName + "/topic-detail.html?forumId=" + savedPost._id  

            savedPost.save().then((updatedForumPost) => {
                returnObj.data.forumPost = updatedForumPost;
                returnObj.data.forumPost.forumUrl = updatedForumPost.forumUrl;
                returnObj.message = 'Post created successfully';
                res.send(returnObj);
                savingFunction(updatedForumPost, updatedForumPost.forumUrl, req, userUrl)
            })

        })
        .catch((e) => { next(e) });
}


function getForumPosts(req, res, next) {
    Forum
        .getForumPosts(parseInt(req.body.skip), parseInt(req.body.limit))
        .then((posts) => {
            const returnObj = {
                success: true,
                message: '',
                data: {},
            };
            returnObj.data.post = posts;
            returnObj.message = 'Forum-Posts retrieved successfully';
            res.send(returnObj);
        })
        .error((e) => next(e));
}






var savingFunction = function (savedPost, pagePath, req, userUrl) {
    return new Promise((resolve, reject) => {

        var obj = {
            entity: 'forum-topic',
            pageId: savedPost._id.toString(),
            title: savedPost.forumTitle,
            country: savedPost.locality.country,
            language: savedPost.locality.language,
            author: req.body.postedByUserName,
            category: savedPost.categoryName,
            summary: req.body.Content,
            postedBy: req.body.postedBy,
            userUrl: userUrl,
            views: savedPost.views,
            cat: savedPost.categoryName,
            replyCount: savedPost.comment.length,
            postedOn: moment(savedPost.postedOn).format("DD MMM YYYY"),
            dateSort: moment().toISOString(),
            url: "/content/royal-enfield/" + savedPost.locality.country + "/" + savedPost.locality.language + "/" + "home/forum/" + savedPost.categoryName + "/forum-topic-detail-catalog.html?forumId=" + savedPost._id,
            id: "/content/royal-enfield/" + savedPost.locality.country + "/" + savedPost.locality.language + "/" + "home/forum/" + savedPost.categoryName + "/forum-topic-detail-catalog?forumId=" + savedPost._id

        }
        indexer.add(obj, function (err, obj) {

            if (err) {
                resolve();
                console.log(err);
            } else {
                var options = {
                    waitSearcher: false
                };
                indexer.commit(options, function (err, obj) {
                    if (err) {
                        console.log(err);
                        resolve();
                    } else {
                        console.log(obj);
                        resolve();
                    }
                })

                console.log(obj);
            }
        })

    })
}


export default {
    getFourmDetail,
    getForumPosts,
    create,
    incrementTopicViews,
};
