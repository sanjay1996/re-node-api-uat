import UserSchema from '../models/user';
import Story from '../models/tripStory';
import multer from 'multer';
import request from "request";
import config from '../../config/env';
import moment from 'moment';
import emailApi from "../service/emailApi";
import { EmailTemplate } from "email-templates";
import path from "path";
import youtubeThumbnail from 'youtube-thumbnail';
var solrClient = require('solr-client');

var indexer = solrClient.createClient({
    host: config.solr,
    port: 8983,
    path: "/solr",
    core: "new_core",
    solrVersion: 721
});
const emailDir = path.resolve(
    __dirname,
    "../templates/mailers",
    "submit-trip-story"); const emailtripstory = new EmailTemplate(path.join(emailDir)); function getStory(req, res, next) {
        var storyId;
        var nextStory = {
            imagePath: null,
            title: null,
        };
        var previousStory = {
            imagePath: null,
            title: null
        };
        if (req.body.requestContentJSON) { storyId = req.body.requestContentJSON };
        //To find previos Story
        Story
            .findOne({ '_id': { '$lt': storyId } }, 'tripStoryImages storyTitle storyUrl')
            .sort({ '_id': -1 })
            .limit(1)
            .then((data) => {
                if (data != null) {
                    if (data.tripStoryImages.length > 0) {
                        previousStory = {
                            thumbnailImagePath: data.tripStoryImages[0].srcPath,
                            title: data.storyTitle,
                            pageUrl: data.storyUrl,
                        }
                    }
                }
            }).catch((err) => {
                res.status(500).send({ message: "Error", success: "False" });
            });
        //To find next Story
        Story
            .findOne({ '_id': { '$gt': storyId } }, 'tripStoryImages storyTitle storyUrl')
            .sort('_id')
            .limit(1)
            .then((data) => {
                if (data != null) {
                    if (data.tripStoryImages.length > 0) {
                        nextStory = {
                            thumbnailImagePath: data.tripStoryImages[0].srcPath,
                            title: data.storyTitle,
                            pageUrl: data.storyUrl
                        }
                    }
                }
            }, (err) => {
                console.log(err);
            });
        Story
            .findOne({ _id: storyId })
            .populate('postedBy', 'fname lname profilePicture')
            .then((doc) => {
                var videoArr = [];
                var videoUrl = [];
                videoUrl = doc.videoThumbnail
                for (var i = 0; i < doc.tripStoryImages.length; i++) {
                    videoArr.push(doc.tripStoryImages[i])
                }
                var arr = [];
                var returnObj =
                {
                    tripStoryId: doc._id,
                    title: doc.storyTitle,
                    tripStoryBody: doc.storyBody,
                    summary: doc.storySummary,
                    postedOn: doc.postedOn,
                    coverImage: { srcPath: doc.coverImage },
                    postedByUser: {
                        firstName: doc.postedBy.fname,
                        lastName: doc.postedBy.lname,
                        profilePicture: { srcPath: doc.postedBy.profilePicture }
                    },
                    tripImages: doc.tripStoryImages,
                    videoUrl: doc.videoThumbnail,
                    categories: { category: doc.categoryName },
                };
                returnObj.previousStory = previousStory;
                returnObj.nextStory = nextStory;
                res.send(returnObj);
            }, (err) => {
                res.send({ message: err, success: "False" });
            });
    }

function create(req, res, next) {
    var language = req.headers['x-custom-language'];
    var country = req.headers['x-custom-country'];
    var imagePath, imageName;
    var arrayFilePath = [];
    const returnObj = {
        success: true,
        message: '',
        data: {},
    };
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './node/assets/TripStory');
        },
        filename: function (req, file, cb) {
            imageName = Date.now() + file.originalname.replace(/\s/g, '');
            cb(null, imageName);
            imagePath = './node/assets/TripStory' + imageName;
        }
    });
    const uploadObj = multer({ storage: storage }).array('tripImages', 10);
    uploadObj(req, res, (err) => {
        var videoData = [];
        for (var i = 0; i < req.body.field_name.length; i++) {
            if (req.body.field_name[i].includes("youtu.be")) {
                videoData.push(req.body.field_name[i].replace('youtu.be', "youtube.com/embed"))
            } else {
                videoData.push(req.body.field_name[i].replace('watch?v=', 'embed/'))
            }
        }
        if (err) {
            returnObj.data.story = null;
            returnObj.message = 'Error Creating Story';
            returnObj.success = false;
            returnObj.data.error = err;
            res.send(returnObj);
        } else {
            req.files
                .forEach(file => {
                    arrayFilePath.push({ srcPath: "/" + file.path });
                });
            if (arrayFilePath.length == 0) {
                arrayFilePath.push({ srcPath: "https:/node/assets/TripStory/dummyTripStory.jpg" });
            }
            const story = new Story({
                storyTitle: req.body.storyTitle,
                storyBody: req.body.editordata,
                storySummary: req.body.storySummary,
                postedBy: req.body.postedBy,
                storyTags: JSON.parse(req.body.storyTags),
                categoryName: req.body.storyCategory,
                tripStoryImages: arrayFilePath,
                videoThumbnail: videoData,
                locality: {
                    country: country,
                    language: language
                },
            });
            story
                .saveAsync()
                .then((savedStory) => {
                    UserSchema.find({ _id: savedStory.postedBy }).then((userFound) => {
                        userFound[0].tripStoriesCreated.push(savedStory._id);
                        userFound[0].save();
                    });
                    var username = config.aemcredentials.user;
                    var password = config.aemcredentials.password;
                    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
                    request.post({
                        headers: {
                            'content-type': 'application/x-www-form-urlencoded',
                            //              "Authorization": auth
                        },
                        url: `${config.aemPublish}bin/createPage`,
                        rejectUnauthorized: false,
                        form: {
                            entity: 'trip-story',
                            pageId: savedStory._id.toString(),
                            title: savedStory.storyTitle,
                            category: savedStory.categoryName,
                            country: country,
                            language: language,
                            pageProperties: JSON.stringify({
                                "title": req.body.storyTitle,
                                "author": req.body.postedByUserName,
                                "category": req.body.storyCategory,
                                "summary": req.body.storySummary,
                                "storyTags": req.body.storyTags ? req.body.storyTags : '',
                                "thumbnailImagePath": arrayFilePath.length != 0 ? arrayFilePath[0].srcPath : '',
                                "postedBy": req.body.postedBy,
                                "thumbnailImagePathAltText": req.body.storyTitle,
                                "dateSort": moment().toISOString(),
                                "videoThumbnail": videoData.length !== 0 ? videoData[0] : ''
                            })
                        }
                    }, function (error, response, data) {
                        console.log(error)
                        var data = JSON.parse(data);
                        if (error) {
                            returnObj.data.story = null;
                            returnObj.message = 'Error Creating Story';
                            returnObj.success = false;
                            res.status(400).send(returnObj);
                        } else {
                            Story.findOneAndUpdate({ _id: savedStory._id },
                                { $set: { storyUrl: data.pagePath } }, { new: true })
                                .then((updatedStory) => {
                                    UserSchema.findOne({ _id: savedStory.postedBy }).then((myUser) => {
                                        var datahtml = {
                                            userName: myUser.fname + " " + myUser.lname,
                                            storytitleHtml: savedStory.storyTitle,
                                            storydescription: savedStory.storySummary,
                                            storyurlhtml: config.websiteUrl + updatedStory.storyUrl.split("/content/royal-enfield")[1],
                                            url: config.websiteUrl
                                        }
                                        const locals = Object.assign({}, { data: datahtml });
                                        emailtripstory.render(locals, (err, results) => {

                                            console.log(err);
                                            var email = emailApi.sendEmailApi(
                                                "Your Royal Enfield Trip Story",
                                                "Thank you for submitting trip story",
                                                results.html,
                                                myUser.email,
                                                "error sending mail",
                                                "mail sent successfully"
                                            );
                                        });
                                    })
                                    returnObj.data.story = savedStory;
                                    if (data.pagePath) {
                                        var username = config.aemcredentials.user;
                                        var password = config.aemcredentials.user;
                                        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
                                        request.post({
                                            headers: {
                                                'content-type': 'application/x-www-form-urlencoded',
                                                //                      "Authorization": auth
                                            },
                                            rejectUnauthorized: false,
                                            url: `${config.aemPublish2}bin/createPage`,
                                            form: {
                                                entity: 'trip-story',
                                                pageId: savedStory._id.toString(),
                                                title: savedStory.storyTitle,
                                                category: savedStory.categoryName,
                                                country: country,
                                                language: language,
                                                pageProperties: JSON.stringify({
                                                    "title": req.body.storyTitle,
                                                    "author": req.body.postedByUserName,
                                                    "category": req.body.storyCategory,
                                                    "summary": req.body.storySummary,
                                                    "storyTags": req.body.storyTags ? req.body.storyTags : '',
                                                    "thumbnailImagePath": arrayFilePath.length != 0 ? arrayFilePath[0].srcPath : '',
                                                    "postedBy": req.body.postedBy,
                                                    "thumbnailImagePathAltText": req.body.storyTitle,
                                                    "dateSort": moment().toISOString(),
                                                    "videoThumbnail": videoData.length !== 0 ? videoData[0] : ''
                                                })
                                            }
                                        }, function (error, response, data) {
                                            var data = JSON.parse(data);
                                            if (error) {
                                                returnObj.data.story = null;
                                                returnObj.message = 'Error Creating Story';
                                                returnObj.success = false;
                                                res.status(400).send(returnObj);
                                            } else {
                                                returnObj.data.story = savedStory;
                                                returnObj.data.story.storyUrl = data.pagePath;
                                                returnObj.message = 'story created successfully';
                                                savingFunction(savedStory, req, arrayFilePath, videoData)
                                                res.send(returnObj)
                                            }
                                        })
                                    } else {
                                        res.status(400).send("Error");
                                    }
                                }).catch(err => res.status(400).send("Error"))
                        }
                    });
                }).error(e => next(e));
        }
    })
}
function update(req, res, next) {
    Story
        .findOneAndUpdate({ _id: req.body._id }, {
            $set: {
                storyTitle: req.body.storyTitle,
                storyBody: req.body.storyBody,
                tripImages: req.body.tripImages,
                storyUrl: req.body.storyUrl
            }
        })
        .then((savedStory) => {
            const returnObj = {
                success: true,
                message: 'Story updated successfully',
                data: savedStory,
            };
            res.send(returnObj);
        })
        .error(e => next(e));
}
function remove(req, res, next) {
    // code to delete story goes here...
    Story
        .findOne({ _id: req.body._id })
        .then((doc) => {
            if (doc.postedBy == req.body.userid) {
                doc.remove();
            }
            else {
                res.send("invalid user");
            }
        })
}
function getStories(req, res, next) {
    Story
        .getStories(parseInt(req.body.skip), parseInt(req.body.limit))
        .then((stories) => {
            const returnObj = {
                success: true,
                message: '',
                data: {},
            };
            returnObj.data.stories = stories;
            returnObj.message = 'stories retrieved successfully';
            res.send(returnObj);
        })
        .error((e) => next(e));
}
function getmystory(req, res, next) {
    var id = req.body.tripstoryid;
    Story
        .find({ _id: id }).populate(
            {
                path: 'comment',
                model: 'comments',
                populate: {
                    path: 'Replyid userdetailscomments',
                    populate: { path: 'userdetailsreplies' }
                },
            })
        .then((tripstory) => {
            res.send(tripstory[0]);
        })
        .catch(e => res.send(err))
}

var savingFunction = function (savedPost, req, arrayFilePath, videoData) {
    return new Promise((resolve, reject) => {
        var obj = {
            entity: 'trip-story',
            pageId: savedPost._id.toString(),
            title: req.body.storyTitle,
            country: savedPost.locality.country,
            language: savedPost.locality.language,
            author: req.body.postedByUserName,
            category: req.body.storyCategory,
            postedBy: req.body.postedBy,
            cat: req.body.storyCategory,
            summary: req.body.storySummary,
            dateSort: moment().toISOString(),
            thumbnailImagePath: arrayFilePath.length != 0 ? arrayFilePath[0].srcPath : '',
            postedBy: req.body.postedBy,
            thumbnailImageAltText: req.body.storyTitle,
            dateSort: moment().toISOString(),
            videoThumbnail: videoData.length !== 0 ? videoData[0] : '',
            url: "/content/royal-enfield/" + savedPost.locality.country + "/" + savedPost.locality.language + "/" + "home/trip-stories/" + savedPost.categoryName +
                "/trip-story-detail-catalog/trip-story-" + savedPost._id + ".html",
            id: "/content/royal-enfield/" + savedPost.locality.country + "/" + savedPost.locality.language + "/" + "home/trip-stories/" + savedPost.categoryName +
                "/trip-story-detail-catalog/trip-story-" + savedPost._id
        }
        indexer.add(obj, function (err, obj) {
            if (err) {
                resolve();
                console.log(err);
            } else {
                var options = {
                    waitSearcher: false
                };
                indexer.commit(options, function (err, obj) {
                    if (err) {
                        console.log(err);
                        resolve();
                    } else {
                        resolve();
                    }
                })
            }
        })
    })
}




export default {
    getStories,
    getStory,
    create,
    update,
    remove,
    getmystory
};
