import BikeSchema from '../models/bike';
import News from '../models/news';
import Forum from '../models/forumCategory';
import mongoose from 'mongoose';

function generateId(req, res, next) {
// console.log("==================bike================")
// console.log(req.body)
// console.log("==================bike================")
    var language = req.headers['x-custom-language'];
  var country = req.headers['x-custom-country'];

    var category = req.body.requestContentJSON;
    if (category == 'news') {
        const news = new News({
            locality: {
                country: country,
                language: language
                    },
        });
        news
            .save()
            .then((savedNews) => {
                var obj = { pageId: savedNews._id }
                res.send({ pageId: savedNews._id.toString() });
            })
            .catch((error) => {
                console.log(error);
            });


    }  else if (category == 'press-release' || category == 'news-event') {
        const news = new News({
            locality: {
                country: country,
             language: language
            },
        });
        news
            .save()
            .then((savedNews) => {
                var obj = { pageId: savedNews._id }
                res.send({ pageId: savedNews._id.toString() });
            })
            .catch((error) => {
                console.log(error);
            });


    }else if(category == "forumCategory"){
	const forum = new Forum({
	categoryName : req.body.category	
	})
	forum
		.save()
		.then((savedForumCategory)=>{
			// console.log(savedForumCategory)
			res.send({pageId: savedForumCategory._id.toString()})
		})
		.catch((err)=>{
			res.send(err.toString())
		})
	
	}

    else {
        var arr = category.split('_');
        const bike = BikeSchema({
            locality: {
                country: country,
                language: language
                    },
            bikeName: arr[1]
        });
        bike.save().then((savedBike) => {
            res.send({ pageId: arr[0] + '_' + savedBike._id });
        })


    }


}
export default { generateId };

