import qs from "querystring";
import crypto from "crypto";
import emailApi from "../service/emailApi";
import { EmailTemplate } from "email-templates";
import config from "../../config/env";
import path from "path";
import MotoHimalaya from "../models/motohimalaya";
import Reunionwest from "../models/reunionwest.js";
import tourIndonesia from "../models/tourIndonesia.js";

const emailMotohimalayaComplete = new EmailTemplate(
  path.join(path.resolve(__dirname, "../templates/mailers", "motohimalayaComplete"))
);
const emailMotohimalayaUploadLater = new EmailTemplate(
  path.join(path.resolve(__dirname, "../templates/mailers", "motohimalayaUploadLater"))
);

const emailDirforReunionWestSingle = path.resolve(__dirname, "../templates/mailers", "reunionWestSingle");
const emailDirforReunionWestcouple = path.resolve(__dirname, "../templates/mailers", "reunionWestcouple");

const emailReunionWestSingle = new EmailTemplate(path.join(emailDirforReunionWestSingle));
const emailReunionWestcouple = new EmailTemplate(path.join(emailDirforReunionWestcouple));

const emailtourIndonesiaUploadLater = new EmailTemplate(
  path.join(path.resolve(__dirname, "../templates/mailers", "tour-Indonesia-UploadLater"))
);

const emailDirfortourIndonesiaSingle = path.resolve(__dirname, "../templates/mailers", "tour-Indonesia-Single");
const emailDirfortourIndonesiacouple = path.resolve(__dirname, "../templates/mailers", "tour-Indonesia-Couple");

const emailtourIndonesiaSingle = new EmailTemplate(path.join(emailDirfortourIndonesiaSingle));
const emailtourIndonesiaCouple = new EmailTemplate(path.join(emailDirfortourIndonesiacouple));

function validatingDynamicForms(req, res, next) {
  var path = "../models/" + req.body.formName;
  var resObj = {};
  var DynamicSchema = require(path);
  DynamicSchema.find()
    .then(formFound => {
      var totalUsers = formFound.length;
      if (totalUsers >= req.body.ridersLimit) {
        resObj.status = 400;
        resObj.message = "Registartion closed";
        res.send(resObj);
      } else {
        resObj.status = 200;
        resObj.message = "Registation open";
        resObj.data = totalUsers;
        var datetime = new Date();
        var date = datetime.toISOString().slice(0, 10);
        resObj.date = date;
        res.send(resObj);
      }
    })
    .catch(err => {
      console.log(err);
    });
}

function motohimalayaRequestPayment(req, res) {
  var returnObj = {
    status: "Failed",
    message: "Something Went Wrong",
    data: {}
  };
  var amt = 0;
  var formName = "Motohimalaya";
  var orderId = "NA";

  if (req.body.registeringAs == "Rider") {
    orderId = "RE" + Math.floor(1000000000 + Math.random() * 9000000000).toString();
    amt = 1500;
  } else {
    orderId = "NRE" + Math.floor(1000000000 + Math.random() * 9000000000).toString();
    amt = 2500;
  }

  console.log("====================================");
  console.log(req.body);
  console.log("====================================");

  MotoHimalaya.find({ "paymentObject.order_status": "Success" }).then(foundRides => {
    console.log("================foundRides.length====================");
    console.log(foundRides.length);
    console.log("================foundRides.length====================");

    if (foundRides.length < req.body.maxRegistrationCount) {
      var newMotoHimalaya = new MotoHimalaya({
        formName: formName,
        order_id: orderId,
        regType: req.body.regType,
        fname: req.body.fname,
        lname: req.body.lname,
        email: req.body.email,
        mobileNumber: req.body.mobileNumber,
        countryName: req.body.countryName,
        gender: req.body.gender,
        dob: req.body.dob,
        emergencyContactFirstName: req.body.emergencyContactFirstName,
        emergencyContactMobileNumber: req.body.emergencyContactMobileNumber,
        emergencyContactEmail: req.body.emergencyContactEmail,
        facebook: req.body.facebook,
        twitterHandle: req.body.twitterHandle,
        instagramHandle: req.body.instagramHandle,
        distanceTravelled: req.body.distanceTravelled,
        clubs: req.body.clubs,
        expectFromRide: req.body.expectFromRide,
        uploadDocLater: req.body.uploadDocLater,
        successUrl: req.body.successUrl,
        failureUrl: req.body.failureUrl,
        thankyouUrl: req.body.thankyouUrl,
        moreinfo: req.body.moreinfo,
        my_files: req.files,
        locality: {
          language: req.headers["x-custom-language"],
          country: req.headers["x-custom-country"]
        }
      });

      newMotoHimalaya
        .save()
        .then(savedForm => {
          var payObj = {
            merchant_id: "10292",
            order_id: savedForm.order_id,
            currency: "USD",
            amount: amt,
            redirect_url: `${config.websiteUrl}/node/api/payment/response-handler-motohimalaya`,
            cancel_url: `${config.websiteUrl}/node/api/payment/response-handler-motohimalaya`,
            language: "EN",
            billing_name: savedForm.fname + " " + savedForm.lname,
            billing_address: "",
            billing_city: "",
            billing_state: "",
            billing_zip: "",
            billing_country: savedForm.countryName,
            billing_tel: savedForm.mobileNumber,
            billing_email: savedForm.email,
            delivery_name: savedForm.fname + " " + savedForm.lname,
            delivery_address: "",
            delivery_city: "",
            delivery_state: "",
            delivery_zip: "",
            delivery_country: savedForm.countryName,
            delivery_tel: savedForm.mobileNumber,
            merchant_param1: savedForm.formName,
            merchant_param2: savedForm.email,
            promo_code: "",
            customer_identifier: ""
          };
          var body = "",
            workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
            accessCode = config.paymentGatewayAccessCode, //Put in the Access Code shared by CCAvenues.
            encRequest = "",
            formbody = "";

          body = `merchant_id=${payObj.merchant_id}&order_id=${payObj.order_id}&currency=${payObj.currency}&amount=${
            payObj.amount
          }&redirect_url=${payObj.redirect_url}&cancel_url=${payObj.cancel_url}&language=${
            payObj.language
          }&billing_name=${payObj.billing_name}&billing_address=${payObj.billing_address}&billing_city=${
            payObj.billing_city
          }&billing_state=${payObj.billing_state}&billing_zip=${payObj.billing_zip}&billing_country=${
            payObj.billing_country
          }&billing_tel=${payObj.billing_tel}&billing_email=${payObj.billing_email}&delivery_name=${
            payObj.delivery_name
          }&delivery_address=${payObj.delivery_address}&delivery_city=${payObj.delivery_city}&delivery_state=${
            payObj.delivery_state
          }&delivery_zip=${payObj.delivery_zip}&delivery_country=${payObj.delivery_country}&delivery_tel=${
            payObj.delivery_tel
          }&merchant_param1=${payObj.merchant_param1}&merchant_param2=${payObj.merchant_param2}&merchant_param3=${
            payObj.merchant_param3
          }&merchant_param4=${payObj.merchant_param4}&merchant_param5=${payObj.merchant_param5}&promo_code=${
            payObj.promo_code
          }&customer_identifier=${payObj.customer_identifier}`;
          encRequest = encrypt(body, workingKey);
          formbody = `<form id="nonseamless" method="POST" name="redirect" action=${
            config.paymentGatewayURL
          }> <input type="hidden" id="encRequest" name="encRequest" value=${encRequest}><input type="hidden" name="access_code" id="access_code" value=${accessCode}><script language="javascript">document.redirect.submit();</script></form>`;
          res.writeHeader(200, { "Content-Type": "text/html" });
          res.write(formbody);
          res.end();
        })
        .catch(err => {
          console.log("====================================");
          console.log(err);
          console.log("====================================");
          returnObj.data = err;
          res.send(returnObj);
        });
    } else {
      var returnObj = {
        status: false,
        message: "Registration Closed",
        data: {}
      };
      //   res.redirect(
      //     `${config.websiteUrl}/${req.headers["x-custom-language"]}/${
      //       req.headers["x-custom-country"]
      //     }/home/payment-successful.html`
      //   );
      res.send(returnObj);
    }
  });
}
function motohimalayaResponseHandler(req, res) {
  var ccavEncResponse = "",
    ccavResponse = "",
    workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
    ccavPOST = "";
  var encryption = req.body.encResp;
  ccavResponse = decrypt(encryption, workingKey);
  var pData = qs.parse(ccavResponse);
  var locality = {};
  MotoHimalaya.findOneAndUpdate(
    { order_id: pData.order_id },
    {
      $set: {
        paymentObject: pData,
        tracking_id: pData.tracking_id,
        bank_ref_no: pData.bank_ref_no,
        order_status: pData.order_status,
        payment_mode: pData.payment_mode
      }
    },
    { new: true }
  )
    .then(updatedForm => {
      locality = updatedForm.locality;
      if (updatedForm.paymentObject.order_status == "Success" && updatedForm.paymentObject.order_id == pData.order_id) {
        var uploadLink = `${config.websiteUrl}/${updatedForm.locality.country}/${
          updatedForm.locality.language
        }/home/rides/marquee-rides/moto-himalaya-2019/complete-registration.html?id=${updatedForm._id}&name=${
          updatedForm.fname
        }`;

        var emailDataDT = {
          riderName: pData.billing_name,
          eventName: updatedForm.formName,
          eventEmail: "motohimalya@royalenfield.com",
          phoneNo: pData.billing_tel,
          address: updatedForm.country,
          amountPaid: pData.currency + " " + pData.amount,
          order_id: updatedForm.order_id,
          riderEmail: updatedForm.email,
          paymnetStatus: pData.order_status,
          upload_link: uploadLink,
          url: config.websiteUrl,
          moreinfo: updatedForm.moreinfo
        };

        if (updatedForm.uploadDocLater == "Yes") {
          const localsDT = Object.assign({}, { data: emailDataDT });
          emailMotohimalayaUploadLater.render(localsDT, (err, results) => {
            console.log(err);
            emailApi.sendEmailApi(
              "Join Ride Confirmation",
              "Thank you for Joining a ride",
              results.html,
              updatedForm.email,
              "error sending mail",
              "mail sent successfully"
            );
          });
          //send mail with link to upload pics or zip
          res.redirect(
            `${config.websiteUrl}/${updatedForm.locality.country}/${
              updatedForm.locality.language
            }/home/rides/marquee-rides/moto-himalaya-2019/payment-successful.html?paymentSuccess=true&orderId=${updatedForm.order_id}&amountPaid=${
              pData.currency
            } ${updatedForm.paymentObject.amount}&regType=${
              updatedForm.regType == "nonReOwner" ? "Single Occupancy" : "Twin Sharing"
            }&riderName=${updatedForm.fname} ${updatedForm.lname}`
          );
        } else {
          const localsDT = Object.assign({}, { data: emailDataDT });
          //send mail with confirmation
          emailMotohimalayaComplete.render(localsDT, (err, results) => {
            console.log(err);
            emailApi.sendEmailApi(
              "Join Ride Confirmation",
              "Thank you for Joining a ride",
              results.html,
              updatedForm.email,
              "error sending mail",
              "mail sent successfully"
            );
          });
          res.redirect(
            `${config.websiteUrl}/${updatedForm.locality.country}/${
              updatedForm.locality.language
            }/home/rides/marquee-rides/moto-himalaya-2019/payment-successful.html?paymentSuccess=true&orderId=${updatedForm.order_id}&amountPaid=${
              pData.currency
            } ${updatedForm.paymentObject.amount}&regType=${
              updatedForm.regType == "nonReOwner" ? "Single Occupancy" : "Twin Sharing"
            }&riderName=${updatedForm.fname} ${updatedForm.lname}`
          );
        }
      } else {
        res.redirect(
          `${config.websiteUrl}/${updatedForm.locality.country}/${
            updatedForm.locality.language
          }/home/rides/marquee-rides/moto-himalaya-2019/payment-failure.html`
        );
      }
    })
    .catch(error => {
      res.redirect(`${config.websiteUrl}/${locality.country}/${locality.language}/home/rides/marquee-rides/moto-himalaya-2019/payment-failed.html`);
      console.log(error);
    });
}

function motohimalayaUpload(req, res) {
  var returnObj = {
    status: false,
    message: "Something Went Wrong",
    data: {}
  };
  MotoHimalaya.findOne({ _id: req.body.id }).then(foundRegistraion => {
    if (foundRegistraion.uploadDocLater == "Yes") {
      MotoHimalaya.findOneAndUpdate(
        { _id: req.body.id },
        {
          $set: {
            uploadDocLater: "No",
            my_files: req.files
          }
        },
        { new: true }
      )
        .then(() => {
          returnObj.status = true;
          returnObj.message = "Files Uploaded Successfully";
          res.send(returnObj);
        })
        .catch(e => {
          returnObj.data = e;
          res.send(returnObj);
        });
    } else {
      returnObj.status = false;
      returnObj.message = "Already Uploaded Docs";
      res.send(returnObj);
    }
  });
}

function getAllSuccessfulMotohimalaya(req, res) {
  var returnObj = {
    status: false,
    message: "Something Went Wrong",
    data: {}
  };
  MotoHimalaya.find({ "paymentObject.order_status": "Success" })
    .select("countryName fname lname")
    .then(foundUsers => {
      if (foundUsers) {
        returnObj.status = true;
        returnObj.message = "All Successfull Registration";
        returnObj.data = foundUsers;
        res.send(returnObj);
      } else {
        res.send(returnObj);
      }
    })
    .catch(e => {
      returnObj.data = e;
      res.send(returnObj);
    });
}

function tourindonesiaRequestPayment(req, res) {
  var returnObj = {
    status: "Failed",
    message: "Something Went Wrong",
    data: {}
  };
  console.log("====================================");
  console.log(req.body);
  console.log("====================================");
  var amt = 0;
  var formName = "TourIndonesia";
  var orderId = "NA";
  var partOrderId = "NA;";
  if (req.body.regType == "Single") {
    orderId = "REW" + Math.floor(1000000000 + Math.random() * 9000000000).toString();
    amt = 5500;
  } else {
    orderId = "REW" + Math.floor(1000000000 + Math.random() * 9000000000).toString();
    partOrderId = "REW" + Math.floor(1000000000 + Math.random() * 9000000000).toString();
    amt = 11000;
  }

  tourIndonesia.find({ "paymentObject.order_status": "Success" }).then(foundRides => {
    if (foundRides.length <= req.body.maxcount) {
      var newtourIndonesia = new tourIndonesia({
        formName: formName,
        orderId: orderId,
        partOrderId: partOrderId,
        regType: req.body.regType,
        fname: req.body.fname,
        lname: req.body.lname,
        email: req.body.email,
        mobileNumber: req.body.mobileNumber,
        address: req.body.address,
        country: req.body.country,
        state: req.body.state,
        city: req.body.city,
        postalCode: req.body.postalCode,
        gender: req.body.gender,
        tShirtSize: req.body.tShirtSize,
        ridingWith: req.body.ridingWith,
        motorcycleOwned: req.body.motorcycleOwned,
        manufacturedYear: req.body.manufacturedYear,
        registrationNumber: req.body.registrationNumber,
        partfname: req.body.partfname,
        partlname: req.body.partlname,
        partemail: req.body.partemail,
        partmobile: req.body.partmobile,
        partgender: req.body.partgender,
        partShirtSize: req.body.partShirtSize,
        partdob: req.body.partdob,
        successUrl: req.body.successUrl,
        failureUrl: req.body.failureUrl,
        thankyouUrl: req.body.thankyouUrl,
        locality: {
          language: req.headers["x-custom-language"],
          country: req.headers["x-custom-country"]
        }
      });

      newtourIndonesia
        .save()
        .then(savedForm => {
          console.log("====================================");
          console.log(savedForm);
          console.log("====================================");

          var payObj = {
            merchant_id: "10292",
            order_id: savedForm.orderId,
            currency: "INR",
            amount: amt,
            redirect_url: `${config.websiteUrl}/node/api/payment/response-handler-tourindonesia`,
            cancel_url: `${config.websiteUrl}/node/api/payment/response-handler-tourindonesia`,
            language: "EN",
            billing_name: savedForm.fname + " " + savedForm.lname,
            billing_address: savedForm.address,
            billing_city: savedForm.city,
            billing_state: savedForm.state,
            billing_zip: savedForm.postalCode,
            billing_country: savedForm.country,
            billing_tel: savedForm.mobileNumber,
            billing_email: savedForm.email,
            delivery_name: savedForm.fname + " " + savedForm.lname,
            delivery_address: savedForm.address,
            delivery_city: savedForm.city,
            delivery_state: savedForm.state,
            delivery_zip: savedForm.postalCode,
            delivery_country: savedForm.country,
            delivery_tel: savedForm.mobileNumber,
            merchant_param1: savedForm.formName,
            merchant_param2: savedForm.email,
            promo_code: "",
            customer_identifier: ""
          };
          var body = "",
            workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
            accessCode = config.paymentGatewayAccessCode, //Put in the Access Code shared by CCAvenues.
            encRequest = "",
            formbody = "";

          body = `merchant_id=${payObj.merchant_id}&order_id=${payObj.order_id}&currency=${payObj.currency}&amount=${
            payObj.amount
          }&redirect_url=${payObj.redirect_url}&cancel_url=${payObj.cancel_url}&language=${
            payObj.language
          }&billing_name=${payObj.billing_name}&billing_address=${payObj.billing_address}&billing_city=${
            payObj.billing_city
          }&billing_state=${payObj.billing_state}&billing_zip=${payObj.billing_zip}&billing_country=${
            payObj.billing_country
          }&billing_tel=${payObj.billing_tel}&billing_email=${payObj.billing_email}&delivery_name=${
            payObj.delivery_name
          }&delivery_address=${payObj.delivery_address}&delivery_city=${payObj.delivery_city}&delivery_state=${
            payObj.delivery_state
          }&delivery_zip=${payObj.delivery_zip}&delivery_country=${payObj.delivery_country}&delivery_tel=${
            payObj.delivery_tel
          }&merchant_param1=${payObj.merchant_param1}&merchant_param2=${payObj.merchant_param2}&merchant_param3=${
            payObj.merchant_param3
          }&merchant_param4=${payObj.merchant_param4}&merchant_param5=${payObj.merchant_param5}&promo_code=${
            payObj.promo_code
          }&customer_identifier=${payObj.customer_identifier}`;
          encRequest = encrypt(body, workingKey);
          formbody = `<form id="nonseamless" method="POST" name="redirect" action=${
            config.paymentGatewayURL
          }> <input type="hidden" id="encRequest" name="encRequest" value=${encRequest}><input type="hidden" name="access_code" id="access_code" value=${accessCode}><script language="javascript">document.redirect.submit();</script></form>`;
          res.writeHeader(200, { "Content-Type": "text/html" });
          res.write(formbody);
          res.end();
        })
        .catch(err => {
          console.log("====================================");
          console.log(err);
          console.log("====================================");
          returnObj.data = err;
          res.send(returnObj);
        });
    } else {
      var returnObj = {
        status: false,
        message: "Registration Closed",
        data: {}
      };
      //   res.redirect(
      //     `${config.websiteUrl}/${req.headers["x-custom-language"]}/${
      //       req.headers["x-custom-country"]
      //     }/home/payment-successful.html`
      //   );
      res.send(returnObj);
    }
  });
}

function tourindonesiaResponseHandler(req, res) {
  var ccavEncResponse = "",
    ccavResponse = "",
    workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
    ccavPOST = "";
  var encryption = req.body.encResp;
  ccavResponse = decrypt(encryption, workingKey);
  var pData = qs.parse(ccavResponse);
  var locality = {};
  tourIndonesia
    .findOneAndUpdate(
      { order_id: pData.order_id },
      {
        $set: {
          paymentObject: pData
        }
      },
      { new: true }
    )
    .then(updatedForm => {
      locality = updatedForm.locality;
      if (updatedForm.paymentObject.order_status == "Success" && updatedForm.paymentObject.order_id == pData.order_id) {
        var uploadLink = `${config.websiteUrl}/${updatedForm.locality.country}/${
          updatedForm.locality.language
        }/home/rides/marquee-rides/tour-indonesia/complete-registration.html?id=${updatedForm._id}&name=${
          updatedForm.fname
        }`;

        var emailDataDT = {
          riderName: pData.billing_name,
          eventName: updatedForm.formName,
          eventEmail: "tourindonesia@royalenfield.com",
          phoneNo: pData.billing_tel,
          address: updatedForm.country,
          amountPaid: pData.currency + " " + pData.amount,
          order_id: updatedForm.order_id,
          riderEmail: updatedForm.email,
          paymnetStatus: pData.order_status,
          upload_link: uploadLink,
          url: config.websiteUrl
        };

        if (updatedForm.uploadDocLater == "Yes") {
          const localsDT = Object.assign({}, { data: emailDataDT });
          emailtourIndonesiaUploadLater.render(localsDT, (err, results) => {
            console.log(err);
            emailApi.sendEmailApi(
              "Join Ride Confirmation",
              "Thank you for Joining a ride",
              results.html,
              updatedForm.email,
              "error sending mail",
              "mail sent successfully"
            );
          });
          //send mail with link to upload pics or zip
          res.redirect(
            `${config.websiteUrl}/${updatedForm.locality.country}/${
              updatedForm.locality.language
            }/home/payment-successful.html?paymentSuccess=true&orderId=${updatedForm.order_id}&amountPaid=${
              pData.currency
            } ${updatedForm.paymentObject.amount}&regType=${updatedForm.regType}&riderName=${updatedForm.fname} ${
              updatedForm.lname
            }`
          );
        } else {
          if (updatedForm.regType == "Single") {
            const localsDT = Object.assign({}, { data: emailDataDT });
            //send mail with confirmation
            emailtourIndonesiaSingle.render(localsDT, (err, results) => {
              console.log(err);
              emailApi.sendEmailApi(
                "Join Ride Confirmation",
                "Thank you for Joining a ride",
                results.html,
                updatedForm.email,
                "error sending mail",
                "mail sent successfully"
              );
            });
          } else {
            const localsDT = Object.assign({}, { data: emailDataDT });
            emailtourIndonesiaCouple.render(localsDT, (err, results) => {
              console.log(err);
              emailApi.sendEmailApi(
                "Join Ride Confirmation",
                "Thank you for Joining a ride",
                results.html,
                updatedForm.email,
                "error sending mail",
                "mail sent successfully"
              );
            });
          }

          res.redirect(
            `${config.websiteUrl}/${updatedForm.locality.country}/${
              updatedForm.locality.language
            }/home/payment-successful.html?paymentSuccess=true&orderId=${updatedForm.order_id}&amountPaid=${
              pData.currency
            } ${updatedForm.paymentObject.amount}&regType=${updatedForm.regType}&riderName=${updatedForm.fname} ${
              updatedForm.lname
            }`
          );
        }
      } else {
        res.redirect(
          `${config.websiteUrl}/${updatedForm.locality.country}/${
            updatedForm.locality.language
          }/home/payment-failed.html`
        );
      }
    })
    .catch(error => {
      res.redirect(`${config.websiteUrl}/${locality.country}/${locality.language}/home/payment-failed.html`);
      console.log(error);
    });
}

function getAllSuccessfulIndonesia(req, res) {
  var returnObj = {
    status: false,
    message: "Something Went Wrong",
    data: {}
  };
  tourIndonesia
    .find({ "paymentObject.order_status": "Success" })
    .then(foundUsers => {
      if (foundUsers) {
        returnObj.status = true;
        returnObj.message = "All Successfull Registration";
        returnObj.data = foundUsers;
        res.send(returnObj);
      } else {
        res.send(returnObj);
      }
    })
    .catch(e => {
      returnObj.data = e;
      res.send(returnObj);
    });
}

function reunionwestRequestPayment(req, res) {
  var returnObj = {
    status: "Failed",
    message: "Something Went Wrong",
    data: {}
  };
  console.log("=================req.body===================");
  console.log(req.body);
  // console.log();
  console.log("==============req.body======================");
  var amt = 0;
  var formName = "REunion West";
  var orderId = "NA";
  var partOrderId = "NA;";
  var countSingle = 0;
  var countCouple = 0;
  var finalCount = 0;
  if (req.body.regType == "Single") {
    orderId = "REW" + Math.floor(1000000000 + Math.random() * 9000000000).toString();
    amt = 5500;
  } else {
    orderId = "REW" + Math.floor(1000000000 + Math.random() * 9000000000).toString();
    partOrderId = "REW" + Math.floor(1000000000 + Math.random() * 9000000000).toString();
    amt = 11000;
  }

  Reunionwest.find({ "paymentObject.order_status": "Success" }).then(foundRides => {
    console.log("foundRides.length      " + foundRides.length);

    foundRides.map((rider, i) => {
      if (rider.regType == "Single") {
        countSingle = countSingle + 1;
        console.log("If loop    " + countSingle);
      } else {
        countCouple = countCouple + 1;
        console.log("else loop      " + countCouple);
      }
      if (foundRides.length == i + 1) {
        finalCount = countSingle * 1 + countCouple * 2;
      }
    });
    if (finalCount < req.body.maxcount) {
      console.log("==================finalCount < req.body.maxcount==================");
      console.log("countSingle            " + countSingle);
      console.log("countCouple            " + countCouple * 2);
      console.log("finalCount            " + finalCount);
      console.log("================finalCount < req.body.maxcount====================");
      var newReunionWest = new Reunionwest({
        formName: formName,
        orderId: orderId,
        partOrderId: partOrderId,
        regType: req.body.regType,
        fname: req.body.fname,
        lname: req.body.lname,
        email: req.body.email,
        mobileNumber: req.body.mobileNumber,
        address: req.body.address,
        country: req.body.country,
        state: req.body.state,
        city: req.body.city,
        postalCode: req.body.postalCode,
        gender: req.body.gender,
        tShirtSize: req.body.tShirtSize,
        ridingWith: req.body.ridingWith,
        motorcycleOwned: req.body.motorcycleOwned,
        manufacturedYear: req.body.manufacturedYear,
        registrationNumber: req.body.registrationNumber,
        partfname: req.body.partfname,
        partlname: req.body.partlname,
        partemail: req.body.partemail,
        partmobile: req.body.partmobile,
        partgender: req.body.partgender,
        partShirtSize: req.body.partShirtSize,
        partdob: req.body.partdob,
        successUrl: req.body.successUrl,
        failureUrl: req.body.failureUrl,
        thankyouUrl: req.body.thankyouUrl,
        locality: {
          language: req.headers["x-custom-language"],
          country: req.headers["x-custom-country"]
        }
      });

      newReunionWest
        .save()
        .then(savedForm => {
          var payObj = {
            merchant_id: "10292",
            order_id: savedForm.orderId,
            currency: "INR",
            amount: amt,
            redirect_url: `${config.websiteUrl}/node/api/payment/response-handler-reunionwest`,
            cancel_url: `${config.websiteUrl}/node/api/payment/response-handler-reunionwest`,
            language: "EN",
            billing_name: savedForm.fname + " " + savedForm.lname,
            billing_address: savedForm.address,
            billing_city: savedForm.city,
            billing_state: savedForm.state,
            billing_zip: savedForm.postalCode,
            billing_country: savedForm.country,
            billing_tel: savedForm.mobileNumber,
            billing_email: savedForm.email,
            delivery_name: savedForm.fname + " " + savedForm.lname,
            delivery_address: savedForm.address,
            delivery_city: savedForm.city,
            delivery_state: savedForm.state,
            delivery_zip: savedForm.postalCode,
            delivery_country: savedForm.country,
            delivery_tel: savedForm.mobileNumber,
            merchant_param1: savedForm.formName,
            merchant_param2: savedForm.email,
            promo_code: "",
            customer_identifier: ""
          };
          var body = "",
            workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
            accessCode = config.paymentGatewayAccessCode, //Put in the Access Code shared by CCAvenues.
            encRequest = "",
            formbody = "";

          body = `merchant_id=${payObj.merchant_id}&order_id=${payObj.order_id}&currency=${payObj.currency}&amount=${
            payObj.amount
          }&redirect_url=${payObj.redirect_url}&cancel_url=${payObj.cancel_url}&language=${
            payObj.language
          }&billing_name=${payObj.billing_name}&billing_address=${payObj.billing_address}&billing_city=${
            payObj.billing_city
          }&billing_state=${payObj.billing_state}&billing_zip=${payObj.billing_zip}&billing_country=${
            payObj.billing_country
          }&billing_tel=${payObj.billing_tel}&billing_email=${payObj.billing_email}&delivery_name=${
            payObj.delivery_name
          }&delivery_address=${payObj.delivery_address}&delivery_city=${payObj.delivery_city}&delivery_state=${
            payObj.delivery_state
          }&delivery_zip=${payObj.delivery_zip}&delivery_country=${payObj.delivery_country}&delivery_tel=${
            payObj.delivery_tel
          }&merchant_param1=${payObj.merchant_param1}&merchant_param2=${payObj.merchant_param2}&merchant_param3=${
            payObj.merchant_param3
          }&merchant_param4=${payObj.merchant_param4}&merchant_param5=${payObj.merchant_param5}&promo_code=${
            payObj.promo_code
          }&customer_identifier=${payObj.customer_identifier}`;
          encRequest = encrypt(body, workingKey);
          formbody = `<form id="nonseamless" method="POST" name="redirect" action=${
            config.paymentGatewayURL
          }> <input type="hidden" id="encRequest" name="encRequest" value=${encRequest}><input type="hidden" name="access_code" id="access_code" value=${accessCode}><script language="javascript">document.redirect.submit();</script></form>`;
          res.writeHeader(200, { "Content-Type": "text/html" });
          res.write(formbody);
          res.end();
        })
        .catch(err => {
          console.log("====================================");
          console.log(err);
          console.log("====================================");
          returnObj.data = err;
          res.send(returnObj);
        });
    } else {
      var returnObj = {
        status: false,
        message: "Registration Closed",
        data: {}
      };
      //   res.redirect(
      //     `${config.websiteUrl}/${req.headers["x-custom-language"]}/${
      //       req.headers["x-custom-country"]
      //     }/home/payment-successful.html`
      //   );
      res.send(returnObj);
    }
  });
}

function reunionwestResponseHandler(req, res) {
  var ccavEncResponse = "",
    ccavResponse = "",
    workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
    ccavPOST = "";
  var encryption = req.body.encResp;
  ccavResponse = decrypt(encryption, workingKey);
  var pData = qs.parse(ccavResponse);
  var locality = {};
  console.log("=============pData=======================");
  console.log(pData);
  console.log("================pData====================");
  Reunionwest.findOneAndUpdate(
    { orderId: pData.order_id },
    {
      $set: {
        paymentObject: pData,
        tracking_id: pData.tracking_id,
        bank_ref_no: pData.bank_ref_no,
        order_status: pData.order_status,
        payment_mode: pData.payment_mode
      }
    },
    { new: true }
  )
    .then(updatedForm => {
      locality = updatedForm.locality;
      console.log("================updatedForm====================");
      console.log(updatedForm);
      console.log("==============updatedForm======================");
      console.log("1st" + updatedForm.orderId + "2nd" + updatedForm.paymentObject.order_id);
      if (updatedForm.paymentObject.order_status == "Success" && updatedForm.paymentObject.order_id == pData.order_id) {
        var emailDataDT = {
          riderName: pData.billing_name,
          riderEmail: updatedForm.email,
          phoneNo: pData.billing_tel,
          orderId: updatedForm.orderId,
          partName: updatedForm.partfname + " " + updatedForm.partlname,
          partRiderEmail: updatedForm.partemail,
          partPhoneNo: updatedForm.partmobile,
          partOrderId: updatedForm.partOrderId,
          amountPaid: pData.currency + " " + pData.amount,
          eventName: updatedForm.formName,
          eventEmail: "reunionwest@royalenfield.com",
          url: config.websiteUrl,

          address:
            updatedForm.address +
            "," +
            updatedForm.city +
            "," +
            updatedForm.state +
            "," +
            updatedForm.country +
            "," +
            updatedForm.postalCode
        };
        if (updatedForm.regType == "Single") {
          const localsDT = Object.assign({}, { data: emailDataDT });
          emailReunionWestSingle.render(localsDT, (err, results) => {
            console.log(err);
            emailApi.sendEmailApi(
              "Join Ride Confirmation",
              "Thank you for Joining a ride",
              results.html,
              updatedForm.email,
              "error sending mail",
              "mail sent successfully"
            );
          });
          res.redirect(
            `${config.websiteUrl}/${updatedForm.locality.country}/${
              updatedForm.locality.language
            }/home/payment-successful.html?paymentSuccess=true&orderId=${updatedForm.orderId}&amountPaid=${
              pData.currency
            } ${updatedForm.paymentObject.amount}&regType=${updatedForm.regType}&riderName=${updatedForm.fname} ${
              updatedForm.lname
            }`
          );
        } else {
          const localsDT = Object.assign({}, { data: emailDataDT });
          emailReunionWestcouple.render(localsDT, (err, results) => {
            console.log(err);
            emailApi.sendEmailApi(
              "Join Ride Confirmation",
              "Thank you for Joining a ride",
              results.html,
              updatedForm.email,
              "error sending mail",
              "mail sent successfully"
            );
          });
          res.redirect(
            `${config.websiteUrl}/${updatedForm.locality.country}/${
              updatedForm.locality.language
            }/home/payment-successful.html?paymentSuccess=true&orderId=${updatedForm.orderId}&amountPaid=${
              pData.currency
            } ${updatedForm.paymentObject.amount}&regType=${updatedForm.regType}&riderName=${updatedForm.fname} ${
              updatedForm.lname
            }&partOrderId=${updatedForm.partOrderId}&partName=${updatedForm.partfname} ${updatedForm.partlname}`
          );
        }
      } else {
        res.redirect(
          `${config.websiteUrl}/${updatedForm.locality.country}/${
            updatedForm.locality.language
          }/home/payment-failed.html`
        );
      }
    })
    .catch(error => {
      res.redirect(`${config.websiteUrl}/${locality.country}/${locality.language}/home/payment-failed.html`);
      console.log(error);
    });
}

function getAllSuccessfulReunionWest(req, res) {
  var returnObj = {
    status: false,
    message: "Something Went Wrong",
    data: {}
  };
  Reunionwest.find({ "paymentObject.order_status": "Success" })
    .select("city fname lname partfname partlname")
    .then(foundUsers => {
      if (foundUsers) {
        returnObj.status = true;
        returnObj.message = "All Successfull Registration";
        returnObj.data = foundUsers;
        res.send(returnObj);
      } else {
        res.send(returnObj);
      }
    })
    .catch(e => {
      returnObj.data = e;
      res.send(returnObj);
    });
}

function encrypt(plainText, workingKey) {
  var m = crypto.createHash("md5");
  m.update(workingKey);
  var key = m.digest("binary");
  var iv = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f";
  var keyBuffer = new Buffer(key, "binary");
  var cipher = crypto.createCipheriv("aes-128-cbc", keyBuffer, iv);
  var encoded = cipher.update(plainText, "utf8", "hex");
  encoded += cipher.final("hex");
  return encoded;
}

function decrypt(encText, workingKey) {
  var m = crypto.createHash("md5");
  m.update(workingKey);
  var key = m.digest("binary");
  var iv = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f";
  var keyBuffer = new Buffer(key, "binary");
  var decipher = crypto.createDecipheriv("aes-128-cbc", keyBuffer, iv);
  var decoded = decipher.update(encText, "hex", "utf8");
  decoded += decipher.final("utf8");
  return decoded;
}

export default {
  validatingDynamicForms,
  motohimalayaRequestPayment,
  motohimalayaResponseHandler,
  motohimalayaUpload,
  getAllSuccessfulMotohimalaya,
  tourindonesiaRequestPayment,
  tourindonesiaResponseHandler,
  getAllSuccessfulIndonesia,
  reunionwestRequestPayment,
  reunionwestResponseHandler,
  getAllSuccessfulReunionWest
};
