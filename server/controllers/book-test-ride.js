import BookTestRide from "../models/book-test-ride";
import BookTestRidefunc from "../service/dmsApi";
import queryString from "query-string";
import axios from "axios";
import request from "request";
import emailApi from "../service/emailApi";
import { EmailTemplate } from "email-templates";
import path from "path";
import Dealer from "../models/dealers";
import jsonexport from "jsonexport";
import fs from "fs";
import config from "../../config/env";


var resObj = {};

const emailDir = path.resolve(__dirname, "../templates/mailers", "test-ride-confirmation");
const emailTestRide = new EmailTemplate(path.join(emailDir));

function create(req, res, next) {
  var language = req.headers['x-custom-language'];
  var country = req.headers['x-custom-country'];
  const bookTestRideVar = new BookTestRide({
    locality: {
      country: country,
      language: language
    },
    fName: req.body.fName,
    lName: req.body.lName,
    email: req.body.email,
    bikeName: req.body.bike,
    countryName: req.body.countryName,
    stateName: req.body.stateName,
    cityName: req.body.cityName,
    dealerName: req.body.dealerName,
    dealerCode: req.body.dealerCode,
    Date: req.body.Date,
    mobile: req.body.mobile,
    buyPlanDate: req.body.buyPlanDate,
    time: req.body.time
  });
  bookTestRideVar.saveAsync().then(savedRide => {
    Dealer.findOne({ DealerID: savedRide.dealerCode }).then(foundDealer => {
      var data = {
        userName: savedRide.fName + " " + savedRide.lName,
        dealerName: savedRide.dealerName,
        dealerAddress: foundDealer.AddressLine1 ? foundDealer.AddressLine1 : "",
        dealerPhone: foundDealer.MainPhoneNo ? foundDealer.MainPhoneNo : "",
        dealerEmail: foundDealer.StoreEmailId ? foundDealer.StoreEmailId : "",
        url: config.websiteUrl
      };
      const locals = Object.assign({}, { data: data });
      emailTestRide.render(locals, (err, results) => {
        emailApi.sendEmailApi(
          "Test Ride Request Confirmation",
          "Thank you for booking test ride",
          results.html,
          savedRide.email,
          "error sending mail",
          "mail sent successfully"
        );
      });
      const newUser = [{
        testrideId: `${savedRide._id}`,
        country: `${savedRide.countryName}`,
        state: `${savedRide.stateName}`,
        city: `${savedRide.cityName}`,
        name: `${savedRide.fName + " " + savedRide.lName}`,
        email: `${savedRide.email}`,
        strphone: `${savedRide.mobile}`,
        address: `${foundDealer.AddressLine1 + " " + foundDealer.AddressLine2 + " " + foundDealer.AddressLine3}`,
        reusedbefore: `NA`,
        ownRe: `NA`,
        bikemodel: `${savedRide.bikeName}`,
        year: `NA`,
        modelId: `NA`,
        modelname: `NA`,
        dealername: `${foundDealer.DealerName}`,
        dealerCode: `${foundDealer.DealerID}`,
        createdon: `${savedRide.Date + " " + savedRide.time}`,
        source: `NA`,
        WebPageSource: `NA`,
      }];

      jsonexport(newUser, { rowDelimiter: "|" }, function (err, csv) {
        if (err) {
          console.log(err);
        } else {
          fs.readdir("./archivedFiles/TestRide", (err, files) => {
            if (files.length == 0) {
              fs.writeFile(`./archivedFiles/TestRide/userTestRide.csv`, csv, function (err) {
                if (err) throw err;
                console.log("============csv after reading dir and its empty========================");
                console.log("File Created Successfully");
                console.log("====================================");
              });
            } else {
              console.log("====================================");
              console.log(csv);
              console.log("====================================");
              if (csv.includes("testrideId|country|state|city|name|email|strphone|address|reusedbefore|ownRe|bikemodel|year|modelId|modelname|dealername|dealerCode|createdon|source|WebPageSource")) {
                console.log("======================================Inside If")
                var dumpData = csv.replace("testrideId|country|state|city|name|email|strphone|address|reusedbefore|ownRe|bikemodel|year|modelId|modelname|dealername|dealerCode|createdon|source|WebPageSource", "");
                var finalData = dumpData.trim();
                console.log('=========inside else loop and this is finalData===========================');
                console.log(finalData);
                console.log('====================================');
                fs.appendFile(`./archivedFiles/TestRide/userTestRide.csv`, "\r\n" + finalData, function (err) {
                  if (err) throw err;
                  console.log('========appended to file and final data============================');
                  console.log(finalData);
                  console.log('====================================');
                });
              }
            }
          });
        }
      });
    });


    var options = {
      method: "POST",
      url: "http://crc.cequitycti.com/RoyalEnfield/service.asmx/Organic",
      headers: {
        "cache-control": "no-cache",
        "content-type": "application/x-www-form-urlencoded"
      },
      form: {
        // Name: "AtulGupta",
        // mobile: "9555280002",
        // EMail: "atul.gupta@techchefz.com",
        // Country: "india",
        // State: "Haryana",
        // City: "Faridabad",
        // Motorcycle: "Squadron Blue",
        // Dealer_Name: "Sohan auto world",
        // When_do_plan_to_buy: "This week",
        // Web_link: "https://royalenfield.com",
        // Tocken: "ayz4EbIZLT403KVQ7dffhJ1Ffzv0yiSui1i1VXmAssesLjYODiy5dHIdfd2W11Q9QH6oHTuTWyIi7t5GsfGYk4qWisQdAP1X",
        // Dealer_Code: savedRide.dealerCode,
        // UTM: "22feet",
        // Test_Ride_Date: "12-01-2019",
        // Test_Ride_Time: "Evening"

        Name: savedRide.fName + savedRide.lName,
        mobile: savedRide.mobile,
        EMail: savedRide.email,
        Country: savedRide.countryName,
        State: savedRide.stateName,
        City: savedRide.cityName,
        Motorcycle: savedRide.bikeName,
        Dealer_Name: savedRide.dealerName,
        When_do_plan_to_buy: savedRide.buyPlanDate,
        Web_link: "https://royalenfield.com",
        Tocken: "ayz4EbIZLT403KVQ7dffhJ1Ffzv0yiSui1i1VXmAssesLjYODiy5dHIdfd2W11Q9QH6oHTuTWyIi7t5GsfGYk4qWisQdAP1X",
        Dealer_Code: savedRide.dealerCode,
        UTM: "22feet",
        Test_Ride_Date: savedRide.Date,
        Test_Ride_Time: savedRide.time
      }
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      resObj.success = true;
      resObj.message = "successfully submitted test ride";
      resObj.data = {};
      res.send(resObj);
      console.log(body);
      console.log("=========body================body===========");
      if (error) {
        console.log("====book-test-ride=============error===================");
        console.log(error);
        console.log("====book-test-ride============error====================");
      } else {
        resObj.success = true;
        resObj.message = "successfully submitted test ride";
        resObj.data = {};
        res.send(resObj);
      }
    });
  });
}

export default {
  create
};
