import DynamicFormSchema from '../models/dynamicForms';
import emailApi from "../service/emailApi";
import { EmailTemplate } from "email-templates";
import config from '../../config/env'
import path from "path";
import { encrypt } from "./payment";


const emailDir = path.resolve(
    __dirname,
    "../templates/mailers",
    "join-ride-events"
);

const emailJoinRide = new EmailTemplate(path.join(emailDir));

function createDynamicForm(req, res, next) {
    var formData = [];
    var resMsg = {
        nodePersist: false
    };
    DynamicFormSchema.findOne({ formId: req.body.obj.formId }).then((foundForm) => {
        // console.log('========foundform============================');
        // console.log(foundForm);
        // console.log('=================foundFroem===================');
        if (foundForm !== null || foundForm !== undefined) {
            const Form = new DynamicFormSchema({
                formId: req.body.obj.formId,
                formConfig: req.body.obj,
                formData: formData,
            });
            Form.save().then(savedObj => {
                resMsg.nodePersist = true;
                res.send(resMsg);
            }).catch(error => {
                console.log(error);
                res.send(resMsg);
            })
        } else {
            res.send(resMsg);
        }
    }).catch((e) => {
        console.log(e);
        res.send(resMsg);
    })

}

function submitDataToForm(req, res, next) {
    var formData = {};
    var reqForm = req.body.formData;
    reqForm.paymentToken = "";
    reqForm.paymentSuccess = false;
    var resMsg = {
        success: false,
        message: "Error Submitting Form",
    };

    var assetsPath = [];

    if (req.files) {
        req.files.forEach(file => {
            assetsPath.push(file.path);
        });
        formData = reqForm;
        formData.formAssets = assetsPath;
    } else {
        formData = reqForm;
    }

    DynamicFormSchema.findOneAndUpdate({ _id: req.body.formId }, { $push: { formData: formData } }, { new: true }).then(form => {
        resMsg.success = true;
        resMsg.message = "Form Submitted Successfully";
        res.send(resMsg);
    }, error => {
        console.log(error);
        res.send(resMsg);
    })
}


function newSubmitDataToForm(req, res, next) {
    var resMsg = {
        success: false,
        message: "Error Submitting Form",
    };
    const regNumber = Math.floor(100000 + Math.random() * 900000); //eslint-disable-line
    req.body.regNumber = regNumber;

    //Check if user has already submitted the form

    DynamicFormSchema.findOne({ formId: req.body.formId })
        .then((foundForm) => {
            // console.log('================foundForm====================');
            // console.log(req.body);
            // console.log('================foundForm====================');
            if (foundForm.formData.length != 0) {
                console.log("inside bada if");

                foundForm.formData.forEach((item, i) => {
                    console.log("inside forecah");

                    if (item.email == req.body.email) {
                        console.log("inside if loop");
                        resMsg.message = "Already registered with given email"
                        res.send(resMsg);
                    } else {
                        console.log("inside else");

                        DynamicFormSchema.findOneAndUpdate({ formId: req.body.formId }, { $push: { formData: req.body } }, { new: true })
                            .then(savedForm => {
                                console.log("Form---Found");
                                var orderId = Math.floor(1000000000 + Math.random() * 9000000000).toString();
                                //Check submitted form is a sub-event or an Event
                                if (req.body.parentFormId == "undefined") {
                                    console.log("Inside-----Event");
                                    /*
                                        Process Event functionality
                                        If submitted Event form contains payment then process to payment. 
                                    */
                                    if (savedForm.formConfig.includePayment == true) {
                                        var payObj = {
                                            merchant_id: "10292",
                                            order_id: orderId,
                                            currency: "INR",
                                             amount: req.body.eventPayment,
                                            //amount: "2.00",
                                            redirect_url: `${config.websiteUrl}/node/api/payment/ccavResponseHandler`,
                                            cancel_url: `${config.websiteUrl}/node/api/payment/ccavResponseHandler`,
                                            language: "EN",
                                            billing_name: req.body.firstName + " " + req.body.lastName,
                                            billing_address: req.body.address1 + " " + req.body.address2,
                                            billing_city: req.body.city,
                                            billing_state: req.body.state,
                                            billing_zip: req.body.postalCode,
                                            billing_country: req.body.country,
                                            billing_tel: req.body.mobileNumber,
                                            billing_email: req.body.email,
                                            delivery_name: req.body.firstName + " " + req.body.lastName,
                                            delivery_address: req.body.address1 + " " + req.body.address2,
                                            delivery_city: req.body.city,
                                            delivery_state: req.body.state,
                                            delivery_zip: req.body.postalCode,
                                            delivery_country: req.body.country,
                                            delivery_tel: req.body.mobileNumber,
                                            merchant_param1: req.body.formId,
                                            merchant_param2: req.body.email,
                                            merchant_param3: "additional + Info",
                                            merchant_param4: "additional + Info",
                                            merchant_param5: "additional + Info",
                                            promo_code: "",
                                            customer_identifier: ""
                                        }
                                        var body = '',
                                            workingKey = config.paymentGatewayWorkingKey,	//Put in the 32-Bit key shared by CCAvenues.
                                            accessCode = config.paymentGatewayAccessCode,			//Put in the Access Code shared by CCAvenues.
                                            encRequest = '',
                                            formbody = '';

                                        body = `merchant_id=${payObj.merchant_id}&order_id=${payObj.order_id}&currency=${payObj.currency}&amount=${payObj.amount}&redirect_url=${payObj.redirect_url}&cancel_url=${payObj.cancel_url}&language=${payObj.language}&billing_name=${payObj.billing_name}&billing_address=${payObj.billing_address}&billing_city=${payObj.billing_city}&billing_state=${payObj.billing_state}&billing_zip=${payObj.billing_zip}&billing_country=${payObj.billing_country}&billing_tel=${payObj.billing_tel}&billing_email=${payObj.billing_email}&delivery_name=${payObj.delivery_name}&delivery_address=${payObj.delivery_address}&delivery_city=${payObj.delivery_city}&delivery_state=${payObj.delivery_state}&delivery_zip=${payObj.delivery_zip}&delivery_country=${payObj.delivery_country}&delivery_tel=${payObj.delivery_tel}&merchant_param1=${payObj.merchant_param1}&merchant_param2=${payObj.merchant_param2}&merchant_param3=${payObj.merchant_param3}&merchant_param4=${payObj.merchant_param4}&merchant_param5=${payObj.merchant_param5}&promo_code=${payObj.promo_code}&customer_identifier=${payObj.customer_identifier}`;
                                        encRequest = encrypt(body, workingKey);
                                        formbody = `<form id="nonseamless" method="POST" name="redirect" action=${config.paymentGatewayURL}> <input type="hidden" id="encRequest" name="encRequest" value=${encRequest}><input type="hidden" name="access_code" id="access_code" value=${accessCode}><script language="javascript">document.redirect.submit();</script></form>`;
                                        res.writeHeader(200, { "Content-Type": "text/html" });
                                        res.write(formbody);
                                        res.end();
                                        return;
                                    } else {
                                        //If submitted event/sub-event form does'nt contains payment then notify user for successfull submission.
                                        var emailDataDT = {
                                            riderName: req.body.firstName + " " + req.body.lastName,
                                            eventName: savedForm.formConfig.sectionHeading,
                                            eventEmail: savedForm.formConfig.eventQueryEmail,
                                            merchandiseQueryEmail: savedForm.formConfig.merchandiseQueryEmail,
                                            phoneNo: req.body.phoneNo,
                                            address: "",
                                            amountPaid: savedForm.formConfig.paymentCurrency + " " + savedForm.formConfig.paymentAmount,
                                            orderId: orderId,
                                            riderEmail: req.body.email,
                                        };
                                        const localsDT = Object.assign({}, { data: emailDataDT });
                                        emailJoinRide.render(localsDT, (err, results) => {
                                            console.log(err);
                                            emailApi.sendEmailApi(
                                                "Join Ride Confirmation",
                                                "Thank you for Joining a ride",
                                                results.html,
                                                req.body.email,
                                                "error sending mail",
                                                "mail sent successfully"
                                            );
                                        });
                                        res.redirect(`${config.websiteUrl}/in/en/home/payment-successful.html?paymentSuccess=true&orderId=${orderId}&amountPaid=NIL&regType=NIL&riderName=${req.body.firstName + " " + req.body.lastName}`);
                                    }
                                } else {
                                    console.log("Inside---Sub---Event");
                                    /*  
                                        Process sub-event functionality
                                        If submitted Sub-event form contains payment then process to payment.
                                        Find Sub-event Data from parent-form-id 
                                    */
                                    var flag = false;
                                    var resMsg = {
                                        success: false,
                                        message: "Error Submitting Form."
                                    };
                                    DynamicFormSchema.findOne({ formId: req.body.parentFormId }).then((form) => {
                                        console.log("Form-Found");
                                        form.formData.forEach((item, index) => {
                                            console.log("COmparing-Form-Found");
                                            console.log(req.body);
                                            if (item.email == req.body.email && flag == false) {
                                                console.log("Form-Found-Match");
                                                flag = true;
                                                //Process Payment Functionality
                                                if (savedForm.formConfig.includePayment == true) {
                                                    console.log("============Sub-Event with payment========================================");
                                                    var payObj = {
                                                        merchant_id: "10292",
                                                        order_id: orderId,
                                                        currency: "INR",
                                                        amount: req.body.subEventPayment,
                                                        redirect_url: `${config.websiteUrl}/node/api/payment/ccavResponseHandler`,
                                                        cancel_url: `${config.websiteUrl}/node/api/payment/ccavResponseHandler`,
                                                        language: "EN",
                                                        billing_name: item.firstName + " " + item.lastName,
                                                        billing_address: item.address1 + " " + item.address2,
                                                        billing_city: item.city,
                                                        billing_state: item.state,
                                                        billing_zip: item.postalCode,
                                                        billing_country: item.country,
                                                        billing_tel: item.mobileNumber,
                                                        billing_email: item.email,
                                                        delivery_name: item.firstName + " " + item.lastName,
                                                        delivery_address: item.address1 + " " + item.address2,
                                                        delivery_city: item.city,
                                                        delivery_state: item.state,
                                                        delivery_zip: item.postalCode,
                                                        delivery_country: item.country,
                                                        delivery_tel: item.mobileNumber,
                                                        merchant_param1: item.formId,
                                                        merchant_param2: item.email,
                                                        merchant_param3: "additional + Info",
                                                        merchant_param4: "additional + Info",
                                                        merchant_param5: "additional + Info",
                                                        promo_code: "",
                                                        customer_identifier: ""
                                                    }

                                                    var body = '',
                                                        workingKey = config.paymentGatewayWorkingKey,	//Put in the 32-Bit key shared by CCAvenues.
                                                        accessCode = config.paymentGatewayAccessCode,			//Put in the Access Code shared by CCAvenues.
                                                        encRequest = '',
                                                        formbody = '';

                                                    body = `merchant_id=${payObj.merchant_id}&order_id=${payObj.order_id}&currency=${payObj.currency}&amount=${payObj.amount}&redirect_url=${payObj.redirect_url}&cancel_url=${payObj.cancel_url}&language=${payObj.language}&billing_name=${payObj.billing_name}&billing_address=${payObj.billing_address}&billing_city=${payObj.billing_city}&billing_state=${payObj.billing_state}&billing_zip=${payObj.billing_zip}&billing_country=${payObj.billing_country}&billing_tel=${payObj.billing_tel}&billing_email=${payObj.billing_email}&delivery_name=${payObj.delivery_name}&delivery_address=${payObj.delivery_address}&delivery_city=${payObj.delivery_city}&delivery_state=${payObj.delivery_state}&delivery_zip=${payObj.delivery_zip}&delivery_country=${payObj.delivery_country}&delivery_tel=${payObj.delivery_tel}&merchant_param1=${payObj.merchant_param1}&merchant_param2=${payObj.merchant_param2}&merchant_param3=${payObj.merchant_param3}&merchant_param4=${payObj.merchant_param4}&merchant_param5=${payObj.merchant_param5}&promo_code=${payObj.promo_code}&customer_identifier=${payObj.customer_identifier}`;
                                                    encRequest = encrypt(body, workingKey);
                                                    formbody = `<form id="nonseamless" method="POST" name="redirect" action=${config.paymentGatewayURL}> <input type="hidden" id="encRequest" name="encRequest" value=${encRequest}><input type="hidden" name="access_code" id="access_code" value=${accessCode}><script language="javascript">document.redirect.submit();</script></form>`;
                                                    res.writeHeader(200, { "Content-Type": "text/html" });
                                                    res.write(formbody);
                                                    res.end();
                                                    return;
                                                } else {
                                                    //If submitted sub-event form does'nt contains payment then notify user for successfull submission.
                                                    console.log("============Sub-Event without payment========================================");
                                                    var emailDataDT = {
                                                        riderName: item.firstName + " " + item.lastName,
                                                        eventName: "Sub-Event of " + form.formConfig.sectionHeading,
                                                        eventEmail: form.formConfig.eventQueryEmail,
                                                        merchandiseQueryEmail: form.formConfig.merchandiseQueryEmail,
                                                        phoneNo: item.mobileNumber,
                                                        address: item.address1 + " " + item.address2,
                                                        amountPaid: form.formConfig.paymentCurrency + " " + form.formConfig.paymentAmount,
                                                        orderId: orderId,
                                                        riderEmail: item.email,
                                                    };
                                                    const localsDT = Object.assign({}, { data: emailDataDT });
                                                    emailJoinRide.render(localsDT, (err, results) => {
                                                        console.log(err);
                                                        emailApi.sendEmailApi(
                                                            "Join Ride Confirmation",
                                                            "Thank you for Joining a ride",
                                                            results.html,
                                                            item.email,
                                                            "error sending mail",
                                                            "mail sent successfully"
                                                        );
                                                    });
                                                    var resMsgSubEvent = {
                                                        success: true,
                                                        containsPayment: false,
                                                        successUrl: `${config.websiteUrl}/in/en/home/payment-successful.html?paymentSuccess=true&orderId=${orderId}&amountPaid=NIL&regType=NIL&riderName=${item.firstName + " " + item.lastName}`,
                                                    }
                                                    res.send(resMsgSubEvent);
                                                }
                                            } else {
                                                console.log("Form-Found-NOT-Match");
                                                if ((form.formData.length - 1) == index) {
                                                    console.log("Form-Found-NOT-Match-END");
                                                    res.send(resMsg);
                                                }
                                            }
                                        });
                                    }).catch(error => {
                                        resMsg.message = "Error Submitting Form For Sub-Event"
                                        res.send(resMsg);
                                    })
                                }


                            }).catch(error => {
                                console.log(error);
                                resMsg.message = "Error Submitting form"
                                resMsg.data = `${config.websiteUrl}/in/en/home/payment-failed.html`;
                                res.send(resMsg);
                            })
                    }
                });
            } else {
                console.log("inside bada else");

                DynamicFormSchema.findOneAndUpdate({ formId: req.body.formId }, { $push: { formData: req.body } }, { new: true })
                    .then(savedForm => {
                        console.log("Form---Found");
                        var orderId = Math.floor(1000000000 + Math.random() * 9000000000).toString();
                        //Check submitted form is a sub-event or an Event
                        if (req.body.parentFormId == "undefined") {
                            console.log("Inside-----Event");
                            /*
                                Process Event functionality
                                If submitted Event form contains payment then process to payment. 
                            */
                            if (savedForm.formConfig.includePayment == true) {
                                var payObj = {
                                    merchant_id: "10292",
                                    order_id: orderId,
                                    currency: "INR",
                                    amount: req.body.eventPayment,
                                   // amount: "2.00",
                                    redirect_url: `${config.websiteUrl}/node/api/payment/ccavResponseHandler`,
                                    cancel_url: `${config.websiteUrl}/node/api/payment/ccavResponseHandler`,
                                    language: "EN",
                                    billing_name: req.body.firstName + " " + req.body.lastName,
                                    billing_address: req.body.address1 + " " + req.body.address2,
                                    billing_city: req.body.city,
                                    billing_state: req.body.state,
                                    billing_zip: req.body.postalCode,
                                    billing_country: req.body.country,
                                    billing_tel: req.body.mobileNumber,
                                    billing_email: req.body.email,
                                    delivery_name: req.body.firstName + " " + req.body.lastName,
                                    delivery_address: req.body.address1 + " " + req.body.address2,
                                    delivery_city: req.body.city,
                                    delivery_state: req.body.state,
                                    delivery_zip: req.body.postalCode,
                                    delivery_country: req.body.country,
                                    delivery_tel: req.body.mobileNumber,
                                    merchant_param1: req.body.formId,
                                    merchant_param2: req.body.email,
                                    merchant_param3: "additional + Info",
                                    merchant_param4: "additional + Info",
                                    merchant_param5: "additional + Info",
                                    promo_code: "",
                                    customer_identifier: ""
                                }

                                var body = '',
                                    workingKey = config.paymentGatewayWorkingKey,	//Put in the 32-Bit key shared by CCAvenues.
                                    accessCode = config.paymentGatewayAccessCode,			//Put in the Access Code shared by CCAvenues.
                                    encRequest = '',
                                    formbody = '';
                                body = `merchant_id=${payObj.merchant_id}&order_id=${payObj.order_id}&currency=${payObj.currency}&amount=${payObj.amount}&redirect_url=${payObj.redirect_url}&cancel_url=${payObj.cancel_url}&language=${payObj.language}&billing_name=${payObj.billing_name}&billing_address=${payObj.billing_address}&billing_city=${payObj.billing_city}&billing_state=${payObj.billing_state}&billing_zip=${payObj.billing_zip}&billing_country=${payObj.billing_country}&billing_tel=${payObj.billing_tel}&billing_email=${payObj.billing_email}&delivery_name=${payObj.delivery_name}&delivery_address=${payObj.delivery_address}&delivery_city=${payObj.delivery_city}&delivery_state=${payObj.delivery_state}&delivery_zip=${payObj.delivery_zip}&delivery_country=${payObj.delivery_country}&delivery_tel=${payObj.delivery_tel}&merchant_param1=${payObj.merchant_param1}&merchant_param2=${payObj.merchant_param2}&merchant_param3=${payObj.merchant_param3}&merchant_param4=${payObj.merchant_param4}&merchant_param5=${payObj.merchant_param5}&promo_code=${payObj.promo_code}&customer_identifier=${payObj.customer_identifier}`;
                                encRequest = encrypt(body, workingKey);
                                formbody = `<form id="nonseamless" method="POST" name="redirect" action=${config.paymentGatewayURL}> <input type="hidden" id="encRequest" name="encRequest" value=${encRequest}><input type="hidden" name="access_code" id="access_code" value=${accessCode}><script language="javascript">document.redirect.submit();</script></form>`;
                                res.writeHeader(200, { "Content-Type": "text/html" });
                                res.write(formbody);
                                res.end();
                                return;
                            } else {
                                //If submitted event/sub-event form does'nt contains payment then notify user for successfull submission.
                                var emailDataDT = {
                                    riderName: req.body.firstName + " " + req.body.lastName,
                                    eventName: savedForm.formConfig.sectionHeading,
                                    eventEmail: savedForm.formConfig.eventQueryEmail,
                                    merchandiseQueryEmail: savedForm.formConfig.merchandiseQueryEmail,
                                    phoneNo: req.body.phoneNo,
                                    address: "",
                                    amountPaid: savedForm.formConfig.paymentCurrency + " " + savedForm.formConfig.paymentAmount,
                                    orderId: orderId,
                                    riderEmail: req.body.email,
                                };
                                const localsDT = Object.assign({}, { data: emailDataDT });
                                emailJoinRide.render(localsDT, (err, results) => {
                                    console.log(err);
                                    emailApi.sendEmailApi(
                                        "Join Ride Confirmation",
                                        "Thank you for Joining a ride",
                                        results.html,
                                        req.body.email,
                                        "error sending mail",
                                        "mail sent successfully"
                                    );
                                });
                                res.redirect(`${config.websiteUrl}/in/en/home/payment-successful.html?paymentSuccess=true&orderId=${orderId}&amountPaid=NIL&regType=NIL&riderName=${req.body.firstName + " " + req.body.lastName}`);
                            }
                        } else {
                            console.log("Inside---Sub---Event");
                            /*  
                                Process sub-event functionality
                                If submitted Sub-event form contains payment then process to payment.
                                Find Sub-event Data from parent-form-id 
                            */
                            var flag = false;
                            var resMsg = {
                                success: false,
                                message: "Error Submitting Form."
                            };
                            DynamicFormSchema.findOne({ formId: req.body.parentFormId }).then((form) => {
                                console.log("Form-Found");
                                form.formData.forEach((item, index) => {
                                    console.log("COmparing-Form-Found");
                                    console.log(req.body);
                                    if (item.email == req.body.email && flag == false) {
                                        console.log("Form-Found-Match");
                                        flag = true;
                                        //Process Payment Functionality
                                        if (savedForm.formConfig.includePayment == true) {
                                            console.log("============Sub-Event with payment========================================");
                                            var payObj = {
                                                merchant_id: "10292",
                                                order_id: orderId,
                                                currency: "INR",
                                                amount: req.body.subEventPayment,
                                                redirect_url: `${config.websiteUrl}/node/api/payment/ccavResponseHandler`,
                                                cancel_url: `${config.websiteUrl}/node/api/payment/ccavResponseHandler`,
                                                language: "EN",
                                                billing_name: item.firstName + " " + item.lastName,
                                                billing_address: item.address1 + " " + item.address2,
                                                billing_city: item.city,
                                                billing_state: item.state,
                                                billing_zip: item.postalCode,
                                                billing_country: item.country,
                                                billing_tel: item.mobileNumber,
                                                billing_email: item.email,
                                                delivery_name: item.firstName + " " + item.lastName,
                                                delivery_address: item.address1 + " " + item.address2,
                                                delivery_city: item.city,
                                                delivery_state: item.state,
                                                delivery_zip: item.postalCode,
                                                delivery_country: item.count,
                                                delivery_tel: item.mobileNumber,
                                                merchant_param1: item.formId,
                                                merchant_param2: item.email,
                                                merchant_param3: "additional + Info",
                                                merchant_param4: "additional + Info",
                                                merchant_param5: "additional + Info",
                                                promo_code: "",
                                                customer_identifier: ""
                                            }

                                            var body = '',
                                                workingKey = config.paymentGateway,	//Put in the 32-Bit key shared by CCAvenues.
                                                accessCode = config.paymentGatewayAccessCode,			//Put in the Access Code shared by CCAvenues.
                                                encRequest = '',
                                                formbody = '';

                                            body = `merchant_id=${payObj.merchant_id}&order_id=${payObj.order_id}&currency=${payObj.currency}&amount=${payObj.amount}&redirect_url=${payObj.redirect_url}&cancel_url=${payObj.cancel_url}&language=${payObj.language}&billing_name=${payObj.billing_name}&billing_address=${payObj.billing_address}&billing_city=${payObj.billing_city}&billing_state=${payObj.billing_state}&billing_zip=${payObj.billing_zip}&billing_country=${payObj.billing_country}&billing_tel=${payObj.billing_tel}&billing_email=${payObj.billing_email}&delivery_name=${payObj.delivery_name}&delivery_address=${payObj.delivery_address}&delivery_city=${payObj.delivery_city}&delivery_state=${payObj.delivery_state}&delivery_zip=${payObj.delivery_zip}&delivery_country=${payObj.delivery_country}&delivery_tel=${payObj.delivery_tel}&merchant_param1=${payObj.merchant_param1}&merchant_param2=${payObj.merchant_param2}&merchant_param3=${payObj.merchant_param3}&merchant_param4=${payObj.merchant_param4}&merchant_param5=${payObj.merchant_param5}&promo_code=${payObj.promo_code}&customer_identifier=${payObj.customer_identifier}`;
                                            encRequest = encrypt(body, workingKey);
                                            formbody = `<form id="nonseamless" method="POST" name="redirect" action=${config.paymentGatewayURL}> <input type="hidden" id="encRequest" name="encRequest" value=${encRequest}><input type="hidden" name="access_code" id="access_code" value=${accessCode}><script language="javascript">document.redirect.submit();</script></form>`;
                                            res.writeHeader(200, { "Content-Type": "text/html" });
                                            res.write(formbody);
                                            res.end();
                                            return;
                                        } else {
                                            //If submitted sub-event form does'nt contains payment then notify user for successfull submission.
                                            console.log("============Sub-Event without payment========================================");
                                            var emailDataDT = {
                                                riderName: item.firstName + " " + item.lastName,
                                                eventName: "Sub-Event of " + form.formConfig.sectionHeading,
                                                eventEmail: form.formConfig.eventQueryEmail,
                                                merchandiseQueryEmail: form.formConfig.merchandiseQueryEmail,
                                                phoneNo: item.mobileNumber,
                                                address: item.address1 + " " + item.address2,
                                                amountPaid: savedForm.formConfig.paymentCurrency + " " + req.body.subEventPayment,
                                                orderId: orderId,
                                                riderEmail: item.email,
                                            };
                                            const localsDT = Object.assign({}, { data: emailDataDT });
                                            emailJoinRide.render(localsDT, (err, results) => {
                                                console.log(err);
                                                emailApi.sendEmailApi(
                                                    "Join Ride Confirmation",
                                                    "Thank you for Joining a ride",
                                                    results.html,
                                                    item.email,
                                                    "error sending mail",
                                                    "mail sent successfully"
                                                );
                                            });
                                            var resMsgSubEvent = {
                                                success: true,
                                                containsPayment: false,
                                                successUrl: `${config.websiteUrl}/in/en/home/payment-successful.html?paymentSuccess=true&orderId=${orderId}&amountPaid=NIL&regType=NIL&riderName=${item.firstName + " " + item.lastName}`,
                                            }
                                            res.send(resMsgSubEvent);
                                        }
                                    } else {
                                        console.log("Form-Found-NOT-Match");
                                        if ((form.formData.length - 1) == index) {
                                            console.log("Form-Found-NOT-Match-END");
                                            res.send(resMsg);
                                        }
                                    }
                                });
                            }).catch(error => {
                                resMsg.message = "Error Submitting Form For Sub-Event"
                                res.send(resMsg);
                            })
                        }
                    }).catch(error => {
                        console.log(error);
                        resMsg.message = "Error Submitting form"
                        resMsg.data = `${config.websiteUrl}/in/en/home/payment-failed.html`;
                        res.send(resMsg);
                    })
            }
        })



}

function verifyPayment(req, res, next) {
    var token = req.body.registrationNumber;
    var resMsg = {
        success: false,
        message: "Error validating token"
    };
    var flag = false;
    DynamicFormSchema.findOne({ formId: req.body.parentFormId }).then((form) => {
        form.formData.forEach((item, index) => {
            if (item.paymentToken == token && flag == false) {
                resMsg.success = true;
                flag = true;
                resMsg.message = "Token verification successfull";
                resMsg.data = item;
                res.send(resMsg);
            } else {
                if ((form.formData.length - 1) == index) {
                    res.send(resMsg);
                }
            }
        });
    }).catch(error => {
        resMsg.message = "Error validating token"
        res.send(resMsg);
    })

}



//Route for validating whether registrations are closed 
function validatingDynamicForms(req,res,next){
console.log(req.body)
var resObj = {
	status : 200,
	message : "",
}
res.send(resObj);
}

export default {
    submitDataToForm,
    verifyPayment,
    createDynamicForm,
    newSubmitDataToForm,
    validatingDynamicForms
};


