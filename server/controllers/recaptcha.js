import Recaptcha from '../models/recaptcha';
import request from 'request';
import config from '../../config/env';

function recaptchastore(req, res, next) {
  req.on("data", function (buffer) {
    var obj = JSON.parse(buffer);
    var randomnumber = obj.randomnumber;
    var publickey = obj.publickey;
    var privatekey = obj.privatekey;

    var recaptcha = new Recaptcha({
      Randomnumber: randomnumber,
      publickey: publickey,
      privatekey: privatekey
    });

    recaptcha
      .save()
      .then((doc) => {
      }, (e) => {
        console.log(e);
      });

    res.send("recaptchastore");
  });
}

function recaptchaverify(req, res, next) {
  if (
    req.body.captcha === undefined ||
    req.body.captcha === '' ||
    req.body.captcha === null
  ) {
    return res.json({ "success": false, "msg": "Please select captcha" });
  }
  //Secret Key
  const secretKey = config.recaptchaSecretKey;

  // Verify URL
  const verifyUrl = `https://google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}&remoteip=${req.connection.remoteAddress}`;

  // Make Request To VerifyURL
  request(verifyUrl, (err, response, body) => {
    body = JSON.parse(body);
    res.send({ msg: "Verified" });
    // If Not Successful
    if (body.success !== undefined && !body.success) {
      res.status(500).send({ msg: "Recaptha not Verified" })
    }
  });
}

export default {

  recaptchaverify,
  recaptchastore

};
