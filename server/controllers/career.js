import careerSchema from "../models/career";
import multer from 'multer';
import { EmailTemplate } from "email-templates";
import path from "path";
import nodemailer from 'nodemailer';
import config from "../../config/env";
import smtpTransport from 'nodemailer-smtp-transport';
import mailClient from 'mail';

const emailDir = path.resolve(__dirname, "../templates/mailers", "careers-submit-resume");
const emailCareer = new EmailTemplate(path.join(emailDir));

function createCareer(req, res, next) {
    var language = req.headers['x-custom-language'];
    var country = req.headers['x-custom-country'];
    var filePath, fileName;
    var arrayFilePath = [];
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './node/assets/cv');
        },
        filename: function (req, file, cb) {
            fileName = Date.now() + file.originalname;
            cb(null, fileName);
            filePath = './node/assets/cv' + fileName;
        }
    });
    var returnObj = {};

    const uploadObj = multer({ storage: storage }).single('cv');
    uploadObj(req, res, (err) => {
        const newcareer = new careerSchema({
            locality: {
                country: country,
                language: language
            },
            fname: req.body.fname,
            lname: req.body.lname,
            email: req.body.email,
            phone: req.body.phone,
            age: req.body.age,
            yearOfExperience: req.body.yearOfExperience,
            highestQualification: req.body.highestQualification,
            institute: req.body.institute,
            secondaryQualification: req.body.secondaryQualification,
            currentDesignation: req.body.currentDesignation,
            keySkills: req.body.keySkills,
            linkedIn: req.body.linkedIn,
            industry: req.body.Industry,
            functionApplyingFor: req.body.functionApplyingFor,
            cvPath: req.file.path
        });

        // console.log('============req.body========================');
        // console.log(req.body);
        // console.log('========req.body============================');
        newcareer.save().then((savedCareer) => {
            // console.log(savedCareer);
            var emailData = {
                "name": savedCareer.fname + " " + savedCareer.lname,
                "email": savedCareer.email,
                "mobile": savedCareer.phone,
                "age": savedCareer.age,
                "yearOfExperience": savedCareer.yearOfExperience,
                "highestQualification": savedCareer.highestQualification,
                "secondaryQualification": savedCareer.secondaryQualification,
                "Industry": savedCareer.industry,
                "functionApplyingFor": savedCareer.functionApplyingFor,
                "keySkills": savedCareer.keySkills,
                "linkedIn": savedCareer.linkedIn,
                "institute": savedCareer.institute,
                "url": config.websiteUrl
            }

            var attachments = [{
                filename: savedCareer.fname + "-CV",
                path: savedCareer.cvPath,
                contentType: 'application/pdf'
            }]

            const locals = Object.assign({}, { data: emailData });

            emailCareer.render(locals, (err, results) => {
                // console.log(results);
                var transporter = nodemailer.createTransport(config.email);
                let mailOptions = {
                    from: config.emailSender, // sender address
                    // to: "sanjay.sharma@techchefz.com", // list of receivers
                    to: req.body.authorEmail, // list of receivers
                    subject: `Application to ${savedCareer.functionApplyingFor} - ${savedCareer.industry} Confirmation`, // Subject line
                    html: results.html, // html body
                    attachments: attachments
                };

                transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        console.log(error)
                    }

                    console.log(info);
                })
            })
            returnObj.success = true;
            returnObj.message = "Submitted Successfully";
            returnObj.data = savedCareer;
            res.send(returnObj);
        }).catch((e) => {
            console.log(e);
            res.success = false;
            res.message = "Something Went Wrong!";
            res.data = {};
            res.send(returnObj);
        });
    });
}


export default {
    createCareer
}
