import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import UserSchema from '../models/user';

const crypto = require('crypto');
const { OAuth2Client } = require('google-auth-library');
var google = require('googleapis');
var request = require('request');
var twitterAPI = require('node-twitter-api');

/**
 * Returns jwt token  and user object if valid email and password is provided
 * @param req (email, password, userType)
 * @param res
 * @param next
 * @returns {jwtAccessToken, user}
 */
function login(req, res, next) {
  var retObj = {
    success: true,
    message: '',
    data: {},
  };
  const userObj = {
    email: req.body.email,
    userType: req.body.userType,
  };
  const phoneObj = {
    phoneNo: req.body.email,
    userType: req.body.userType,
  };

  UserSchema.findOne({ $or: [userObj, phoneObj] }, '+password').then((user) => {
    if (!user) {
      const err = new APIError('User not found with the given email id', httpStatus.NOT_FOUND);
      return next(err);
    } else {
      user.comparePassword(req.body.password, (passwordError, isMatch) => {
        if (passwordError || !isMatch) {
          retObj.success = false;
          retObj.message = 'Incorrect Password';
          res.json(retObj);
        } else {
          user.loginStatus = true;
          UserSchema.findOneAndUpdate({ _id: user._id }, { $set: user }, { new: true }).select({
            password: 0,
            addressInfo: 0,
            mobileVerified: 0,
            emailVerified: 0,
            otp: 0,
            emailToken: 0,
            ownedBikeInfo: 0,
            userType: 0,
            loginStatus: 0,
            jwtAccessToken: 0,
          }).then((updatedUser) => {
            const token = jwt.sign(updatedUser, config.jwtSecret, { expiresIn: "7d" });
            var userObj = { email: updatedUser.email,verifiedAccount :updatedUser.verifiedAccount , profilePicture: { srcPath: updatedUser.profilePicture }, userId: updatedUser._id, phone: updatedUser.phoneNo, firstName: updatedUser.fname, lastName: updatedUser.lname };
            retObj.success = true;
            retObj.message = 'user successfully logged in';
            retObj.data.jwtAccessToken = `JWT ${token}`;
            retObj.data.user = userObj;
            res.json(retObj);
          }).catch((error) => {
            const err = new APIError(`error in updating user details while login ${error}`, httpStatus.INTERNAL_SERVER_ERROR);
            next(err);
          });
        }
      });

    }
  }).catch((e) => {
    console.log(e);
    const err = new APIError(`erro while finding user ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
    next(err);
  });
}

function facebookSignup(req, res, next) {
  var retObj = {
    status: false,
    userObj: null,
    message: null
  };

  var accessToken = req.body.AccessToken;
  var clientSecret = config.facebookclientsecret;
  var appsecret_proof = crypto.createHmac('sha256', clientSecret).update(accessToken).digest('hex');
  var verifyUrl = `https://graph.facebook.com/v3.2/me?fields=id,email,first_name,last_name,picture,gender&access_token=${accessToken}&appsecret_proof=${appsecret_proof}`;
  request(verifyUrl, (err, response, body) => {
    var responseBody = JSON.parse(body);
    UserSchema.findOne({ email: responseBody.email }).select({
      password: 0,
      addressInfo: 0,
      mobileVerified: 0,
      emailVerified: 0,
      otp: 0,
      emailToken: 0,
      ownedBikeInfo: 0,
      userType: 0,
      loginStatus: 0,
      jwtAccessToken: 0,
    }).then((UserObj) => {
      if (UserObj == null) {
        retObj.status = true,
          retObj.userObj = responseBody,
          retObj.message = null;
        res.send(retObj);
      } else if (UserObj.email === responseBody.email) {
        retObj.status = false;
        retObj.userObj = null;
        retObj.message = "User Already Exists !";
        res.send(retObj);
      } else {
        retObj.status = false;
        retObj.userObj = null;
        retObj.message = "Error Creating User, Please Try again !";
      }
    }, (err) => {
      console.log(err);
    })
  });
}


function googleSignup(req, res, next) {
  var retObj = {
    status: false,
    userObj: null,
    message: null
  };
  const ClientId = config.googleclientid;
  const ClientSecret = config.googleclientsecret;
  function getOAuthClient() {
    return new OAuth2Client(ClientId, ClientSecret, config.googleredirecturi);
  }

  var oauth2Client = getOAuthClient();
  var code = req.body.authCode;
  oauth2Client.getToken(code, function (err, tokens) {
    // Now tokens contains an access_token and an optional refresh_token. Save them.
    if (!err) {
      oauth2Client.setCredentials(tokens);
      var token = tokens.id_token;
      var verifyUrl = `https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${token}`;
      request(verifyUrl, (err, response, body) => {
        var responseBody = JSON.parse(body);
        UserSchema.findOne({ email: responseBody.email }).select({
          password: 0,
          addressInfo: 0,
          mobileVerified: 0,
          emailVerified: 0,
          otp: 0,
          emailToken: 0,
          ownedBikeInfo: 0,
          userType: 0,
          loginStatus: 0,
          jwtAccessToken: 0,
        }).then((UserObj) => {
          if (UserObj == null) {
            retObj.status = true,
              retObj.userObj = responseBody,
              retObj.message = null;
            res.send(retObj);
          } else if (UserObj.email === responseBody.email) {
            retObj.status = false;
            retObj.userObj = null;
            retObj.message = "User Already Exists !";
            res.send(retObj);
          } else {
            retObj.status = false;
            retObj.userObj = null;
            retObj.message = "Error Creating User, Please Try again !";
            res.send(retObj);
          }
        }, (err) => {
          console.log(err);
        })
      });
    }
    else {
      console.log(err);
    }
  });
}

function facebookLogin(req, res, next) {
  var retObj = {
    status: true,
    message: '',
    data: {},
  };
  var accessToken = req.body.obj.accessToken;
  var clientSecret = config.facebookclientsecret;
  var appsecret_proof = crypto.createHmac('sha256', clientSecret).update(accessToken).digest('hex');
  var verifyUrl = `https://graph.facebook.com/v3.2/me?fields=id,email,first_name,last_name,picture,gender&access_token=${accessToken}&appsecret_proof=${appsecret_proof}`;
  request(verifyUrl, (err, response, body) => {
    var responseBody = JSON.parse(body);
    UserSchema.findOne({ email: responseBody.email }).select({
      password: 0,
      addressInfo: 0,
      mobileVerified: 0,
      emailVerified: 0,
      otp: 0,
      emailToken: 0,
      ownedBikeInfo: 0,
      userType: 0,
      loginStatus: 0,
      jwtAccessToken: 0,
    }).then((UserObj) => {
      if (UserObj == null) {
        retObj.status = false;
        retObj.user = null;
        retObj.message = "User Doesn't Exists !";
        res.send(retObj);
      } else if (UserObj.email === responseBody.email) {
        retObj.status = true;
        retObj.message = "Logged in Successfully";
        const jwtAccessToken = jwt.sign(UserObj, config.jwtSecret);
        retObj.data.jwtAccessToken = `JWT ${jwtAccessToken}`;
        var obj = { email: UserObj.email,verifiedAccount : UserObj.verifiedAccount, profilePicture: { srcPath: UserObj.profilePicture }, userId: UserObj._id, phone: UserObj.phoneNo, firstName: UserObj.fname, lastName: UserObj.lname };
        retObj.data.user = obj;
        res.send(retObj);
      } else {
        retObj.status = false;
        retObj.user = null;
        retObj.message = "Error !, Please Try again !";
        res.send(retObj);
      }
    }, (err) => {
      console.log(err);
    })
  });
}


function googleLogin(req, res, next) {
  var retObj = {
    success: true,
    message: '',
    data: {},
  };
  
  const ClientId = config.googleclientid;
  const ClientSecret = config.googleclientsecret;
  function getOAuthClient() {
    return new OAuth2Client(ClientId, ClientSecret, config.googleredirecturi);

  }
  var oauth2Client = getOAuthClient();
  var code = req.body.authCode;
  oauth2Client.getToken(code, function (err, tokens) {
    // Now tokens contains an access_token and an optional refresh_token. Save them.
    if (!err) {
      oauth2Client.setCredentials(tokens);
      var token = tokens.id_token;
      var verifyUrl = `https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${token}`;
      request(verifyUrl, (err, response, body) => {
        var responseBody = JSON.parse(body);
        UserSchema.findOne({ email: responseBody.email }).select({
          password: 0,
          addressInfo: 0,
          mobileVerified: 0,
          emailVerified: 0,
          otp: 0,
          emailToken: 0,
          ownedBikeInfo: 0,
          userType: 0,
          loginStatus: 0,
          jwtAccessToken: 0,
        }).then((UserObj) => {
          if (UserObj == null) {
            retObj.status = false;
            retObj.user = null;
            retObj.message = "User Doesn't Exists !";
            res.send(retObj);
          } else if (UserObj.email === responseBody.email) {
            retObj.status = true;
            retObj.message = "Logged in Successfully";
            const jwtAccessToken = jwt.sign(UserObj, config.jwtSecret);
            retObj.data.jwtAccessToken = `JWT ${jwtAccessToken}`;
            var obj = { email: UserObj.email,verifiedAccount : UserObj.verifiedAccount, profilePicture: { srcPath: UserObj.profilePicture }, userId: UserObj._id, phone: UserObj.phoneNo, firstName: UserObj.fname, lastName: UserObj.lname };
            retObj.data.user = obj;
            res.send(retObj);
          } else {
            retObj.status = false;
            retObj.user = null;
            retObj.message = "Error !, Please Try again !";
            res.send(retObj);
          }
        }, (err) => {
          console.log(err);
        })
      });
    }
    else {
      console.log(err);
    }
  });
}


var twitter = new twitterAPI({
  
//  consumerKey: config.twitterconfig.consumerKey,
//  consumerSecret: config.twitterconfig.consumerSecret,
  callback: config.twitterconfig.callbacksignup,

  //Royal-Enfield
   consumerKey:'fGxqYNW0wmHGCkANFWJWuYl59',
   consumerSecret:'KHP5fa20Ql4eZqRIoNxB47MHZhMkZgMFxHpO8BUC7fzvSQzl1R',
   //callback: 'https://prod.royalenfield.com/in/en/'
});
var _requestSecret;
function getTwitterRequestToken(req, res, next) {
  twitter.getRequestToken(function (error, requestToken, requestTokenSecret, results) {
    if (error) {
      console.log("Error getting OAuth request token : " + error);
      res.send(error);
    } else {
      _requestSecret = requestTokenSecret;
      res.redirect("https://api.twitter.com/oauth/authenticate?oauth_token=" + requestToken);
      //store token and tokenSecret somewhere, you'll need them later; redirect user
    }
  });
}

function getTwitterAccessToken(req, res, next) {
  var token = req.query.oauth_token;
  var verifier = req.query.oauth_verifier;
  twitter.getAccessToken(token, _requestSecret, verifier, function (error, accessToken, accessTokenSecret, data) {
    if (error) {
      console.log(error);
    } else {
      //store accessToken and accessTokenSecret somewhere (associated to the user)
      //Step 4: Verify Credentials belongs here
      twitter.verifyCredentials(accessToken, accessTokenSecret, { include_email: true, skip_status: true }, function (error, user, response) {
        if (error) {
          //something was wrong with either accessToken or accessTokenSecret
          //start over with Step 1
          console.log(error);
        } else {
          UserSchema.findOne({ email: user.email }).select({
            password: 0,
            addressInfo: 0,
            mobileVerified: 0,
            emailVerified: 0,
            otp: 0,
            emailToken: 0,
            ownedBikeInfo: 0,
            userType: 0,
            loginStatus: 0,
            jwtAccessToken: 0,
          }).then((UserObj) => {
            var retObj = {};
            if (UserObj == null) {
              retObj.status = true,
                retObj.userObj = user,
                retObj.message = null;
              res.send(user);
            } else if (UserObj.email === user.email) {
              retObj.status = false;
              retObj.userObj = null;
              retObj.message = "User Already Exists !";
              res.send(retObj);
            } else {
              retObj.status = false;
              retObj.userObj = null;
              retObj.message = "Error Creating User, Please Try again !";
            }
          }, (err) => {
            console.log(err);
          })

          //accessToken and accessTokenSecret can now be used to make api-calls (not yet implemented)
          //data contains the user-data described in the official Twitter-API-docs
          //you could e.g. display his screen_name
          // res.send(user)
        }
      });
    }
  });
}

var login1 = 'login';
var twitter2 = new twitterAPI({
  //consumerKey: config.twitterconfig.consumerKey,
  //consumerSecret: config.twitterconfig.consumerSecret,
	consumerKey : "fGxqYNW0wmHGCkANFWJWuYl59",
	consumerSecret : "KHP5fa20Ql4eZqRIoNxB47MHZhMkZgMFxHpO8BUC7fzvSQzl1R",
  callback: config.twitterconfig.callbackLogin
});
var _requestSecret1;


function getTwitterRequestTokenLogIn(req, res, next) {
  twitter2.getRequestToken(function (error, requestToken, requestTokenSecret, results) {
    if (error) {
      console.log("Error getting OAuth request token : " + error);
      res.send(error);
    } else {
      _requestSecret1 = requestTokenSecret;
      res.redirect("https://api.twitter.com/oauth/authenticate?oauth_token=" + requestToken);
      //store token and tokenSecret somewhere, you'll need them later; redirect user
    }
  });
}

function getTwitterAccessTokenLogIn(req, res, next) {
  var token = req.query.oauth_token;
  var verifier = req.query.oauth_verifier;
  twitter2.getAccessToken(token, _requestSecret1, verifier, function (error, accessToken, accessTokenSecret, data) {
    if (error) {
      console.log(error);
    } else {
      //store accessToken and accessTokenSecret somewhere (associated to the user)
      //Step 4: Verify Credentials belongs here
      twitter2.verifyCredentials(accessToken, accessTokenSecret, { include_email: true, skip_status: true }, function (error, user, response) {
        if (error) {
          //something was wrong with either accessToken or accessTokenSecret
          //start over with Step 1
          console.log("Verification Error");
          console.log(error);
        } else {
          UserSchema.findOne({ email: user.email }).select({
            password: 0,
            addressInfo: 0,
            mobileVerified: 0,
            emailVerified: 0,
            otp: 0,
            emailToken: 0,
            ownedBikeInfo: 0,
            userType: 0,
            loginStatus: 0,
            jwtAccessToken: 0,
          }).then((UserObj) => {
            var retObj = {};
            retObj.data = {};
            if (UserObj == null) {
              retObj.status = false;
              retObj.user = null;
              retObj.message = "User Doesn't Exists !";
              res.send(retObj);
            } else if (UserObj.email === user.email) {
              console.log('sda');
              retObj.status = true;
              retObj.message = "Logged in Successfully";
              const jwtAccessToken = jwt.sign(UserObj, config.jwtSecret);
              retObj.data.jwtAccessToken = `JWT ${jwtAccessToken}`;
              var obj = { email: UserObj.email,verifiedAccount : UserObj.verifiedAccount, profilePicture: { srcPath: UserObj.profilePicture }, userId: UserObj._id, phone: UserObj.phoneNo, firstName: UserObj.fname, lastName: UserObj.lname };
              retObj.data.user = obj;
              res.send(retObj);
            } else {
              retObj.status = false;
              retObj.user = null;
              retObj.message = "Error !, Please Try again !";
              res.send(retObj);
            }
          }, (err) => {
            console.log(err);
          })
        }
      });
    }
  });
}

function verifyUserAccount(req,res,next){
UserSchema.findOne({_id : req.body.userId}).then((userFound)=>{
console.log(userFound.otp)
if(req.body.otp == userFound.otp){
	userFound.verifiedAccount.status = "Verified"
	userFound.save().then(()=>{
			res.send({status :200 , message :"Verification Successful"})
				})		
}else{
	res.send({status : 500  , message : "Otp not matched"})		
}
}).catch((err)=>{
console.log(err)
})
}


function sendingOtp(req,res,next){
var phoneNo = req.body.phoneNo;
var returnObj = {};
console.log(phoneNo)
  if (phoneNo) {
    UserSchema.findOne({ phoneNo: phoneNo })
      .then(user => {
	console.log(user)
        if (user) {
          const otpValue = Math.floor(100000 + Math.random() * 900000);
          user.otp = otpValue;
          user.save();
          var finalNumber;
          var num = phoneNo.slice(-10);
          var country = req.headers["x-custom-country"];
          switch (country) {
            case "in":
              finalNumber = "91" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    // returnObj.data = finalNumber
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "au":
              finalNumber = "61" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "us":
              finalNumber = "1" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "uk":
              finalNumber = "44" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "ar":
              finalNumber = "54" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "ca":
              finalNumber = "1" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "mx":
              finalNumber = "52" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "co":
              finalNumber = "57" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "es":
              finalNumber = "34" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "br":
              finalNumber = "55" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "pt":
              finalNumber = "351" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "th":
              finalNumber = "66" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "id":
              finalNumber = "62" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "fr":
              finalNumber = "33" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "it":
              finalNumber = "39" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "de":
              finalNumber = "49" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
            case "vn":
              finalNumber = "84" + num;
              request(
                `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                (err, response, body) => {
                  if (err) {
                    returnObj.success = false;
                    returnObj.message = "error sending otp";
                    returnObj.data = finalNumber;
                    res.send(returnObj);
                  } else {
                    returnObj.success = true;
                    returnObj.message = "otp sent successfully";
                    res.status(200).send(returnObj);
                    setTimeout(() => {
                      user.otp = "";
                    }, config.otpExipryTime);
                  }
                }
              );
              break;
          }
        } else {
          returnObj.success = false;
          returnObj.message = "user Not found";
          res.status(404).send(returnObj);
        }
      })
      .catch(e => {
        console.log(e);
        res.send("something went wrong");
      });
  }

}

export default
  {
    getTwitterRequestTokenLogIn,
    getTwitterAccessTokenLogIn,
    login,
    facebookSignup,
    googleSignup,
    googleLogin,
    facebookLogin,
    getTwitterRequestToken,
    getTwitterAccessToken,
verifyUserAccount,
sendingOtp
  };

