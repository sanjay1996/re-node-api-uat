import OwnersManual from '../models/owners-manual';

function create(req, res, next) {
    var language = req.headers['x-custom-language'];
    var country = req.headers['x-custom-country'];


    const ownersManual = new OwnersManual({
        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        city: req.body.city,
        locality: {
            country: country,
            language: language
        }
    });

    ownersManual.saveAsync().then((doc) => {
        res.send('saved successfully');
    }).catch(err => {
        res.send(err)
    });
}

export default {
    create
}