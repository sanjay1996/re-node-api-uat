import Reviews from '../models/reviews';
import mongoose from 'mongoose';
import moment from 'moment';
import BikeSchema from '../models/bike';
import User from '../models/user';
mongoose.Promise = global.Promise;

function create(req, res, next) {
var language = req.headers['x-custom-language'];
  var country = req.headers['x-custom-country'];
	if (req.body.reviewEntityId === '') {
		var retObj = {}
		retObj.msg = 'Bike Not Selected'
		retObj.status = 200
		res.send(retObj)
	}
	else {
		var a = req.body.performance;
		var b = req.body.handling;
		var c = req.body.style;
		var d = req.body.ownershipexperience;
		var e = req.body.verstality;
		var sum = Number(a) + Number(b) + Number(c) + Number(d) + Number(e);
		var averageRating = sum / 5;

		var averageRatingPercentage = averageRating * 20;
		var review = new Reviews({
			locality: {
        		country: country,
        		language: language
    				},
			reviewEntityId: req.body.reviewEntityId,
			reviewByUser: req.body.reviewByUser,
			reviewDescription: req.body.reviewDescription,
			reviewText: req.body.reviewText,


			averageRating: averageRating,
			averageRatingPercentage: averageRatingPercentage,
			reviewCriterias: {

				performance:
				{

					reviewCriteria: 'performance',
					reviewCriteriaLabel: 'Performance',
					reviewRating: req.body.performance,
					reviewRatingPercentage: req.body.performance * 20,
				},

				handling:
				{

					reviewCriteria: 'handling',
					reviewCriteriaLabel: 'Handling',
					reviewRating: req.body.handling,
					reviewRatingPercentage: req.body.handling * 20,

				},
				style:
				{

					reviewCriteria: 'style',
					reviewCriteriaLabel: 'Style',
					reviewRating: req.body.style,
					reviewRatingPercentage: req.body.style * 20,

				},
				versatality: {
					reviewCriteria: 'verstality',
					reviewCriteriaLabel: 'Versatality',
					reviewRating: req.body.verstality,
					reviewRatingPercentage: req.body.verstality * 20,

				},
				ownershipExperience:
				{

					reviewCriteria: 'ownershipexperience',
					reviewCriteriaLabel: 'OwnershipExperience',
					reviewRating: req.body.ownershipexperience,
					reviewRatingPercentage: req.body.ownershipexperience * 20,

				}
			}



		});

		review
			.save()
			.then((savedReview) => {

				User
					.findOne({ _id: req.body.reviewByUser })
					.then((user) => {

						user.reviewCreated.push(savedReview._id);
						user.save();

					});

				BikeSchema
					.findOne({ bikeName: req.body.reviewEntityId })
					.then((bikeFound) => {
						if(bikeFound){
						bikeFound.ReviewId.push(savedReview._id);
						bikeFound
							.save();
							}
						res.send({ success: true, status: 200, msg: 'Review Successfully Submitted' });
					})
					.catch((e) => { console.log(e) });
			}, (err) => {
				res.send(err);
			});
	}
}


// for  detial listing-------------------------------------------------------------------------------------
function fetch(req, res, next) {

var language = req.headers['x-custom-language'];
  var country = req.headers['x-custom-country'];
//console.log("==================")
//console.log(req.body)
//console.log(language)
//console.log(country);
//console.log("===================")
	var reviewEntityId = req.body.obj.reviewEntityId;
if(reviewEntityId != null || reviewEntityId != "undefined"){
	Reviews
		.aggregate([
			{ $match: { reviewEntityId: reviewEntityId , 'locality.language' : language,'locality.country' : country} },
			{
				$group:
				{
					_id: 'reviewEntityId',
					Count: { $sum: 1 },
					averagePerformance: { $avg: '$reviewCriterias.performance.reviewRating' },
					averagehandling: { $avg: '$reviewCriterias.handling.reviewRating' },
					averageownershipExperience: { $avg: '$reviewCriterias.ownershipExperience.reviewRating' },
					averageverstality: { $avg: '$reviewCriterias.versatality.reviewRating' },
					averagestyle: { $avg: '$reviewCriterias.style.reviewRating' },
				}
			}
		])
		.then((doc) => {
//		console.log("=========length==============")
	//	console.log(doc[0].Count)
//		console.log("===========length================")
			
			if(doc.length != 0){
			var performancerating = doc[0].averagePerformance;
			var performanceratingpercentage = performancerating * 20;

			//    //	------------------------
			var stylerating = doc[0].averagestyle;
			var stylepercentage = stylerating * 20;


			//    //---------------------------------	
			var verstalityrating = doc[0].averageverstality;
			var verstalityratingpercentage = verstalityrating * 20;

			//    	//----------------------------
			var ownershipExperiencerating = doc[0].averageownershipExperience;
			var ownershipExperiencepercentage = ownershipExperiencerating * 20;

			//    	// ----------------------------------
			var handlingrating = doc[0].averagehandling;
			var handlingpercentage = handlingrating * 20;


			// //----------------------------------------   	
			var count = doc[0].Count;
			var averageRating = (doc[0].averagePerformance + doc[0].averagestyle + doc[0].averageverstality + doc[0].averageownershipExperience + doc[0].averagehandling) / 5;
			var averageRatingPercentage = averageRating / 5 * 100;
			// //------------------------------------

			var obj = {};
			obj =
				{
					// req.body.EntityId
					reviewEntityId: reviewEntityId,
					averageRating: parseFloat(averageRating).toFixed(2),
					averageRatingPercentage: averageRatingPercentage,
					reviewersCount: count,
					reviewCriterias:
					{

						performance:
						{

							reviewCriteria: 'performane',
							reviewCriteriaLabel: 'Performance',
							reviewRating: performancerating,
							reviewRatingPercentage: performanceratingpercentage,

						},

						handling:
						{

							reviewCriteria: 'handling',
							reviewCriteriaLabel: 'Handling',
							reviewRating: handlingrating,
							reviewRatingPercentage: handlingpercentage,


						},
						style:
						{
							reviewCriteria: 'style',
							reviewCriteriaLabel: 'Style',
							reviewRating: stylerating,
							reviewRatingPercentage: stylepercentage,
						},



						versatality:
						{
							reviewCriteria: 'verstality',
							reviewCriteriaLabel: 'Versatality',
							reviewRating: verstalityrating,
							reviewRatingPercentage: verstalityratingpercentage,
						},


						ownershipExperience:
						{
							reviewCriteria: 'ownershipExperience',
							reviewCriteriaLabel: 'OwnershipExperience',
							reviewRating: ownershipExperiencerating,
							reviewRatingPercentage: ownershipExperiencepercentage,
						},
					}
				}
			//--------------------------------------  	
			//console.log('=====================AfterFirstObject=============================================')
			Reviews
				.find({$and :[{ reviewEntityId: reviewEntityId , 'locality.country' : country, 'locality.language' : language }]}, { averageRatingPercentage: 1, averageRating: 1, reviewCriterias: 1, _id: 0, reviewText: 1, reviewDescription: 1, reviewDateText: 1 })
				.populate({path : 'reviewByUser' , populate : {path : 'ownedBikeInfo.bikeId'}})
				.sort({ averageRatingPercentage: -1, reviewDateText: -1 }).limit(parseInt(req.body.obj.resultLimit) > 0 ? parseInt(req.body.obj.resultLimit) : doc[0].Count )
				.then((docs) => {
				//	console.log("==========userDocsLength================")
				//	console.log(docs.length);
				//	console.log("==========userDocsLength==================================result======================")
					if(docs.length != 0){
					var limit = 0;
				//	console.log("================userDocslengthnotzero===================")
					
					if(parseInt(req.body.obj.resultLimit) > 0)
					{
						if(parseInt(req.body.obj.resultLimit) > doc[0].Count || parseInt(req.body.obj.resultLimit) == doc[0].Count )
						{
							limit = doc[0].Count
							obj.msg= "disable Load more"
						}
						else
						{
							limit = parseInt(req.body.obj.resultLimit);
						}
					}
					else if(parseInt(req.body.obj.resultLimit) == 0)
					{
						limit = doc[0].Count;
					}
					//console.log("===========limt========")
					//console.log(limit)
					//console.log("=========limit=====");

					var array = [];
				
					var i = 0;
					for (i = 0; i < limit; i++) {
					
						array.push({
							reviewText: docs[i].reviewText,
							averageRatingPercentage: docs[i].averageRatingPercentage,
							reviewByUser: {ownerBikeDetails : docs[i].reviewByUser.ownedBikeInfo.length != 0 ? docs[i].reviewByUser.ownedBikeInfo[0].bikeId.bikeName.replace(/-/g,' ').charAt(0).toUpperCase() + docs[i].reviewByUser.ownedBikeInfo[0].bikeId.bikeName.replace(/-/g,' ').slice(1)  : '', firstName: docs[i].reviewByUser.fname, profilePageUrl: docs[i].reviewByUser.userUrl, profilePicture: { srcPath: docs[i].reviewByUser.profilePicture } },
							reviewCriterias: docs[i].reviewCriterias,
							averageRating: docs[i].averageRating,
							reviewDescription: docs[i].reviewDescription,
							reviewDateText: moment(docs[i].reviewDateText).format('YYYY-MM-DD'),
							reviewText: docs[i].reviewText,
						});
					}
					obj.reviewList = array;
					//console.log("======================")
					//console.log(obj)
					//console.log("==========================")
					res.send(obj);
				}else{
					console.log("herrreerrererrerer")
					res.send([]);
				}
				})

				.catch(err => res.send(err))
}
else
{res.status(500).send("error")
}
		});

	}
	else
	{
		res.status(500).send("error")
	}

}



//-------------------------------------------------------------------------------------------------````````
// summary details reviews

function getReviews(req, res, next) {
	var country = req.headers['x-custom-country'];
	var language = req.headers['x-custom-language'];
	var reviewEntityId = req.query.jsonString.split("_")[0];
	var limit = req.query.jsonString.split("_")[1];
//console.log("==========================reviews=====================")
//console.log(req.query)
//console.log("===================reviews=======================")
	Reviews
		.find({ reviewEntityId: reviewEntityId ,'locality.country':country,'locality.language':language}, { reviewText: 1, reviewDateText: 1, _id: 0, averageRatingPercentage: 1, }).populate('reviewByUser', 'fname lname profilePicture userUrl')
		.sort({ averageRatingPercentage: -1, reviewDateText : -1 }).limit(parseInt(limit))
		.then((doc) => {
			var obj = [];
			var sum = 0;
			var i = 0;
			for (i = 0; i < doc.length; i++) {
				obj.push({
					reviewText: doc[i].reviewText,
					averageRatingPercentage: doc[i].averageRatingPercentage,
					reviewByUser: { firstName: doc[i].reviewByUser ? doc[i].reviewByUser.fname : "", profilePicture: { srcPath: doc[i].reviewByUser ? doc[i].reviewByUser.profilePicture : "" }, profilePageUrl: doc[i].reviewByUser ? doc[i].reviewByUser.userUrl : "" },
				});
				sum = sum + doc[i].averageRatingPercentage;
			}
			var finalRating = sum / doc.length;
			res.send({ reviewersCount: doc.length, averageRatingPercentage: finalRating, reviewList: obj });
		})
		.catch((e) => { res.send(e) });
}





export default { create, fetch, getReviews }
