import Subscribe from '../models/subscribe';
import emailApi from '../service/emailApi';
import { EmailTemplate } from 'email-templates';
import path from 'path';


const emailDir = path.resolve(__dirname, '../templates/mailers', 'E-Mail-subscription');
const emailSubscribe = new EmailTemplate(path.join(emailDir));

function subscribed(req, res) {
	var subscribe = new Subscribe({
		email: req.body.email
	});

	subscribe.save().then((doc) => {
		var data = { name: doc.email };

		const locals = Object.assign({}, { data: data });
		emailSubscribe.render(locals, (err, results) => {
			
			console.log(err)
			var email = emailApi.sendEmailApi(
				"Royal Enfield subscription",
				"Thank you for subscribing for all the updates",
				results.html,
				"divanshu.goyal@techchefz.com",
				"error sending mail",
				"mail sent successfully"
			);
		});

		res.send(doc);
	}, (e) => {
		console.log(e);
	});
}

export default {
	subscribed
};
