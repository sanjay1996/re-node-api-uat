import CityStates from "../models/citiesStates";
import Dealers from "../models/dealers";
import countriesstatescities from "../models/countriesstatescities";
import request from "request";

var headers = {
  "Content-Type": "text/plain"
};

var fetchCityStatesData = {
  ResourceIdentifier: 206,
  EntityType: "9021",
  EntityID: 45,
  CardType: 34,
  RequestKey: "rgfvrgkjwkdjsddcawdwhcb"
};

var fetchCityStatesString = JSON.stringify(fetchCityStatesData);

var fetchCityStatesOptions = {
  url: "http://refineb2cdvp.excelloncloud.com/exapis//api/portalentity",
  method: "POST",
  headers: headers,
  body: fetchCityStatesString
};

function fetchCityStates(req, res, next) {
  request(fetchCityStatesOptions, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      // Print out the response body
      res.send(body);
      const cityStatesData = JSON.parse(body);
      CityStates.insertMany(cityStatesData, insertedDoc => {
        res.send(insertedDoc);
      });
    } else {
      console.log(error);
    }
  });
}

function getcity(req, res, next) {
  CityStates.find({ StateName: req.body.stateName })
    .then(foundCity => {
      let citiesArr = [];
      foundCity.map(a => {
        citiesArr.push({ cityName: a.CityName, cityId: a.CityId });
      });
      res.send(citiesArr);
    })
    .catch(err => {
      res.send(err);
    });
}

function getState(req, res, next) {
  CityStates.distinct("StateName").then(foundState => {
    res.send(foundState);
  });
}

function getDealers(req, res, next) {
  Dealers.find({ City: req.body.city, BranchTypeIdentifier: "Sales" })
    .then(foundDealer => {
      var dealersArr = [];
      foundDealer.map(dealer => {
        let dealerObj = {
          DealerName: dealer.DealerName,
          BranchName: dealer.BranchName,
          AddressLine1: dealer.AddressLine1,
          AddressLine2: dealer.AddressLine2,
          AddressLine3: dealer.AddressLine3,
          Locality: dealer.Locality,
          Landmark: dealer.Landmark,
          dealerID: dealer.DealerID,
          State: dealer.State,
          City: dealer.City,
          Pincode: dealer.Pincode,
          MainPhoneNo: dealer.MainPhoneNo,
          StoreEmailId: dealer.StoreEmailId,
          BranchCode: dealer.BranchCode,
          DealerID: dealer.DealerID
        };
        dealersArr.push(dealerObj);
      });
      res.send(dealersArr);
    })
    .catch(err => res.send(err));
}

function getAllCountryStateCity(req, res) {
  // var fetchCityStatesOptions = {
  //     url: 'http://188.166.68.8:4000/getAllCountriesStatesCities',
  //     method: 'GET'
  // }

  // request(fetchCityStatesOptions, function (error, response, body) {
  //     console.log('==========body==========================');
  //     console.log(response);
  //     console.log('==========body==========================');
  //     res.send(response);
  // });
  countriesstatescities.find({}, { _id: 0, oldCountryId: 0, __v: 0 }).then(foundCountries => {
    res.send(foundCountries);
  });
}

export default {
  getState,
  getcity,
  getDealers,
  fetchCityStates,
  getAllCountryStateCity
};
