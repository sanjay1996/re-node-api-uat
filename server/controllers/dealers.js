import Dealer from "../models/dealers";
import moment from "moment";

//=====================================================================================================================================================

var express = require("express");
var app = express();
var mongoose = require("mongoose");
var request = require("request");
var asyncc = require("asyncawait/async");
var awaitt = require("asyncawait/await");
var Schema = mongoose.Schema;

function createPage(req, res) {
  res.send("tripme");
  Dealer.find()
    .then(dealerFound => {
      // console.log(dealerFound);
      var fun = asyncc(function() {
        for (const item of dealerFound) {
          awaitt(savingFunctiontripstory(item));
        }
      });

      fun();
    })
    .catch(err => {
      console.log(err);
    });
}

var savingFunctiontripstory = function(savedStory) {
  return new Promise((resolve, reject) => {
    var username = "techchefz_admin";
    //var username = "admin";
    var password = "36A31NbkdMd]FixaxCA+*?NU";
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    request.post(
      {
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          Authorization: auth
        },
        rejectUnauthorized: false,
        url: `https://13.126.238.1/bin/createPage`,
        form: {
          entity: "dealers",
          pageId: savedStory._id.toString(),
          title: savedStory.DealerName,
          country: savedStory.locality.country,
          language: savedStory.locality.language,
          pageProperties: JSON.stringify({
            storeManagerName: savedStory.DealerPrincipalName,
            phoneNo: savedStory.MainPhoneNo,
            summary: savedStory.DealerDescription ? savedStory.DealerDescription : "",
            thumbnailImagePath: savedStory.Thumbnail_Image,
            googlePlaceId: savedStory.GooglePlaceId,
            email: savedStory.StoreEmailId,
            cover_image: savedStory.Cover_image,
            BranchTypeIdentifier: savedStory.BranchTypeIdenitifier,
            dateSort: moment().toISOString(),
            BranchCode: savedStory.BranchCode,
            pincode: savedStory.Pincode,
            DealerImage: savedStory.DealerImage,
            PrimaryImage_mobile: savedStory.PrimaryImage_mobile,
            PrimaryImage: savedStory.PrimaryImage,
            Cover_image_mobile: savedStory.Cover_image_mobile,
            AddressLine1: savedStory.AddressLine1,
            AddressLine2: savedStory.AddressLine2,
            AddressLine3: savedStory.AddressLine3,
            City: savedStory.City,
            State: savedStory.State,
            StoreManager: savedStory.StoreManagerName,
            landmark: savedStory.Landmark
          })
        }
      },
      function(error, response, data) {
        // console.log(data)
        console.log(error);
        var data = JSON.parse(data);

        savedStory.DealerUrl = data.pagePath;
        savedStory.save().then(story => {
          // console.log("This is the dealer url")
          // console.log(story.DealerUrl)
          resolve(story);
        });
      }
    );
  });
};

//=====================================================================================================================================================

function create(req, res, next) {
  Dealer.findOneAsync({
    $and: [{ dealerName: req.body.dealerName }, { placeId: req.body.placeId }]
  }).then(foundDealer => {
    if (foundDealer !== null) {
      const returnObj = {
        success: true,
        message: "",
        data: {}
      };
      returnObj.message = "dealer already exist";
      returnObj.success = false;
      return res.send(returnObj);
    } else {
      const dealer = new Dealer({
        dealer_id: req.body.dealer_id,
        DealerName: req.body.DealerName,
        BranchName: req.body.BranchName,
        address: req.body.address,
        contact_number: req.body.contact_number,
        isServiceApplicable: req.body.isServiceApplicable,
        isSalesApplicable: req.body.isSalesApplicable,
        StartTime: req.body.StartTime,
        EndTime: req.body.EndTime,
        Week_Off: req.body.Week_Off,
        image: req.body.image,
        email: req.body.email,
        review: req.body.review
      });
      dealer
        .saveAsync()
        .then(savedDealer => {
          const returnObj = {
            success: true,
            message: "",
            data: {}
          };
          returnObj.data.dealer = savedDealer;
          returnObj.message = "dealer created successfully";
          res.send(returnObj);
        })
        .error(e => next(e));
    }
  });
}
function update(req, res, next) {
  //code to update dealer details goes here ..
}

function remove(req, res, next) {
  // code to remove dealer goes here..
}

function getBusinessHours(str) {
  console.log("=================This is the dealers buisness hours========");
  console.log(str);
  console.log("============Thus us the dealers buinsess hours=============");
  var d = new Date();
  var n = d.getDay();
  var finalDate;
  var spl = str.split(",");
  for (var i = 0; i < spl.length; i++) {
    //console.log(parsed);
    console.log("::parsedVariable");
    console.log(n);
    console.log("This is the pasred date");
    if (i == n) {
      finalDate = spl[i].split(":");
      console.log(finalDate);
      var from = moment(finalDate[1] + ":" + finalDate[2], ["HH:mm"]).format("hh:mm A");
      var to = moment(finalDate[3] + ":" + finalDate[4], ["HH:mm"]).format("hh:mm A");
      // console.log(from + "-" + to);
      var time = from + "-" + to;
      return time;
    }
  }
}

function get(req, res, next) {
  var d = new Date();
  var day = d.getDay();
  var finalDate;
  var from;
  var to;
  var obj;
  Dealer.findOne({ _id: req.query.jsonString })
    .then(dealers => {
      var weeklyOfff;
      if (dealers.WeeklyOff == 0) {
        weeklyOfff = "Sunday";
      } else if (dealers.WeeklyOff == 1) {
        weeklyOfff = "Monday";
      } else if (dealers.WeeklyOff == 2) {
        weeklyOfff = "Tuesday";
      } else if (dealers.WeeklyOff == 3) {
        weeklyOfff = "Wednesday";
      } else if (dealers.WeeklyOff == 4) {
        weeklyOfff = "Thursday";
      } else if (dealers.WeeklyOff == 5) {
        weeklyOfff = "Friday";
      } else if (dealers.WeeklyOff == 6) {
        weeklyOfff = "Saturday";
      }
      var businessHours = getBusinessHours(dealers.BusinessHours);
      obj = {
        timingDetails: businessHours,
        day: weeklyOfff,
        Locality: dealers.Locality
      };
      res.send(obj);
    })
    .catch(err => {
      console.log(err);
    });
}

function rideOuts(req, res, next) {
  var dealerId = req.query.jsonString;
  // console.log("==================rideOuts================================")
  // console.log(dealerId);
  Dealer.findOne({ _id: dealerId })
    .populate("rideOutId")
    .then(dealer => {
      // console.log(dealer)
      // console.log("==========rideOuts===================================");
      var ride = [];
      if (dealer.rideOutId.length != 0) {
        for (const item of dealer.rideOutId) {
          var obj = {
            rideName: item.rideName,
            thumbnailImagePath: item.rideImages.length != 0 ? item.rideImages[0].srcPath : "",
            rideSummary: item.rideDetails ? item.rideDetails : "",
            startDate: item.startDate,
            endDate: item.endDate,
            distance: item.totalDistance,
            distanceUnit: "Kms",
            createdDate: item.createdOn ? item.createdOn : "",
            pageUrl: item.ridePageUrl
          };
          ride.push(obj);
        }
        res.send({ rides: ride });
      } else {
        res.send({ ride: ride });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).send("error");
    });
}

export default {
  get,
  create,
  update,
  remove,
  rideOuts,
  createPage
};
