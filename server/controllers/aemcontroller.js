
import request from 'request';
import logger from '../../config/winston';
import config from '../../config/env';
// they are hardcoded, make them as environment variable
//------------------------------------------------------------------------
var username = config.aemcredentials.user;
var password = config.aemcredentials.password;
var url = 'http://localhost:4502/bin/payment';
// ------------------------------------------------------------------------


function aemcontrol(data) {
  var object = { data: data }
  var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
  request.post({
    headers: {
      'content-type': 'application/x-www-form-urlencoded',
      "Authorization": auth
    },
    url: url,
    from: object
  }, function (error, response, body) {
    logger.log({
      level: 'error',
      message: JSON.stringify(error),
    });
  });
}

export default { aemcontrol }
