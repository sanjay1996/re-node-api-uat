import Ride from "../models/ride";
import { aemcontrol } from "./aemcontroller";
import request from "request";
import config from "../../config/env";
import sortJsonArray from "sort-json-array";
import geoLib from "geo-lib";
import mongoose from "mongoose";
import User from "../models/user";
import moment from "moment";
import user from "../models/user";
import Solr from "./solr";
import jsonexport from "jsonexport";
import fs from "fs";
import emailApi from "../service/emailApi";
import { EmailTemplate } from "email-templates";
import path from "path";
import solrClient from 'solr-client';

var indexer = solrClient.createClient({
  host: config.solr,
  port: 8983,
  path: "/solr",
  core: "new_core",
  solrVersion: 721
});


mongoose.Promise = global.Promise;
const emailDir = path.resolve(__dirname, "../templates/mailers", "create-ride");
const emailCreateRide = new EmailTemplate(path.join(emailDir));

function create(req, res, next) {
  var language = req.headers['x-custom-language'];
  var country = req.headers['x-custom-country'];
  var waypts = [];
  var imagePath, imageName;
  var arrayFilePath = [];
  var returnObj = {
    success: true,
    message: "",
    data: {}
  };

  req.files.forEach(file => {
    arrayFilePath.push({ srcPath: "/" + file.path });
  });
  var ride;

  if (req.body.waypoints) {
    waypts = JSON.parse(req.body.waypoints);
    ride = new Ride({
      rideStartDateIso: moment(req.body.startDate, "DD-MM-YYYY").toISOString(),
      rideEndDateIso: moment(req.body.endDate, "DD-MM-YYYY").toISOString(),
      rideName: req.body.rideName,
      locality: {
        country: country,
        language: language
      },
      difficulty: req.body.difficulty,
      startPoint: {
        name: req.body.startPoint[1],
        latitude: req.body.startLatitude,
        longitude: req.body.startLongitude
      },
      endPoint: {
        name: req.body.destination,
        latitude: req.body.destinationlatitude,
        longitude: req.body.destinationlongitude
      },
      durationInDays: req.body.durationInDays,
      rideDetails: req.body.rideDetails,
      startDate: req.body.startDate,
      endDate: req.body.endDate,
      terrain: req.body.terrain,
      createdByUser: req.body.userId,
      startTime: `${req.body.startTimeHours + ":" + req.body.startTimeMins + ":" + req.body.startTimeZone}`,
      totalDistance: req.body.totalDistance,
      rideImages: arrayFilePath.length != 0 ? arrayFilePath : [{ srcPath: "/node/assets/Rides/ridePlaceholder.jpg" }],
      personalInfo: {
        fName: req.body.fname,
        lName: req.body.lname,
        gender: req.body.gender,
        email: req.body.email,
        password: req.body.password,
        isRoyalEnfieldOwner: req.body.moto,
        dob: req.body.dob,
        city: req.body.city,
        bikeName: req.body.bikeOwned
      },
      waypoints: waypts,
      rideCategory: req.body.rideCategory,

      geo: {
        type: "Point",
        coordinates: [req.body.startLongitude, req.body.startLatitude]
      }
      //  geometry : {type : 'Point',coordinates :[req.body.startLongitude,req.body.startLatitude]}
    });
  } else {
    ride = new Ride({
      rideStartDateIso: moment(req.body.startDate, "DD-MM-YYYY").toISOString(),
      rideEndDateIso: moment(req.body.endDate, "DD-MM-YYYY").toISOString(),
      rideName: req.body.rideName,
      difficulty: req.body.difficulty,
      locality: {
        country: country,
        language: language
      },
      startPoint: {
        name: req.body.startPoint[1],
        latitude: req.body.startLatitude,
        longitude: req.body.startLongitude
      },
      endPoint: {
        name: req.body.destination,
        latitude: req.body.destinationlatitude,
        longitude: req.body.destinationlongitude
      },
      durationInDays: req.body.durationInDays,
      rideDetails: req.body.rideDetails,
      startDate: req.body.startDate,
      endDate: req.body.endDate,
      terrain: req.body.terrain,
      createdByUser: req.body.userId,
      startTime: `${req.body.startTimeHours + ":" + req.body.startTimeMins + ":" + req.body.startTimeZone}`,
      totalDistance: req.body.totalDistance,
      rideImages: arrayFilePath.length != 0 ? arrayFilePath : [{ srcPath: "/node/assets/Rides/ridePlaceholder.jpg" }],
      personalInfo: {
        fName: req.body.fname,
        lName: req.body.lname,
        gender: req.body.gender,
        email: req.body.email,
        password: req.body.password,
        isRoyalEnfieldOwner: req.body.moto,
        dob: req.body.dob,
        city: req.body.city,
        bikeName: req.body.bikeOwned
      },
      rideCategory: req.body.rideCategory,
      geo: {
        type: "Point",
        coordinates: [req.body.startLongitude, req.body.startLatitude]
      }

      //  geometry : {type : 'Point',coordinates :[req.body.startLongitude,req.body.startLatitude]}
    });
  }
  ride.saveAsync().then(savedRide => {
    User.findOne({ _id: savedRide.createdByUser }).then(userFound => {
      userFound.ridesCreated.push(savedRide._id);
      userFound.save();
    }).catch(e => console.log(e));
    var waypointsData = "";
    for (var i = 0; i < savedRide.waypoints.length; i++) {
      var waypointsname = savedRide.waypoints[i].name;
      var waypointslat = savedRide.waypoints[i].latitude;
      var waypointslng = savedRide.waypoints[i].longitude;
      waypointsData +=
        waypointsname + "," + waypointslat + "," + waypointslng + "|";
    }
    var username = config.aemcredentials.user;
    var password = config.aemcredentials.password;
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    request.post(
      {
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          Authorization: auth
        },
        rejectUnauthorized: false,
        url: `${config.aemPublish}bin/createPage`,
        form: {
          entity: savedRide.rideCategory,
          pageId: savedRide._id.toString(),
          title: savedRide.rideName,
          country: country,
          language: language,
          pageProperties: JSON.stringify({
            rideEndDateIso: savedRide.rideEndDateIso,
            rideStartDateIso: savedRide.rideStartDateIso,
            startPointPlaceName: savedRide.startPoint.name,
            startPointLongitude: req.body.startLongitude,
            startPointLatitude: req.body.startLatitude,
            endPointPlaceName: savedRide.endPoint.name,
            endPointLongitude: req.body.destinationlongitude,
            endPointLatitude: req.body.destinationlatitude,
            startDate: req.body.startDate,
            endDate: req.body.endDate,
            terrain: req.body.terrain,
            duration: req.body.durationInDays,
            thumbnailImagePath: savedRide.rideImages[0].srcPath,
            author: req.body.fname + " " + req.body.lname,
            totalDistance: req.body.totalDistance,
            rideDescription: req.body.rideDetails,
            wayPoints: waypointsData,
            dateSort: moment().toISOString()
          })
        }
      },
      function (error, response, data) {
        var resData = JSON.parse(data);
        if (error) {
          returnObj.data.ride = null;
          returnObj.message = "Error Creating ride";
          returnObj.success = false;
          res.send(returnObj);
        } else {
          //-----------------------------------------------------------------------------------------------------------------------
          var username = config.aemcredentials.user;
          var password = config.aemcredentials.password;
          var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
          request.post(
            {
              headers: {
                "content-type": "application/x-www-form-urlencoded",
                Authorization: auth
              },
              url: `${config.aemPublish2}bin/createPage`,
              rejectUnauthorized: false,
              form: {
                entity: savedRide.rideCategory,
                pageId: savedRide._id.toString(),
                title: savedRide.rideName,
                country: country,
                language: language,
                pageProperties: JSON.stringify({
                  rideEndDateIso: savedRide.rideEndDateIso,
                  rideStartDateIso: savedRide.rideStartDateIso,
                  startPointPlaceName: savedRide.startPoint.name,
                  startPointLongitude: req.body.startLongitude,
                  startPointLatitude: req.body.startLatitude,
                  endPointPlaceName: savedRide.endPoint.name,
                  endPointLongitude: req.body.destinationlongitude,
                  endPointLatitude: req.body.destinationlatitude,
                  startDate: req.body.startDate,
                  endDate: req.body.endDate,
                  terrain: req.body.terrain,
                  duration: req.body.durationInDays,
                  thumbnailImagePath: savedRide.rideImages[0].srcPath,
                  author: req.body.fname + " " + req.body.lname,
                  totalDistance: req.body.totalDistance,
                  rideDescription: req.body.rideDetails,
                  wayPoints: waypointsData,
                  dateSort: moment().toISOString()
                })
              }
            },
            function (error, response, data) {
              console.log(data)
              console.log(error);
              console.log("next response came");
              Ride.findOneAndUpdate({ _id: savedRide._id }, { $set: { ridePageUrl: resData.pagePath } })
                .then(updatedride => {
                  console.log("inside updated");

                  User.findOne({ _id: savedRide.createdByUser }).then(found => {
                    var emailData = {
                      userName: found.fname + " " + found.lname,
                      rideName: savedRide.rideName,
                      rideDescription: savedRide.rideDescription,
                      rideOrigin: savedRide.startPoint.name,
                      rideDestination: savedRide.endPoint.name,
                      linkRide: config.websiteUrl + resData.pagePath.split("/content/royal-enfield")[1],
                      url: config.websiteUrl
                    };
                    const locals = Object.assign({}, { data: emailData });
                    emailCreateRide.render(locals, (err, results) => {
                      console.log(results);
                      console.log(err);
                      var email = emailApi.sendEmailApi(
                        "Ride-" + emailData.rideName,
                        "Thank you for booking test ride",
                        results.html,
                        found.email,
                        "error sending mail",
                        "mail sent successfully"
                      );
                    });
                  });
                  returnObj.data.ride = savedRide;
                  returnObj.data.ride.ridePageUrl = resData.pagePath.toString();
                  returnObj.message = "ride created successfully";
                  savingFunction(savedRide, req, country, language, waypointsData, resData.pagePath)
                  res.send(returnObj);
                });

            })
        }
      }
    );
  }).catch(e => console.log(e));
}

function update(req, res, next) {

}

function remove(req, res, next) {
  // code to remove ride goes here..
  Ride.find({ _id: req.body.rideId })
    .remove()
    .then(removedDoc => {
      res.send("ride removed successfully");
    })
    .catch(e => console.log(e));
}

function joinRide(req, res, next) {
  const emailDirJoin = path.resolve(__dirname, "../templates/mailers", "join-ride-ride-creator");
  const emailJoinRide = new EmailTemplate(path.join(emailDirJoin));

  var userId = req.body.userId;
  var rideId = req.body.rideId;
  var returnObj = {};
  var category = req.body.category;
  var joinerObj;

  Ride.findOne({ _id: rideId }).populate('DealerId createdByUser')
    .then(foundRide => {
      if (foundRide.createdByUser && foundRide.createdByUser._id == userId) {
        returnObj.message = "owner";
        returnObj.success = false;
        res.send(returnObj);
      }
      else if (foundRide.ridersJoined.indexOf(userId) != -1) {
        returnObj.message = "user already exist";
        returnObj.success = false;
        res.send(returnObj);
      } else {
         foundRide.ridersJoined.push(userId);
        foundRide.save().then(savedRide => {
        savedRide.ridersCount = savedRide.ridersJoined.length;
          savedRide.save().then(() => {
            User.findOne({ _id: userId }).then(foundUser => {
              foundUser.ridesJoined.push(rideId);
              if (foundRide.rideCategory == "ride-out") {
                const newUser = [{
                  ContactName: `${foundUser.fname + " " + foundUser.lname}`,
                  CustomerContactNo: `${foundUser.phoneNo}`,
                  EmailID: `${foundUser.email}`,
                  RideID: `${foundRide.RideId}`,
                  Remarks: `NA`,
                  RideCategory: `${foundRide.rideCategory}`,
                  RideSubCategory: `NA`,
                  Gender: `${foundUser.gender}`,
                  Size: `NA`
                }];
               jsonexport(newUser, { rowDelimiter: "|" }, function (err, csv) {
                  if (err) {
                    console.log(err);
                  } else {
                    fs.readdir("archivedFiles/RidesJoined", (err, files) => {
                      console.log('=============files.length=======================');
                      console.log(files.length);
                      console.log('================files.length====================');
                      if (files.length == 0) {
                        fs.writeFile(`archivedFiles/RidesJoined/VW_WEBSITE_RIDEENROLLMENTDETAIL_${moment().format("DDMMYYYY_HHMMSS")}.csv`, csv, function (err) {
                          if (err) throw err;
                        });
                      } else {
                        if (csv.includes("ContactName|CustomerContactNo|EmailID|RideID|Remarks|RideCategory|RideSubCategory|Gender|Size")) {
                          var dumpData = csv.replace("ContactName|CustomerContactNo|EmailID|RideID|Remarks|RideCategory|RideSubCategory|Gender|Size", "");
                          var finalData = dumpData.trim();
                          console.log('=========inside else loop and this is finalData===========================');
                          console.log(finalData);
                          console.log('===============inside else loop and this is finalData=====================');
                          fs.appendFile(`archivedFiles/RidesJoined/${files[0]}`, "\r\n" + finalData.trim(), function (err) {
                            if (err) throw err;
                            console.log('===========================================appended to file and final data============================');
                            console.log(finalData);
                            console.log('===========##################################=====appended to file and final data====================');
                          });
                        }
                      }
                    });
                  }
                });
              }

              foundUser.save().then(savedUser => {
                if (foundRide.createdByUser) {
                  User.findOne({ _id: foundRide.createdByUser }).then((creator) => {
                    var emailObj = {
                      creatorName: creator.fname + " " + creator.lname,
                      rideName: foundRide.rideName,
                      joinerName: savedUser.fname + "" + savedUser.lname,
                      joinerEmail: savedUser.email,
                      joinerMobile: savedUser.phoneNo,
                      url: config.websiteUrl
                    }
                    const locals = Object.assign({}, { data: emailObj });
                    emailJoinRide.render(locals, (err, results) => {
                      var email = emailApi.sendEmailApi(
                        emailObj.rideName + "-Join confirmation",
                        "Thank you for joining ride",
                        results.html,
                        creator.email,
                        "error sending mail",
                        "mail sent successfully"
                      );
                    });
                  })
                }
              }).catch(err => {
                console.log("====================================");
                console.log(err);
                console.log("====================================");
              });
            });
            //	Solr.updateJoinRide(savedRide)
            returnObj.message = "successfully joined ride";
            returnObj.success = true;
            res.send(returnObj);
          });
        })
          .catch(err => {
            console.log("====================================");
            console.log(err);
            console.log("====================================");
          });
      }
    }).catch(err => {
      console.log(err);
      returnObj.message = "error joining ride";
      returnObj.success = false;
      res.send(returnObj);
    });
}

function ridersJoined(req, res, next) {
  var returnObj = {};
  var rideId = req.query.rideId;
  console.log(rideId);
  Ride.findOne({ _id: rideId }).then(foundRide => {
    console.log(foundRide);
    returnObj.message = null;
    returnObj.success = true;
    returnObj.data = foundRide.ridersJoined.length;
    res.send(returnObj);
  });
}
/**
 * Get Rides list.
 * @property {number} req.body.skip - Number of rides to be skipped.
 * @property {number} req.body.limit - Limit number of rides to be returned.
 * @returns {User[]}
 */

function getRide(req, res, next) {
  Ride.findOne({ _id: req.query.jsonString }).populate("createdByUser").populate("DealerId").then(ride => {
    if (ride.createdByUser) {
      var waypointsData = "";
      if (ride.waypoints) {
        waypointsData = "";
        for (var i = 0; i < ride.waypoints.length; i++) {
          var waypointsname = ride.waypoints[i].name;
          var waypointslat = ride.waypoints[i].latitude;
          var waypointslng = ride.waypoints[i].longitude;
          waypointsData += waypointsname + "," + waypointslat + "," + waypointslng + "|";
        }
      }

      var obj = {
        rideId: ride._id,
        difficulty: ride.difficulty ? ride.difficulty : "",
        rideName: ride.rideName,
        rideDescription: ride.rideDetails,
        startPoint: {
          name: ride.startPoint.name,
          longitude: ride.startPoint.longitude,
          latitude: ride.startPoint.latitude
        },
        endPoint: {
          name: ride.endPoint.name,
          longitude: ride.endPoint.longitude,
          latitude: ride.endPoint.latitude
        },
        durationInDays: ride.durationInDays,
        distance: ride.totalDistance,
        terrain: ride.terrain,
        startDate: ride.startDate,
        endDate: ride.endDate,
        createdBy: {
          fullName: ride.personalInfo.fName + " " + ride.personalInfo.lName,
          phone: ride.createdByUser.phoneNo,
          email: ride.personalInfo.email
        },
        waypoints: waypointsData,
        thumbnailImagePath: ride.rideImages.length != 0 ? ride.rideImages[0].srcPath : "",
        ridersInterestedCount: ride.ridersCount ? parseInt(ride.ridersCount) + 1 : 1
      };
      res.send(obj);
    } else if (ride.DealerId) {
      var waypointsData = "";
      if (ride.waypoints) {
        waypointsData = "";
        for (var i = 0; i < ride.waypoints.length; i++) {
          var waypointsname = ride.waypoints[i].name;
          var waypointslat = ride.waypoints[i].latitude;
          var waypointslng = ride.waypoints[i].longitude;
          waypointsData += waypointsname + "," + waypointslat + "," + waypointslng + "|";
        }
      }
      var obj = {
        rideId: ride._id,
        difficulty: ride.difficulty ? ride.difficulty : "Easy",
        rideName: ride.rideName,
        startPoint: {
          name: ride.startPoint.name,
          longitude: ride.startPoint.longitude,
          latitude: ride.startPoint.latitude
        },
        endPoint: {
          name: ride.endPoint.name,
          longitude: ride.endPoint.longitude,
          latitude: ride.endPoint.latitude
        },
        durationInDays: ride.durationInDays ? ride.durationInDays : "",
        distance: ride.totalDistance,
        terrain: ride.terrain,
        rideDescription: ride.rideDetails,
        startDate: ride.startDate,
        endDate: ride.endDate,
        ridersInterestedCount: ride.ridersCount,
        createdBy: {
          fullName: ride.DealerId.DealerName,
          phone: ride.DealerId.MainPhoneNo,
          email: ride.DealerId.StoreEmailId
        },
        waypoints: waypointsData,
        thumbnailImagePath: ride.rideImages.length != 0 ? ride.rideImages[0].srcPath : "",
        ridersInterestedCount: ride.ridersJoined.length + 1
      };
      res.send(obj);
    } else {
      var waypointsData = "";
      if (ride.waypoints) {
        waypointsData = "";
        for (var i = 0; i < ride.waypoints.length; i++) {
          var waypointsname = ride.waypoints[i].name;
          var waypointslat = ride.waypoints[i].latitude;
          var waypointslng = ride.waypoints[i].longitude;
          waypointsData +=
            waypointsname + "," + waypointslat + "," + waypointslng + "|";
        }
      }

      var obj = {
        rideId: ride._id,
        rideName: ride.rideName,
        rideDescription: ride.rideDetails,
        startPoint: {
          name: ride.startPoint.name,
          longitude: ride.startPoint.longitude,
          latitude: ride.startPoint.latitude
        },
        endPoint: {
          name: ride.endPoint.name,
          longitude: ride.endPoint.longitude,
          latitude: ride.endPoint.latitude
        },
        ridersInterestedCount: ride.ridersCount,
        durationInDays: ride.durationInDays,
        distance: ride.totalDistance,
        terrain: ride.terrain,
        startDate: ride.startDate,
        endDate: ride.endDate,
        createdBy: {
          fullName: ride.CompanyName,
          phone: "",
          email: "shaktimotors@gmail.com"
        },
        waypoints: waypointsData,
        thumbnailImagePath:
          ride.rideImages.length != 0 ? ride.rideImages[0].srcPath : "",
        ridersInterestedCount: ride.ridersJoined.length + 1
      };
      res.send(obj);
    }
  }).catch(err => {
    console.log(err);
  });
}

function getRides(req, res, next) {
  Ride.find({ _id: req.body.rideId })
    .then(rides => {
      res.send(rides);
    });
}

function filterRides(req, res, next) {
  var userId = req.body.obj.userId;
  User.findOne({ _id: userId })
    .populate("ridesJoined")
    .then(foundUser => {
      var d = new Date();
      var n = d.toISOString();

      var upcomingRidesArr = [];
      var pastRides = [];
      if (foundUser.ridesJoined.length != 0) {
        for (var i = 0; i < foundUser.ridesJoined.length; i++) {
          var verDate = moment(foundUser.ridesJoined[i].startDate, "DD-MM-YYYY").format("YYYY-MM-DD");
          if (moment(verDate).isAfter(moment().format("YYYY-MM-DD"))) {
            upcomingRidesArr.push({
              rideName: foundUser.ridesJoined[i].rideName,
              rideTypeText: foundUser.ridesJoined[i].rideCategory,
              rideDetails: foundUser.ridesJoined[i].rideDetails,
              pageUrl: foundUser.ridesJoined[i].ridePageUrl,
              startPoint: { name: foundUser.ridesJoined[i].startPoint.name },
              endPoint: { name: foundUser.ridesJoined[i].endPoint.name },
              startDate: foundUser.ridesJoined[i].startDate,
              durationInDays: foundUser.ridesJoined[i].durationInDays
            });
          } else {
            pastRides.push({
              rideName: foundUser.ridesJoined[i].rideName,
              rideTypeText: foundUser.ridesJoined[i].rideCategory,
              rideDetails: foundUser.ridesJoined[i].rideDetails,
              pageUrl: foundUser.ridesJoined[i].ridePageUrl,
              startPoint: { name: foundUser.ridesJoined[i].startPoint.name },
              endPoint: { name: foundUser.ridesJoined[i].endPoint.name },
              startDate: foundUser.ridesJoined[i].startDate,
              durationInDays: foundUser.ridesJoined[i].durationInDays
            });
          }
        }
        console.log({ upcomingRides: upcomingRidesArr, pastRides: pastRides });
        res.send({ upcomingRides: upcomingRidesArr, pastRides: pastRides });
      } else {
        res.send({ upcomingRides: [], pastRides: [] });
      }
    })
    .catch(err => {
      console.log(err);
    });
}

function getRidesAroundMeBackup(req, res, next) {
  //   Ride.aggregate([
  //    {
  //      $geoNear: {
  //         near: { type: "Point", coordinates: [ req.body.longitude , req.body.latitude ] },
  //         distanceField: "dist.calculated",
  //         query: { type: "public" },
  //         includeLocs: "dist.location",
  //         num: 5,
  //         spherical: true
  //      }
  //    }
  // ])
  Ride.geoNear([req.body.latitude, req.body.longitude], { distanceField: "dist.calculated", spherical: true })
    .then(doc => {
      const result = {
        object: doc
      };
      res.send(result);
    });
}

function getRidesAroundMe(req, res, next) {
  var language = req.headers["x-custom-language"];
  var country = req.headers["x-custom-country"];
  var Userlat = parseFloat(req.body.latitude);
  var Userlong = parseFloat(req.body.longitude);
  var category = req.body.category;
  var d = new Date();
  var n = d.toISOString();
  var filter = req.body.filter;
  if (Userlat) {
    switch (filter) {
      case "upcoming":
        Ride.find({
          rideStartDateIso: { $gte: n },
          rideCategory: category,
          "locality.language": language,
          "locality.country": country,
          geo: {
            $near: {
              $geometry: {
                type: "Point",
                coordinates: [req.body.longitude, req.body.latitude]
              }
            }
          }
        })
          .populate("DealerId")
          .limit(parseInt(req.body.limit))
          .then(upcomingRides => {
            res.send({ object: upcomingRides });
          });
        break;
      case "completed":
        Ride.find({
          rideStartDateIso: { $lte: n },
          rideCategory: category,
          "locality.language": language,
          "locality.country": country,
          geo: {
            $near: {
              $geometry: {
                type: "Point",
                coordinates: [req.body.longitude, req.body.latitude]
              }
            }
          }
        })
          .populate("DealerId")
          .limit(parseInt(req.body.limit))
          .then(completedRides => {
            res.send({ object: completedRides });
          });
        break;
      case "popular":
        Ride.find({
          rideCategory: category,
          "locality.language": language,
          "locality.country": country,
          geo: {
            $near: {
              $geometry: {
                type: "Point",
                coordinates: [req.body.longitude, req.body.latitude]
              }
            }
          }
        })
          .populate("DealerId")
          .sort({ ridersCount: -1 })
          .limit(parseInt(req.body.limit))
          .then(popularRides => {
            res.send({ object: popularRides });
          });
        break;
      default:
        Ride.find({
          rideStartDateIso: { $gte: n },
          rideCategory: "user-ride",
          "locality.language": language,
          "locality.country": country,
          geo: {
            $near: {
              $geometry: {
                type: "Point",
                coordinates: [req.body.longitude, req.body.latitude]
              }
            }
          }
        })
          .limit(parseInt(req.body.limit))
          .then(upcomingRides => {
            res.send({ object: upcomingRides });
          });
    }
  } else {
    switch (filter) {
      case "upcoming":
        Ride.find({
          rideStartDateIso: { $gte: n },
          rideCategory: category,
          "locality.language": language,
          "locality.country": country
        })
          .populate("DealerId")
          .limit(parseInt(req.body.limit))
          .then(upcomingRides => {
            res.send({ object: upcomingRides });
          });
        break;
      case "completed":
        Ride.find({
          rideStartDateIso: { $lte: n },
          rideCategory: category,
          "locality.language": language,
          "locality.country": country
        })
          .populate("DealerId")
          .limit(parseInt(req.body.limit))
          .then(completedRides => {
            res.send({ object: completedRides });
          });
        break;
      case "popular":
        Ride.find({
          rideCategory: category,
          "locality.language": language,
          "locality.country": country
        })
          .sort({ ridersCount: -1 })
          .populate("DealerId")
          .limit(parseInt(req.body.limit))
          .then(popularRides => {
            res.send({ object: popularRides });
          });
        break;
      default:
        Ride.find({
          rideStartDateIso: { $gte: n },
          rideCategory: "user-ride",
          "locality.language": language,
          "locality.country": country
        })
          .limit(parseInt(req.body.limit))
          .then(upcomingRides => {
            res.send({ object: upcomingRides });
          });
    }
  }
}







var savingFunction = function (savedRide, req, country, language, waypointsData, pagePath) {
  return new Promise((resolve, reject) => {
    console.log("========================================================inside page Creation================")
    var obj = {
      entity: savedRide.rideCategory,
      pageId: savedRide._id.toString(),
      title: savedRide.rideName,
      country: country,
      language: language,
      rideEndDateIso: savedRide.rideEndDateIso,
      rideStartDateIso: savedRide.rideStartDateIso,
      startPointPlaceName: savedRide.startPoint.name,
      startPointLongitude: req.body.startLongitude,
      startPointLatitude: req.body.startLatitude,
      endPointPlaceName: savedRide.endPoint.name,
      endPointLongitude: req.body.destinationlongitude,
      endPointLatitude: req.body.destinationlatitude,
      startDate: req.body.startDate,
      endDate: req.body.endDate,

      duration: req.body.durationInDays,
      thumbnailImagePath: savedRide.rideImages[0].srcPath,
      author: req.body.fname + " " + req.body.lname,
      totalDistance: req.body.totalDistance,
      rideDescription: req.body.rideDetails,
      wayPoints: waypointsData,
      dateSort: moment().toISOString(),
      url: pagePath,
      id: pagePath,
      parentEntity: 'rides'
    }
    indexer.add(obj, function (err, obj) {
      if (err) {
        resolve();
        console.log(err);
      } else {
        var options = {
          waitSearcher: false
        };
        indexer.commit(options, function (err, obj) {
          if (err) {
            console.log(err);
            resolve();
          } else {
            console.log(obj);
            resolve();
          }
        })
        console.log(obj);
      }
    })
  })
}


export default {
  getRides,
  filterRides,
  getRide,
  create,
  update,
  remove,
  getRidesAroundMe,
  joinRide,
  ridersJoined
};


