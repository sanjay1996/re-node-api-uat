import fs from "fs";
import isNumber from "is-number";
import exceltoJson from "xlsx-to-json-lc";
import config from "../../config/env";
import request from "request";
import asyncc from "asyncawait/async";
import awaitt from "asyncawait/await";
import moment from "moment";
import csvToJson from "../../convert-csv-to-json";
import BikeCsvSchema from "../models/bikeCsv";
import Dealer from "../models/dealers";
import citiesStatesModel from "../models/citiesStates";
import { EmailTemplate } from "email-templates";
import emailApi from "../service/emailApi";
import ssh2, { Client } from "ssh2";

import path from "path";
const emailDir = path.resolve(__dirname, "../templates/mailers", "test");
const emailJoinRide = new EmailTemplate(path.join(emailDir));

const connection = new ssh2();
var resObj = {};

function connectSftp(req, res) {
  var fileName;
  connection.on("ready", function() {
    console.log("Client :: ready");
    connection.sftp(function(err, sftp) {
      if (err) throw err;
      sftp.readdir("DMS-Web3/Testing/Model", function(err, list) {
        if (err) throw err;
        console.log(list);
        fileName = list[1].filename;
        console.log(fileName);
        var sourceFile = sftp.createReadStream("DMS-Web3/Testing/Model/" + fileName);
        let destinationFile = fs.createWriteStream("csvFolder/bike.csv");
        sourceFile.pipe(destinationFile);

        sourceFile.on("end", () => {
          console.log("source ended");
        });

        destinationFile.on("finish", () => {
          console.log("destination finished");
          bikeCsvToJson();
        });
        // connection.end();
      });
    });
  });

  connection.on("error", err => {
    console.log(err);
  });

  connection.connect({
    debug: function(s) {
      console.log(s);
    },
    host: "https://ftpL.royalenfield.com",
    port: 22,
    username: "interface",
    password: "Interface@re",
    privateKey: fs.readFileSync("config/id_rsa.ppk"),
    passphrase: "Interface@re",
    readyTimeout: 99999
  });
}

function bikeCsvToJson(req, res) {
  var json = csvToJson
    .formatValueByType()
    .fieldDelimiter("|")
    .getJsonFromCsv("csvFolder/bike.csv");
  var arr = [];
  for (var i = 0; i < json.length; i++) {
    arr.push({
      familyName: isNumber(json[i].ModelGroupName) ? json[i].ModelGroupName.toString() : json[i].ModelGroupName,
      familyCode: isNumber(json[i].ModelGroupCode) ? json[i].ModelGroupCode.toString() : json[i].ModelGroupCode,
      bikeModelCode: isNumber(json[i].motorcycle_code) ? json[i].motorcycle_code.toString() : json[i].motorcycle_code,
      bikeName: isNumber(json[i].motorcycle_name) ? json[i].motorcycle_name.toString() : json[i].motorcycle_name,
      bikeStatus: isNumber(json[i].Status) ? json[i].Status.toString() : json[i].Status
    });
  }
  BikeCsvSchema.insertMany(arr, (err, docs) => {
    if (docs) {
      resObj.success = true;
      resObj.message = "inserted successfully";
      resObj.data = {};
    } else {
      resObj.success = false;
      resObj.message = "error inserting docs";
      resObj.data = err;
    }
  });
}

function dealerDataToDB(req, res, next) {
  res.send("Inserting Data Into Mongoo");
  var arr = [];
  exceltoJson(
    {
      input: "csvFolder/REDatabaseFinal_2_Jan_19.xlsx",
      output: null,
      lowerCaseHeaders: false
    },
    function(err, result) {
      if (err) {
        console.error(err);
      } else {
        console.log("====================================");
        console.log(result.length);
        console.log("====================================");
        Dealer.insertMany(result).then(insertedData => {
          console.log("Dealer inserted in database");

          Dealer.find().then(foundDealers => {
            console.log("Dealers found" + foundDealers.length);

            foundDealers.forEach(foundDealer => {
              if (foundDealer.BranchTypeIdentifier == "Sales") {
                console.log("Inside sales if");

                Dealer.findOneAndUpdate(
                  { _id: foundDealer._id },
                  {
                    $set: {
                      ifSales: true,
                      locality: {
                        country: "in",
                        language: "en"
                      },
                      DealerPrincipalName:
                        foundDealer.DealerPrincipalName == "NULL" ? "" : foundDealer.DealerPrincipalName,
                      Cover_image: foundDealer.Cover_image
                        ? `/node/assets/Dealer/CoverImage/${foundDealer.Cover_image}`
                        : "/node/assets/Dealer/CoverImage/Cover_image_placeholder.jpg",
                      Cover_image_mobile: foundDealer.Cover_image_mobile
                        ? `/node/assets/Dealer/CoverImage/mobile/${foundDealer.Cover_image_mobile}`
                        : "/node/assets/Dealer/CoverImage/mobile/Cover_image_mobile_placeholder.jpg",
                      Thumbnail_Image: foundDealer.Thumbnail_Image
                        ? `/node/assets/Dealer/ThumbnailImage/${foundDealer.Thumbnail_Image}`
                        : "/node/assets/Dealer/ThumbnailImage/Thumbnail_Image_placeholder.jpg",
                      PrimaryImage: foundDealer.PrimaryImage
                        ? `/node/assets/Dealer/PrimaryImage/${foundDealer.PrimaryImage}`
                        : "/node/assets/Dealer/PrimaryImage/PrimaryImage_placeholder.jpg",
                      PrimaryImage_mobile: foundDealer.PrimaryImage_mobile
                        ? `/node/assets/Dealer/PrimaryImage/mobile/${foundDealer.PrimaryImage_mobile}`
                        : "/node/assets/Dealer/PrimaryImage/mobile/PrimaryImage_mobile_placeholder.jpg",
                      DealerImage: foundDealer.DealerImage
                        ? `/node/assets/Dealer/DealerImage/${foundDealer.DealerImage}`
                        : "/node/assets/Dealer/DealerImage/DealerImage_placeholder.jpg"
                    }
                  }
                ).then(updatedDealer => {
                  console.log("inside saved file of sales if");
                  Dealer.find({
                    $and: [
                      { DealerID: updatedDealer.DealerID, BranchTypeIdentifier: "Service", City: updatedDealer.City }
                    ]
                  }).then(foundObj => {
                    foundObj.forEach(element => {
                      if ((element.BranchTypeIdentifier = "Service")) {
                        Dealer.findOneAndUpdate({ _id: foundDealer._id }, { $set: { ifService: true } }).then(() => {
                          console.log("saved ho gya inside if loop +service");
                        });
                      }
                    });
                  });
                });
              } else if (foundDealer.BranchTypeIdentifier == "Service") {
                console.log("inside found else if services");

                Dealer.findOneAndUpdate(
                  { _id: foundDealer._id },
                  {
                    $set: {
                      ifService: true,
                      locality: {
                        country: "in",
                        language: "en"
                      },
                      DealerPrincipalName:
                        foundDealer.DealerPrincipalName == "NULL" ? "" : foundDealer.DealerPrincipalName,
                      Cover_image: foundDealer.Cover_image
                        ? `/node/assets/Dealer/CoverImage/${foundDealer.Cover_image}`
                        : "/node/assets/Dealer/CoverImage/Cover_image_placeholder.jpg",
                      Cover_image_mobile: foundDealer.Cover_image_mobile
                        ? `/node/assets/Dealer/CoverImage/mobile/${foundDealer.Cover_image_mobile}`
                        : "/node/assets/Dealer/CoverImage/mobile/Cover_image_mobile_placeholder.jpg",
                      Thumbnail_Image: foundDealer.Thumbnail_Image
                        ? `/node/assets/Dealer/ThumbnailImage/${foundDealer.Thumbnail_Image}`
                        : "/node/assets/Dealer/ThumbnailImage/Thumbnail_Image_placeholder.jpg",
                      PrimaryImage: foundDealer.PrimaryImage
                        ? `/node/assetss/Dealer/PrimaryImage/${foundDealer.PrimaryImage}`
                        : "/node/assets/Dealer/PrimaryImage/PrimaryImage_placeholder.jpg",
                      PrimaryImage_mobile: foundDealer.PrimaryImage_mobile
                        ? `/node/assets/Dealer/PrimaryImage/mobile/${foundDealer.PrimaryImage_mobile}`
                        : "/node/assets/Dealer/PrimaryImage/mobile/PrimaryImage_mobile_placeholder.jpg",
                      DealerImage: foundDealer.DealerImage
                        ? `/node/assetss/Dealer/DealerImage/${foundDealer.DealerImage}`
                        : "/node/assets/Dealer/DealerImage/DealerImage_placeholder.jpg"
                    }
                  }
                ).then(updatedDealer => {
                  console.log(".then of update");

                  Dealer.find({
                    $and: [
                      { DealerID: updatedDealer.DealerID, BranchTypeIdentifier: "Sales", City: updatedDealer.City }
                    ]
                  }).then(foundObj => {
                    foundObj.forEach(element => {
                      if ((element.BranchTypeIdentifier = "Sales")) {
                        Dealer.findOneAndUpdate({ _id: foundDealer._id }, { $set: { ifSales: true } }).then(() => {
                          console.log("saved ho gya Inside Else if loop +sales");
                        });
                      }
                    });
                  });
                });
              }
            });
          });
        });
      }
    }
  );
}

function dealerPageCreation(req, res) {
  res.send("Page Creation Started");
  Dealer.find()
    .then(dealerFound => {
      console.log(dealerFound.length);
      var fun = asyncc(function() {
        for (const item of dealerFound) {
          awaitt(savingFunctiontripstory(item));
        }
      });
      fun();
    })
    .catch(err => {
      res.send("inside error");
      console.log(err);
    });
}

var savingFunctiontripstory = function(savedStory) {
  return new Promise((resolve, reject) => {
    var username = "admin";
    var password = "admin";
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    request.post(
      {
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          Authorization: auth
        },
        url: `${config.aemAuthorUrl}/bin/createPage`,
        form: {
          entity: "dealers",
          pageId: savedStory._id.toString(),
          title: savedStory.DealerName,
          country: savedStory.locality.country,
          language: savedStory.locality.language,
          pageProperties: JSON.stringify({
            storeManagerName: savedStory.DealerPrincipalName,
            phoneNo: savedStory.MainPhoneNo,
            summary: savedStory.DealerDescription ? savedStory.DealerDescription : "",
            thumbnailImagePath: savedStory.Thumbnail_Image,
            googlePlaceId: savedStory.GooglePlaceId,
            email: savedStory.StoreEmailId,
            cover_image: savedStory.Cover_image,
            BranchTypeIdentifier: savedStory.BranchTypeIdentifier,
            dateSort: moment().toISOString(),
            BranchCode: savedStory.BranchCode,
            pincode: savedStory.Pincode,
            DealerImage: savedStory.DealerImage,
            PrimaryImage_mobile: savedStory.PrimaryImage_mobile,
            PrimaryImage: savedStory.PrimaryImage,
            Cover_image_mobile: savedStory.Cover_image_mobile,
            AddressLine1: savedStory.AddressLine1,
            AddressLine2: savedStory.AddressLine2,
            AddressLine3: savedStory.AddressLine3,
            City: savedStory.City,
            State: savedStory.State,
            StoreManager: savedStory.StoreManagerName
          })
        }
      },
      function(error, response, data) {
        // console.log(data)
        var data = JSON.parse(data);
        savedStory.DealerUrl = data.pagePath;
        savedStory.save().then(story => {
          console.log("This is the dealer url" + story.DealerUrl);
          // console.log()
          resolve(story);
        });
      }
    );
  });
};

function rideoutsCsvToJson(req, res) {
  var fileName;
  connection.on("ready", function() {
    console.log("Client :: ready");
    connection.sftp(function(err, sftp) {
      if (err) throw err;
      sftp.readdir("DMS-Web3/Testing/RideList", function(err, list) {
        if (err) throw err;
        fileName = list[0].filename;
        console.log(fileName);
        // connection.end();
      });
    });
  });

  connection.on("error", err => {
    console.log(err);
  });

  connection.connect({
    debug: function(s) {
      console.log(s);
    },
    host: "https://ftpL.royalenfield.com",
    port: 22,
    username: "interface",
    password: "Interface@re",
    privateKey: fs.readFileSync("config/id_rsa.ppk"),
    passphrase: "Interface@re",
    readyTimeout: 99999
  });
}

function cityStatesData(req, res) {
  res.send("json");
  var json = csvToJson
    .formatValueByType()
    .fieldDelimiter("|")
    .getJsonFromCsv("csvFolder/VW_WEBSITE_STATECITY_29092018_071501.csv");
  citiesStatesModel.insertMany(json).then(abc => {});
}

function testMail(req, res) {
  var emailDataDT = {
    riderName: "",
    partName: "",
    eventName: "",
    eventEmail: "",
    phoneNo: "",
    partPhoneNo: "",
    address: "",
    amountPaid: "",
    orderId: "",
    partOrderId: "",
    riderEmail: "",
    partRiderEmail: "",
    url: "https://uat.royalenfield.com"
  };
  const localsDT = Object.assign({}, { data: emailDataDT });

  emailJoinRide.render(localsDT, (err, results) => {
    console.log(err);
    emailApi.sendEmailApi(
      "Congratulations!Order Placed with Royal Enfield",
      "Thank for Booking Interceptor 650.",
      results.html,
      "haritikachopra55@gmail.com",
      "error sending mail",
      "mail sent successfully",
      "sales@royalenfield.com"
    );
  });
  res.send("Mail Sent");
}

export default {
  connectSftp,
  bikeCsvToJson,
  dealerDataToDB,
  cityStatesData,
  dealerPageCreation,
  testMail
};
