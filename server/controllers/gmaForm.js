import GmaForm from "../models/gmaform";
import emailApi from "../service/emailApi";
import { EmailTemplate } from "email-templates";
import path from "path";
import config from "../../config/env";
const emailDir = path.resolve(__dirname, "../templates/mailers", "GMA-enquiry");

const emailDir1 = path.resolve(__dirname, "../templates/mailers", "gma");
const emailgma = new EmailTemplate(path.join(emailDir));
const emailgma1 = new EmailTemplate(path.join(emailDir1));
var resObj = {};

function create(req, res, next) {
  const gmaForm = new GmaForm({
    firstName: req.body.firstName,
    lname: req.body.lname,
    email: req.body.email,
    mobile: req.body.mobile,
    city: req.body.city,
    pinCode: req.body.pinCode,
    productSKU: req.body.productSKU,
    enquiry: req.body.enquiry
  });

  gmaForm
    .save()
    .then(doc => {
      var emailData = {
        userName: doc.firstName
      };
      var emailData1 = {
        firstName: doc.firstName,
        lname: doc.lname,
        email: doc.email,
        mobile: doc.mobile,
        city: doc.city,
        pinCode: doc.pinCode,
        productSKU: doc.productSKU,
        enquiry: doc.enquiry,
        url: config.websiteUrl
      };
      const locals = Object.assign({}, { data: emailData });
      const locals1 = Object.assign({}, { data: emailData1 });

      emailgma1.render(locals1, (err, results) => {
        var email1 = emailApi.sendEmailApi(
          "New GMA Enquiry",
          "Thank you for Registeration",
          results.html,
          req.body.productEmail,
          "error sending mail",
          "mail sent successfully"
        );
      });
      emailgma.render(locals, (err, results) => {
        var email = emailApi.sendEmailApi(
          "Thank you for enquiry",
          "Thank you for Registeration",
          results.html,
          doc.email,
          "error sending mail",
          "mail sent successfully",
          "support@royalenfield.com"
        );
      });
      resObj.success = true;
      resObj.message = "form successfully submitted";
      resObj.data = {};
      res.send(resObj);
    })
    .catch(err => res.send(err));
}

export default {
  create
};
