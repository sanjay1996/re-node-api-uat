
import SocialFeeds from '../models/socialfeeds'
var util = require('util');


function insert(req, res, next) {
//console.log("=======================================locobuzz request======================");
//console.log(req.body.socialFeeds);
//console.log("=======================================locobuzz request========================");
	var flag = true
	var arr = []

	for (const item of req.body.socialFeeds) {
		if (util.isArray(item.Metatag)) {
			arr.push({
				HashTag: item.HashTag, ListOfTags: item.ListOfTags, UserName: item.UserName
				, UserId: item.UserId, Metatag: item.Metatag, Text: item.Text, SocialFeedImage: item.SocialFeedImage,
				Likes: item.Likes, ReTweets: item.ReTweets, SocialLinks: item.SocialLinks,
				SocialSource: item.SocialSource, isApproved: item.isApproved,
				isBrandPost: item.isBrandPost,
				moderationReference: item.HashTag + "_" + Math.floor((Math.random() * 9) + 1) + Math.floor((Math.random() * 9) + 1) + Math.floor((Math.random() * 9) + 1) + Math.floor((Math.random() * 9) + 1) + Math.floor((Math.random() * 9) + 1)
			})
		}
		else {
			flag = false;
		}
	}

	SocialFeeds
		.insertMany(arr)
		.then((doc) => {
			if (flag == true) {
				res.status(200).send('Data inserted Successfully')
			}
			else {
				res.status(500).send('fields inappropriate')
			}

		})
		.catch((err) => {
			console.log(err);
		})
}


export default { insert };
