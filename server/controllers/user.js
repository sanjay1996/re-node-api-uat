//import bcrypt from 'bcrypt';
import httpStatus from "http-status";
import jwt from "jsonwebtoken";
import APIError from "../helpers/APIError";
import config from "../../config/env";
import User from "../models/user";
import request from "request";
import moment from "moment";
import base64 from "base64-img";
import TripStory from "../models/tripStory";
import emailApi from "../service/emailApi";
import { EmailTemplate } from "email-templates";
import path from "path";
var solrClient = require("solr-client");

var geoip = require("geoip-lite");

var indexer = solrClient.createClient({
  host: config.solr,
  port: 8983,
  path: "/solr",
  core: "new_core",
  solrVersion: 721
});

const emailDir = path.resolve(__dirname, "../templates/mailers", "sign-up");

const emailregistration = new EmailTemplate(path.join(emailDir));

function testingroute(req, res, next) {
  res.send(req.headers);
  var ip;
  if (req.headers["x-forwarded-for"]) {
    ip = req.headers["x-forwarded-for"].split(",")[0];
  } else if (req.connection && req.connection.remoteAddress) {
    ip = req.connection.remoteAddress;
  } else {
    ip = req.ip;
  }
  var geo = geoip.lookup(ip);
}

/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */

function create(req, res, next) {
  var language = req.headers["x-custom-language"];
  var country = req.headers["x-custom-country"];

  // returnObj defination
  const returnObj = {
    success: true,
    message: "",
    data: {}
  };

  // find User if exists and update
  User.findOneAsync({
    $or: [
      { $and: [{ email: req.body.email }, { phoneNo: req.body.phoneNo }] },
      { $or: [{ email: req.body.email }, { phoneNo: req.body.phoneNo }] }
    ]
  }).then(foundUser => {
    if (foundUser !== null) {
      returnObj.message = "User Already Exist`s";
      returnObj.success = false;
      res.send(returnObj);

      User.findOneAndUpdateAsync({ _id: foundUser._id }, { $set: { loginStatus: true } }, { new: true }) //eslint-disable-line
        // eslint-disable-next-line
        .then(updateUserObj => {
          if (updateUserObj) {
            const jwtAccessToken = jwt.sign(updateUserObj, config.jwtSecret);
            returnObj.message = "User Already Exists !";
            returnObj.success = false;
          }
        })
        .error(e => {
          console.log(e);
          const err = new APIError(
            `error in updating user details while login ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          next(err);
        });
    } else {
      const otpValue = Math.floor(100000 + Math.random() * 900000); //eslint-disable-line
      const emailToken = Math.floor(700000000000 + Math.random() * 900000); //eslint-disable-line
      var bikeId = req.body.bikeOwned
        ? [{ bikeId: req.body.bikeOwned[0].split("_")[1] }]
        : [];

      var user;
      if (req.body.moto == "no") {
        user = new User({
          email: req.body.email,
          password: req.body.password,
          userType: req.body.userType,
          fname: req.body.fname,
          lname: req.body.lname,
          ownBike: req.body.moto,
          phoneNo: req.body.phoneNo,
          otp: otpValue,
          emailToken: emailToken,
          loginStatus: true,
          gender: req.body.gender,
          addressInfo: { city: req.body.city },
          dob: req.body.dob,
	verifiedAccount : {status : "Pending"},
          locality: {
            country: country,
            language: language
          }
        });
      } else {
        user = new User({
          email: req.body.email,
          password: req.body.password,
          userType: req.body.userType,
          fname: req.body.fname,
          lname: req.body.lname,
          ownedBikeInfo: bikeId,
          ownBike: req.body.moto,
          phoneNo: req.body.phoneNo,
          otp: otpValue,
          emailToken: emailToken,
          loginStatus: true,
          gender: req.body.gender,
          addressInfo: { city: req.body.city },
          dob: req.body.dob,
	verifiedAccount : {status : "Pending"},
          locality: {
            country: country,
            language: language
          }
        });
      }
      user
        .saveAsync()
        .then(savedUser => {
          const returnObj = {
            success: true,
            message: "",
            data: {}
          };
          var data = {
            pagePath:
              "/content/royal-enfield/" +
              savedUser.locality.country +
              "/" +
              savedUser.locality.language +
              "/" +
              "home/users/user-profiles.html?userid=" +
              savedUser._id
          };
          //---------------------------------------
          User.findOne({ _id: savedUser._id })
            .select({
              password: 0,
              addressInfo: 0,
              mobileVerified: 0,
              emailVerified: 0,
              otp: 0,
              emailToken: 0,
              ownedBikeInfo: 0,
              userType: 0,
              loginStatus: 0,
              jwtAccessToken: 0
            })
            .then(updatedUser => {
              var emailData = {
                userName: savedUser.fname + " " + savedUser.lname,
                url:
                  config.websiteUrl +
                  data.pagePath.split("/content/royal-enfield")[1],
                webUrl: config.websiteUrl
              };
              const locals = Object.assign({}, { data: emailData });
              emailregistration.render(locals, (err, results) => {
                console.log(err);

                emailApi.sendEmailApi(
                  "Congratulations on getting new Royal Enfield Account",
                  "Thank you for Registeration",
                  results.html,
                  savedUser.email,
                  "error sending mail",
                  "mail sent successfully"
                );
              });
              updatedUser.userUrl = data.pagePath;
              updatedUser.save();
              const jwtAccessToken = jwt.sign(savedUser, config.jwtSecret);
              returnObj.data.jwtAccessToken = `JWT ${jwtAccessToken}`;

              var obj = {
                email: savedUser.email,
                profilePicture: { srcPath: savedUser.profilePicture },
                userId: savedUser._id,
                phone: savedUser.phoneNo,
                firstName: savedUser.fname,
                lastName: savedUser.lname,
                verifiedAccount: savedUser.verifiedAccount
              };
              sendOtpForRegisteredUser(savedUser, savedUser.phoneNo, savedUser.locality.country);
              returnObj.data.user = obj;
              returnObj.message = "user created successfully";
              res.send(returnObj);
              savingFunction(updatedUser, data.pagePath);
            });
        })
        .error(e => {
          next(e);
          console.log(e);
        });
    }
  });
}

var savingFunction = function(user, pagePath) {
  return new Promise((resolve, reject) => {
    var obj = {
      name: user.fname,
      email: user.email,
      id: user._id,
      country: user.locality.country,
      language: user.locality.language,
      entity: "user",
      url: pagePath,
      title: user.fname
    };

    indexer.add(obj, function(err, obj) {
      if (err) {
        resolve();
        console.log(err);
      } else {
        var options = {
          waitSearcher: false
        };
        indexer.commit(options, function(err, obj) {
          if (err) {
            console.log(err);
            resolve();
          } else {
            resolve();
          }
        });
      }
    });
  });
};

function forgot(req, res, next) {
  const emailDirForget = path.resolve(
    __dirname,
    "../templates/mailers",
    "reset-password"
  );
  const emailForget = new EmailTemplate(path.join(emailDirForget));

  const returnObj = {
    success: true,
    message: "",
    data: {}
  };
  var emailregex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var phoneNo = req.body.phoneNo;
  if (emailregex.test(phoneNo)) {
    User.findOne({ email: phoneNo })
      .then(foundUser => {
        const otpValue = Math.floor(100000 + Math.random() * 900000);
        foundUser.otp = otpValue;
        foundUser.save();
        var emailData = {
          name: foundUser.fname + " " + foundUser.lname,
          otp: otpValue,
          url: config.websiteUrl
        };
        const locals = Object.assign({}, { data: emailData });
        emailForget.render(locals, (err, results) => {
          var emailResponse = emailApi.sendEmailApi(
            "Reset your royal enfield password",
            `Royalenfield account reset Request`,
            results.html,
            phoneNo,
            "error sending otp",
            "otp sent successfully"
          );
        });
        returnObj.success = true;
        returnObj.message = "otp sent via mail";
        res.status(200).send(returnObj);
      })
      .catch(err => {
        returnObj.success = false;
        returnObj.message = "error mailing otp";
        res.send(returnObj);
      });
  } else {
    if (phoneNo) {
      User.findOne({ phoneNo: phoneNo })
        .then(user => {
          if (user) {
            const otpValue = Math.floor(100000 + Math.random() * 900000);
            user.otp = otpValue;
            user.save();
            var finalNumber;
            var num = phoneNo.slice(-10);
            var country = req.headers["x-custom-country"];
            switch (country) {
              case "in":
                finalNumber = "91" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      // returnObj.data = finalNumber
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "au":
                finalNumber = "61" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "us":
                finalNumber = "1" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "uk":
                finalNumber = "44" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "ar":
                finalNumber = "54" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "ca":
                finalNumber = "1" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "mx":
                finalNumber = "52" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "co":
                finalNumber = "57" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "es":
                finalNumber = "34" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "br":
                finalNumber = "55" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "pt":
                finalNumber = "351" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "th":
                finalNumber = "66" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "id":
                finalNumber = "62" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "fr":
                finalNumber = "33" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "it":
                finalNumber = "39" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "de":
                finalNumber = "49" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "vn":
                finalNumber = "84" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
            }
          } else {
            returnObj.success = false;
            returnObj.message = "user Not found";
            res.status(404).send(returnObj);
          }
        })
        .catch(e => {
          console.log(e);
          res.send("something went wrong");
        });
    } else {
      res.status(406).send("please enter the phone no.");
    }
  }
}

function sendOtp(req, res, next) {
  const emailDirForget = path.resolve(__dirname, "../templates/mailers", "reset-password");
  const emailForget = new EmailTemplate(path.join(emailDirForget));

  const returnObj = {
    success: true,
    message: "",
    data: {}
  };
  var emailregex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var phoneNo = req.body.userId;
  if (emailregex.test(phoneNo)) {
    User.findOne({ email: phoneNo })
      .then(foundUser => {
        const otpValue = Math.floor(100000 + Math.random() * 900000);
        foundUser.otp = otpValue;
        foundUser.save();
        var emailData = {
          name: foundUser.fname + " " + foundUser.lname,
          otp: otpValue,
          url: config.websiteUrl
        };
        const locals = Object.assign({}, { data: emailData });
        emailForget.render(locals, (err, results) => {
          var emailResponse = emailApi.sendEmailApi(
            "Reset your royal enfield password",
            `Royalenfield account reset Request`,
            results.html,
            phoneNo,
            "error sending otp",
            "otp sent successfully"
          );
        });
        returnObj.success = true;
        returnObj.message = "otp sent successfully";
        res.status(200).send(returnObj);
      })
      .catch(err => {
        returnObj.success = false;
        returnObj.message = "error sending otp";
        res.send(returnObj);
      });
  } else {
    if (phoneNo) {
      User.findOne({ phoneNo: phoneNo })
        .then(user => {
          if (user) {
            const otpValue = Math.floor(100000 + Math.random() * 900000);
            user.otp = otpValue;
            user.save();
            var finalNumber;
            var num = phoneNo.slice(-10);
            var country = req.headers["x-custom-country"];
            switch (country) {
              case "in":
                finalNumber = "91" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    console.log("=================body===================");
                    console.log(body);
                    console.log("=================body===================");

                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      // returnObj.data = finalNumber
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "au":
                finalNumber = "61" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "us":
                finalNumber = "1" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "uk":
                finalNumber = "44" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "ar":
                finalNumber = "54" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "ca":
                finalNumber = "1" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "mx":
                finalNumber = "52" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "co":
                finalNumber = "57" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "es":
                finalNumber = "34" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "br":
                finalNumber = "55" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "pt":
                finalNumber = "351" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "th":
                finalNumber = "66" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "id":
                finalNumber = "62" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "fr":
                finalNumber = "33" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "it":
                finalNumber = "39" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "de":
                finalNumber = "49" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
              case "vn":
                finalNumber = "84" + num;
                request(
                  `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
                  (err, response, body) => {
                    if (err) {
                      returnObj.success = false;
                      returnObj.message = "error sending otp";
                      returnObj.data = finalNumber;
                      res.send(returnObj);
                    } else {
                      returnObj.success = true;
                      returnObj.message = "otp sent successfully";
                      res.status(200).send(returnObj);
                      setTimeout(() => {
                        user.otp = "";
                      }, config.otpExipryTime);
                    }
                  }
                );
                break;
            }
          } else {
            returnObj.success = false;
            returnObj.message = "user Not found";
            res.status(404).send(returnObj);
          }
        })
        .catch(e => {
          console.log(e);
          res.send("something went wrong");
        });
    } else {
      res.status(406).send("please enter the phone no.");
    }
  }
}

/**
 * Update existing user
 * @property {Object} req.body.user - user object containing all fields.
 * @returns {User}
 */
function update(req, res, next) {
  User.findOne({ _id: req.body.userId }).then(user => {
    if (user) {
      var oldPassword = req.body.password;
      user.comparePassword(oldPassword, function(err, isMatch) {
        if (err) throw err;
        if (isMatch === true && user.email == req.body.email) {
          user.fname = req.body.fname ? req.body.fname : user.fname;
          user.lname = req.body.lname ? req.body.lname : user.lname;
          user.dob = req.body.dob ? req.body.dob : user.dob;
          user.gender = req.body.gender ? req.body.gender : user.gender;
          user.phoneNo = req.body.phoneNo ? req.body.phoneNo : user.phoneNo;
          user.bikename = req.body.bikename ? req.body.bikename : user.bikename;
          user.city = req.body.city ? req.body.city : user.city;
          user.aboutMe = req.body.aboutMe;
          user.ownBike = req.body.moto ? req.body.moto : user.ownBike;
          user.saveAsync().then(savedUser => {
            var retObj = {};
            retObj.success = true;
            retObj.message = "Profile Successfully Updated";
            retObj.data = savedUser;
            res.send(retObj);
          });
        } else {
          var retObj = {};
          retObj.message = "Password doesnt match";
          retObj.success = false;
          retObj.data = {};
          res.send(retObj);
        }
      });
    } else {
      var retObj = {};
      retObj.message = "User doesnt exist";
      retObj.success = false;
      retObj.data = {};
      res.send(retObj);
    }
  });
}

function resetPassword(req, res) {
  var resObj = {};

  User.findOne({ $or: [{ phoneNo: req.body.phoneNo }, { email: req.body.phoneNo }] }).then(user => {
    if (user) {
      if (user.otp == req.body.otp) {
        resObj.success = true;
        resObj.message = "password updated successfully";
        resObj.data = {};

        var password = req.body.password;
        user.password = password;
        user.save().then(doc => {
          res.send(resObj);
        });
      } else {
        resObj.success = false;
        resObj.message = "password failed to update";
        resObj.data = {};
      }
    } else {
      resObj.success = false;
      resObj.message = "no such user exists";
      resObj.data = {};
      res.send(resObj);
    }
  });
}

function getUserDetails(req, res, next) {
  var id = req.query.jsonString;
  var tripImageLength = 0;
  var arr = [];
  User.findOne({ _id: id })
    .select({
      password: 0,
      mobileVerified: 0,
      emailVerified: 0,
      otp: 0,
      emailToken: 0,
      userType: 0,
      loginStatus: 0,
      jwtAccessToken: 0
    })
    .populate("ridesCreated tripStoriesCreated discussionJoined ridesJoined")
    .then(doc => {
      if (doc) {
        var d = new Date();
        var n = d.toISOString();

        var upcomingRides = [];
        var pastRides = [];
        if (doc.ridesJoined.length != 0) {
          for (var i = 0; i < doc.ridesJoined.length; i++) {
            var verDate = moment(
              doc.ridesJoined[i].startDate,
              "DD-MM-YYYY"
            ).format("YYYY-MM-DD");
            if (moment(verDate).isAfter(moment().format("YYYY-MM-DD"))) {
              upcomingRides.push({
                rideName: doc.ridesJoined[i].rideName,
                rideTypeText: doc.ridesJoined[i].rideCategory,
                rideDetails: doc.ridesJoined[i].rideDetails,
                pageUrl: doc.ridesJoined[i].ridePageUrl,
                startPoint: { name: doc.ridesJoined[i].startPoint.name },
                endPoint: { name: doc.ridesJoined[i].endPoint.name },
                startDate: doc.ridesJoined[i].startDate,
                durationInDays: doc.ridesJoined[i].durationInDays
              });
            } else {
              pastRides.push({
                rideName: doc.ridesJoined[i].rideName,
                rideTypeText: doc.ridesJoined[i].rideCategory,
                rideDetails: doc.ridesJoined[i].rideDetails,
                pageUrl: doc.ridesJoined[i].ridePageUrl,
                startPoint: { name: doc.ridesJoined[i].startPoint.name },
                endPoint: { name: doc.ridesJoined[i].endPoint.name },
                startDate: doc.ridesJoined[i].startDate,
                durationInDays: doc.ridesJoined[i].durationInDays
              });
            }
          }
        }

        var usrRides = [];
        for (var i = 0; i < doc.ridesCreated.length; i++) {
          usrRides.push({
            rideName: doc.ridesCreated[i].rideName,
            rideTypeText: doc.ridesCreated[i].rideCategory,
            rideDetails: doc.ridesCreated[i].rideDetails,
            pageUrl: doc.ridesCreated[i].ridePageUrl,
            startPoint: { name: doc.ridesCreated[i].startPoint.name },
            endPoint: { name: doc.ridesCreated[i].endPoint.name },
            startDate: doc.ridesCreated[i].startDate,
            durationInDays: doc.ridesCreated[i].durationInDays
          });
        }
        var bikeArr = [];
        for (var i = 0; i < doc.ownedBikeInfo.length; i++) {
          bikeArr.push({
            bikeId: doc.ownedBikeInfo[i].bikeId,
            vehicleNo: doc.ownedBikeInfo[i].vehicleNo
          });
        }

        var tripStoriesSend = [];
        for (var i = 0; i < doc.tripStoriesCreated.length; i++) {
          tripStoriesSend.push({
            title: doc.tripStoriesCreated[i].storyTitle,
            summary: doc.tripStoriesCreated[i].storySummary,
            pageUrl: doc.tripStoriesCreated[i].storyUrl,
            thumbnailImagePath:
              doc.tripStoriesCreated[i].tripStoryImages.length != 0
                ? doc.tripStoriesCreated[i].tripStoryImages[0].srcPath
                : ""
          });
          tripImageLength += doc.tripStoriesCreated[i].tripStoryImages.length;
        }

        for (var i = 0; i < doc.discussionJoined.length; i++) {
          let time = moment(doc.discussionJoined[i].postedOn).fromNow();
          doc.discussionJoined[i].timeAgo = time;
        }
        if (doc.ownBike == "no") {
          var bikeNameInfo = [];
        } else {
          var bikeNameInfo = bikeArr;
        }
        var obj = {
          profilePageUrl: doc.userUrl,
          firstName: doc.fname ? doc.fname : "",
          address: {
            addressAsText: doc.addressInfo.city ? doc.addressInfo.city : ""
          },
          lastName: doc.lname ? doc.lname : "",
          coverImage: { srcPath: doc.coverImage ? doc.coverImage : "" },
          profilePicture: {
            srcPath: doc.profilePicture ? doc.profilePicture : ""
          },
          ownerBikeDetails: doc.bikeName ? doc.bikeName : "",
          aboutMe: doc.aboutMe ? doc.aboutMe : "",
          listofTags: doc.listofTags ? doc.listofTags : [],
          ridesCreated: doc.ridesCreated.length ? doc.ridesCreated.length : 0,
          ridesByUser: usrRides,
          tripStoriesCreated: doc.tripStoriesCreated
            ? doc.tripStoriesCreated.length
            : 0,
          imagesUploaded: tripImageLength ? tripImageLength : 0,
          gender: doc.gender ? doc.gender : "",
          address: { city: doc.addressInfo.city ? doc.addressInfo.city : "" },
          dob: moment()
            .format(doc.dob)
            .toString(),
          email: doc.email,
          ownBike: doc.ownBike ? doc.ownBike : "",
          discussionJoined:
            doc.discussionJoined.length != 0 ? doc.discussionJoined : [],
          phone: doc.phoneNo,
          tripStories: tripStoriesSend,
          pastRides: pastRides,
          upcomingRides: upcomingRides,
          motorcyclesOwned: bikeArr
        };

        res.send(obj);
      } else {
        res.status(500).send({ firstName: "Not Found" });
      }
    })
    .catch(e => {
      res.send(e);
    });
}

function getUserDetailsMobile(req, res, next) {
  var id = req.query.jsonString;
  var tripImageLength = 0;
  var arr = [];
  User.findOne({ _id: id })
    .select({
      password: 0,
      mobileVerified: 0,
      emailVerified: 0,
      otp: 0,
      emailToken: 0,
      userType: 0,
      loginStatus: 0,
      jwtAccessToken: 0
    })
    .populate("ridesCreated tripStoriesCreated discussionJoined ridesJoined ownedBikeInfo.bikeId")
    .then(doc => {
      if (doc) {
        var d = new Date();
        var n = d.toISOString();

        var upcomingRides = [];
        var pastRides = [];
        if (doc.ridesJoined.length != 0) {
          for (var i = 0; i < doc.ridesJoined.length; i++) {
            var verDate = moment(doc.ridesJoined[i].startDate, "DD-MM-YYYY").format("YYYY-MM-DD");
            if (moment(verDate).isAfter(moment().format("YYYY-MM-DD"))) {
              upcomingRides.push({
                rideName: doc.ridesJoined[i].rideName,
                rideTypeText: doc.ridesJoined[i].rideCategory,
                rideDetails: doc.ridesJoined[i].rideDetails,
                pageUrl: doc.ridesJoined[i].ridePageUrl,
                startPoint: { name: doc.ridesJoined[i].startPoint.name },
                endPoint: { name: doc.ridesJoined[i].endPoint.name },
                startDate: doc.ridesJoined[i].startDate,
                durationInDays: doc.ridesJoined[i].durationInDays
              });
            } else {
              pastRides.push({
                rideName: doc.ridesJoined[i].rideName,
                rideTypeText: doc.ridesJoined[i].rideCategory,
                rideDetails: doc.ridesJoined[i].rideDetails,
                pageUrl: doc.ridesJoined[i].ridePageUrl,
                startPoint: { name: doc.ridesJoined[i].startPoint.name },
                endPoint: { name: doc.ridesJoined[i].endPoint.name },
                startDate: doc.ridesJoined[i].startDate,
                durationInDays: doc.ridesJoined[i].durationInDays
              });
            }
          }
        }

        var usrRides = [];
        for (var i = 0; i < doc.ridesCreated.length; i++) {
          usrRides.push({
            rideName: doc.ridesCreated[i].rideName,
            rideTypeText: doc.ridesCreated[i].rideCategory,
            rideDetails: doc.ridesCreated[i].rideDetails,
            pageUrl: doc.ridesCreated[i].ridePageUrl,
            startPoint: { name: doc.ridesCreated[i].startPoint.name },
            endPoint: { name: doc.ridesCreated[i].endPoint.name },
            startDate: doc.ridesCreated[i].startDate,
            durationInDays: doc.ridesCreated[i].durationInDays
          });
        }
        var bikeArr = [];
        for (var i = 0; i < doc.ownedBikeInfo.length; i++) {
          bikeArr.push({
            bikeId: doc.ownedBikeInfo[i].bikeId,
            vehicleNo: doc.ownedBikeInfo[i].vehicleNo
          });
        }

        var tripStoriesSend = [];
        for (var i = 0; i < doc.tripStoriesCreated.length; i++) {
          tripStoriesSend.push({
            title: doc.tripStoriesCreated[i].storyTitle,
            summary: doc.tripStoriesCreated[i].storySummary,
            pageUrl: doc.tripStoriesCreated[i].storyUrl,
            thumbnailImagePath:
              doc.tripStoriesCreated[i].tripStoryImages.length != 0
                ? doc.tripStoriesCreated[i].tripStoryImages[0].srcPath
                : ""
          });
          tripImageLength += doc.tripStoriesCreated[i].tripStoryImages.length;
        }

        for (var i = 0; i < doc.discussionJoined.length; i++) {
          let time = moment(doc.discussionJoined[i].postedOn).fromNow();
          doc.discussionJoined[i].timeAgo = time;
        }
        if (doc.ownBike == "no") {
          var bikeNameInfo = [];
        } else {
          var bikeNameInfo = bikeArr;
        }
        var obj = {
          profilePageUrl: doc.userUrl,
          firstName: doc.fname ? doc.fname : "",
          address: {
            addressAsText: doc.addressInfo.city ? doc.addressInfo.city : ""
          },
          lastName: doc.lname ? doc.lname : "",
          coverImage: { srcPath: doc.coverImage ? doc.coverImage : "" },
          profilePicture: {
            srcPath: doc.profilePicture ? doc.profilePicture : ""
          },
          ownerBikeDetails: doc.bikeName ? doc.bikeName : "",
          aboutMe: doc.aboutMe ? doc.aboutMe : "",
          listofTags: doc.listofTags ? doc.listofTags : [],
          ridesCreated: doc.ridesCreated.length ? doc.ridesCreated.length : 0,
          ridesByUser: usrRides,
          tripStoriesCreated: doc.tripStoriesCreated ? doc.tripStoriesCreated.length : 0,
          imagesUploaded: tripImageLength ? tripImageLength : 0,
          gender: doc.gender ? doc.gender : "",
          address: { city: doc.addressInfo.city ? doc.addressInfo.city : "" },
          dob: moment()
            .format(doc.dob)
            .toString(),
          email: doc.email,
          ownBike: doc.ownBike ? doc.ownBike : "",
          discussionJoined: doc.discussionJoined.length != 0 ? doc.discussionJoined : [],
          phone: doc.phoneNo,
          tripStories: tripStoriesSend,
          pastRides: pastRides,
          upcomingRides: upcomingRides,
          motorcyclesOwned: doc.ownedBikeInfo
        };

        res.send(obj);
      } else {
        res.status(500).send({ firstName: "Not Found" });
      }
    })
    .catch(e => {
      res.send(e);
    });
}

function userInterests(req, res, next) {
  var id = req.body.userId;
  User.findOneAndUpdate(
    { _id: id },
    { $set: { listofTags: [] } },
    { new: true }
  )
    .then(docs => {
      var i;
      for (i = 0; i < req.body.interests.length; i++) {
        docs.listofTags.push({ userTags: req.body.interests[i] });
      }
      docs.save().then(user => {
        res.send("Updated User Interests");
      });
    })
    .catch(e => console.log(e));
}

function updateCoverImage(req, res, next) {
  User.findOne({ _id: req.body.userId }).then(doc => {
    var image = req.body.image;
    var path = "./node/assets/User/coverImage";
    base64.img(image, path, doc._id, function(err, filepath) {
      const returnObj = {
        success: true,
        message: "",
        data: {}
      };
      if (err) {
        returnObj.success = false;
        returnObj.message = "error updating image";
        returnObj.data = err;
        res.send(returnObj);
      } else {
        User.findByIdAndUpdate(
          { _id: req.body.userId },
          { $set: { coverImage: "/" + filepath } }
        ).then(
          () => {
            returnObj.message = "updated Successfully";
            res.send(returnObj);
          },
          err => {
            returnObj.success = false;
            returnObj.message = "error updating image";
            returnObj.data = err;
            res.send(returnObj);
          }
        );
      }
    });
  });
}

function updateProfileImage(req, res, next) {
  User.findOne({ _id: req.body.userId }).then(doc => {
    var image = req.body.image;
    var path = "./node/assets/User/ProfileImage";
    base64.img(image, path, doc._id, function(err, filepath) {
      const returnObj = {
        success: true,
        message: "",
        data: {}
      };
      if (err) {
        returnObj.success = false;
        returnObj.message = "error updating image";
        returnObj.data = err;
        res.send(returnObj);
      } else {
        User.findByIdAndUpdate({ _id: req.body.userId }, { $set: { profilePicture: "/" + filepath } }).then(
          () => {
            returnObj.message = "updated Successfully";
            res.send(returnObj);
          },
          err => {
            returnObj.success = false;
            returnObj.message = "error updating image";
            returnObj.data = err;
            res.send(returnObj);
          }
        );
      }
    });
  });
}

function deleteProfileImage(req, res, next) {
  var dummyProfileImage = "/node/assets/User/ProfileImage/profile_image_dummy.jpg";
  const returnObj = {
    success: true,
    message: "",
    data: {}
  };

  User.findByIdAndUpdate({ _id: req.body.userId }, { $set: { profilePicture: dummyProfileImage } }).then(
    () => {
      returnObj.message = "deleted Successfully";
      returnObj.data = { dummyImage: dummyProfileImage };
      res.send(returnObj);
    },
    err => {
      returnObj.success = false;
      returnObj.message = "deletion failed";
      res.status(204).send(returnObj);
    }
  );
}

function deleteCoverImage(req, res, next) {
  var dummyCoverImage = "/node/assets/User/coverImage/cover-image.jpg";
  const returnObj = {
    success: true,
    message: "",
    data: {}
  };

  User.findByIdAndUpdate({ _id: req.body.userId }, { $set: { coverImage: dummyCoverImage } }).then(
    () => {
      returnObj.message = "deleted Successfully";
      returnObj.data = { dummyImage: dummyCoverImage };
      res.send(returnObj);
    },
    err => {
      returnObj.success = false;
      returnObj.message = " deletion failed";
      res.status(204).send("all good");
    }
  );
}

function suggestedTripStories(req, res, next) {
  var userInterestArr = [];
  var matched = [];
  var userId = req.query.jsonString;
  User.findOne({ _id: userId }).then(userFound => {
    for (var i = 0; i < userFound.listofTags.length; i++) {
      userInterestArr.push(userFound.listofTags[i].userTags);
    }
    TripStory.find().then(tripStoryarr => {
      var tripStories = tripStoryarr;
      for (var i = 0; i < userInterestArr.length; i++) {
        for (var j = 0; j < tripStories.length; j++) {
          for (var k = 0; k < tripStories[j].storyTags.length; k++) {
            if (userInterestArr[i] == tripStories[j].storyTags[k]) {
              matched.push(tripStories[j]);
            }
          }
        }
      }

      let unique = [];
      if (matched.length != 0) {
        unique = [...new Set(matched.map(item => item._id))];
      }

      var finalResponse = [];
      for (var i = 0; i < tripStories.length; i++) {
        for (var j = 0; j < unique.length; j++) {
          if (unique[j] == tripStories[i]._id) {
            finalResponse.push({
              thumbnailImagePath:
                tripStories[i].tripStoryImages.length != 0
                  ? tripStories[i].tripStoryImages[0].srcPath
                  : "",
              title: tripStories[i].storyTitle,
              summary: tripStories[i].storySummary,
              pageUrl: tripStories[i].storyUrl
            });
          }
        }
      }
      res.send({ tripStories: finalResponse });
    });
  });
}

function upcomingRides(req, res, next) {
  Rides.find().then(doc => {
    for (var i = 0; i < doc.length; i++) {
      var year = doc[i].startDate[6] + doc[i].startDate[7] + doc[i].startDate[8] + doc[i].startDate[9];
      var date = doc[i].startDate[0] + doc[i].startDate[1];
      var month = doc[i].startDate[3] + doc[i].startDate[4];
      var myDate = new Date(year, month, date);
      if (myDate > Date.now()) {
      }
    }
  });
}

function reviewForUser(req, res, next) {
  var arr = [];
  User.findOne({ _id: req.query.jsonString })
    .populate({
      path: "reviewCreated",
      options: { sort: "-reviewDateText" }
    })

    .then(doc => {
      for (var i = 0; i < doc.reviewCreated.length; i++) {
        arr.push({
          reviewText: doc.reviewCreated[0].reviewText,
          averageRatingPercentage: doc.reviewCreated[i].averageRatingPercentage,
          reviewByUser: {
            firstName: doc.fname,
            profilePageUrl: doc.userUrl,
            profilePicture: { srcPath: doc.profilePicture }
          },
          reviewCriterias: doc.reviewCreated[i].reviewCriterias,
          averageRating: doc.reviewCreated[i].averageRating,
          reviewDescription: doc.reviewCreated[i].reviewDescription,
          reviewDateText: moment(doc.reviewCreated[i].reviewDateText).format(
            "YYYY-MM-DD"
          )
        });
      }
      res.send(arr[0]);
    });
}

//Function for sending otp for registered User

function sendOtpForRegisteredUser(user, phoneNo, userCountry) {
  if (user) {
    const otpValue = Math.floor(100000 + Math.random() * 900000);
    user.otp = otpValue;
    user.save();
    var finalNumber;
    var num = phoneNo.slice(-10);
    var country = userCountry;
    switch (country) {
      case "in":
        finalNumber = "91" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "au":
        finalNumber = "61" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "us":
        finalNumber = "1" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "uk":
        finalNumber = "44" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "ar":
        finalNumber = "54" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "ca":
        finalNumber = "1" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "mx":
        finalNumber = "52" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "co":
        finalNumber = "57" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "es":
        finalNumber = "34" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "br":
        finalNumber = "55" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "pt":
        finalNumber = "351" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "th":
        finalNumber = "66" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "id":
        finalNumber = "62" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "fr":
        finalNumber = "33" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "it":
        finalNumber = "39" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "de":
        finalNumber = "49" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
      case "vn":
        finalNumber = "84" + num;
        request(
          `https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=refildotp&subEnterpriseid=refildotp&pusheid=refildotp&pushepwd=refildotp1&msisdn=${finalNumber}&sender=RECARE&msgtext=Dear user your one time password is ${otpValue}. Please don't share this with any one. Thanks RE team.&intflag=fasle`,
          (err, response, body) => {
            if (err) {
            } else {
              setTimeout(() => {
                user.otp = "";
              }, config.otpExipryTime);
            }
          }
        );
        break;
    }
  }
}

export default {
  testingroute,
  reviewForUser,
  suggestedTripStories,
  create,
  update,
  forgot,
  sendOtp,
  getUserDetails,
  getUserDetailsMobile,
  userInterests,
  resetPassword,
  updateProfileImage,
  updateCoverImage,
  deleteCoverImage,
  deleteProfileImage,
  upcomingRides
};
