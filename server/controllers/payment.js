import qs from "querystring";
import crypto from "crypto";
import config from "../../config/env";
import DynamicFormSchema from "../models/dynamicForms";
import emailApi from "../service/emailApi";
import { EmailTemplate } from "email-templates";
import path from "path";

const emailDir = path.resolve(__dirname, "../templates/mailers", "join-ride-events");

const emailDirforTOB = path.resolve(__dirname, "../templates/mailers", "join-ride-tob");
const emailDirforDT = path.resolve(__dirname, "../templates/mailers", "join-ride-dirt-track");

const emailJoinRide = new EmailTemplate(path.join(emailDir));
const emailJoinRideforTOB = new EmailTemplate(path.join(emailDirforTOB));
const emailJoinRideforDT = new EmailTemplate(path.join(emailDirforDT));

function ccavRequestHandler(req, res, next) {
  var payObj = {
    merchant_id: "10292",
    order_id: Math.floor(1000000000 + Math.random() * 9000000000).toString(),
    currency: "INR",
    amount: "1.00",
    redirect_url: `${config.websiteUrl}/node/api/payment/ccavResponseHandler`,
    cancel_url: `${config.websiteUrl}/node/api/payment/ccavResponseHandler`,
    language: "EN",
    billing_name: req.body.fname + req.body.lname,
    billing_address: req.body.Address1,
    billing_city: req.body.city,
    billing_state: req.body.state,
    billing_zip: req.body.postalCode,
    billing_country: "India",
    billing_tel: req.body.mobileNumber,
    billing_email: req.body.email,
    delivery_name: req.body.fname + req.body.lname,
    delivery_address: req.body.Address1,
    delivery_city: req.body.city,
    delivery_state: req.body.state,
    delivery_zip: req.body.zipCode,
    delivery_country: "India",
    delivery_tel: req.body.mobileNumber,
    merchant_param1: req.body.formID,
    merchant_param2: req.body.email,
    merchant_param3: "additional + Info",
    merchant_param4: "additional + Info",
    merchant_param5: "additional + Info",
    promo_code: "",
    customer_identifier: ""
  };

  var body = "",
    workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
    accessCode = config.paymentGatewayAccessCode, //Put in the Access Code shared by CCAvenues.
    encRequest = "",
    formbody = "";

  body = `merchant_id=${payObj.merchant_id}&order_id=${payObj.order_id}&currency=${payObj.currency}&amount=${
    payObj.amount
  }&redirect_url=${payObj.redirect_url}&cancel_url=${payObj.cancel_url}&language=${payObj.language}&billing_name=${
    payObj.billing_name
  }&billing_address=${payObj.billing_address}&billing_city=${payObj.billing_city}&billing_state=${
    payObj.billing_state
  }&billing_zip=${payObj.billing_zip}&billing_country=${payObj.billing_country}&billing_tel=${
    payObj.billing_tel
  }&billing_email=${payObj.billing_email}&delivery_name=${payObj.delivery_name}&delivery_address=${
    payObj.delivery_address
  }&delivery_city=${payObj.delivery_city}&delivery_state=${payObj.delivery_state}&delivery_zip=${
    payObj.delivery_zip
  }&delivery_country=${payObj.delivery_country}&delivery_tel=${payObj.delivery_tel}&merchant_param1=${
    payObj.merchant_param1
  }&merchant_param2=${payObj.merchant_param2}&merchant_param3=${payObj.merchant_param3}&merchant_param4=${
    payObj.merchant_param4
  }&merchant_param5=${payObj.merchant_param5}&promo_code=${payObj.promo_code}&customer_identifier=${
    payObj.customer_identifier
  }`;
  encRequest = encrypt(body, workingKey);
  formbody = `<form id="nonseamless" method="POST" name="redirect" action=${
    config.paymentGatewayURL
  }> <input type="hidden" id="encRequest" name="encRequest" value=${encRequest}><input type="hidden" name="access_code" id="access_code" value=${accessCode}><script language="javascript">document.redirect.submit();</script></form>`;
  res.writeHeader(200, { "Content-Type": "text/html" });
  res.write(formbody);
  res.end();
  return;
}

function ccavResponseHandler(req, res, next) {
  var ccavEncResponse = "",
    ccavResponse = "",
    workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
    ccavPOST = "";
  console.log("====================================");
  console.log("Inside RESPONSJHBKN,");
  console.log("====================================");
  var encryption = req.body.encResp;
  ccavResponse = decrypt(encryption, workingKey);
  var pData = qs.parse(ccavResponse);
  console.log("=============pData=======================");
  console.log(pData);
  console.log("=============pData=======================");
  DynamicFormSchema.findOne({ formId: pData.merchant_param1 })
    .then(form => {
      form.formData.forEach((item, index) => {
        if (item.email == pData.merchant_param2 && pData.order_status == "Success") {
          item.paymentToken = pData.order_id;
          item.paymentSuccess = true;
          DynamicFormSchema.findOneAndUpdate({ formId: pData.merchant_param1 }, { $set: { formData: form.formData } })
            .then(savedForm => {
              if (form.formData.length - 1 == index) {
                var emailDataDT = {
                  riderName: pData.billing_name,
                  eventName: form.formConfig.sectionHeading,
                  eventEmail: form.formConfig.eventQueryEmail,
                  merchandiseQueryEmail: form.formConfig.merchandiseQueryEmail,
                  phoneNo: pData.billing_tel,
                  address:
                    item.Address1 +
                    " " +
                    item.Address2 +
                    "," +
                    item.city +
                    "," +
                    item.state +
                    "," +
                    item.country +
                    "," +
                    item.postalCode,
                  amountPaid: form.formConfig.paymentCurrency + " " + pData.amount,
                  // amountPaid: "INR 1",
                  orderId: item.paymentToken,
                  riderEmail: item.email
                };
                console.log("==============item======================");
                console.log(emailDataDT);
                console.log("==============item======================");

                const localsDT = Object.assign({}, { data: emailDataDT });
                emailJoinRide.render(localsDT, (err, results) => {
                  console.log(err);
                  var email = emailApi.sendEmailApi(
                    "Join Ride Confirmation",
                    "Thank you for Joining a ride",
                    results.html,
                    item.email,
                    "error sending mail",
                    "mail sent successfully"
                  );
                });
                res.redirect(
                  `${config.websiteUrl}/in/en/home/payment-successful.html?paymentSuccess=true&orderId=${
                    pData.order_id
                  }& amountPaid=${form.formConfig.paymentCurrency + "%20" + pData.amount}&regType=${
                    item.regNumber
                  }&riderName=${pData.billing_name}`
                );
              }
            })
            .catch(error => {
              if (form.formData.length - 1 == index) {
                res.redirect(`${config.websiteUrl}/in/en/home/payment-failed.html`);
              }
              console.log(error);
            });
        } else {
          if (form.formData.length - 1 == index) {
            res.redirect(`${config.websiteUrl}/in/en/home/payment-failed.html`);
          }
          console.log("Error Updating payment details");
        }
      });
    })
    .catch(error => {
      res.redirect(`${config.websiteUrl}/in/en/home/payment-failed.html`);
    });
}

function ccavRequestHandlerSingleForm(req, res, next) {
  var payObj = {
    merchant_id: "10292",
    order_id: Math.floor(1000000000 + Math.random() * 9000000000).toString(),
    currency: "INR",
    amount: "1.00",
    redirect_url: `${config.websiteUrl} /node/api / payment / ccavResponseHandlerSingleForm`,
    cancel_url: `${config.websiteUrl} /node/api / payment / ccavResponseHandlerSingleForm`,
    language: "EN",
    billing_name: req.body.fname + req.body.lname,
    billing_address: req.body.address,
    billing_city: req.body.city,
    billing_state: req.body.state,
    billing_zip: req.body.zipCode,
    billing_country: "India",
    billing_tel: req.body.phoneNo,
    billing_email: req.body.email,
    delivery_name: req.body.fname + req.body.lname,
    delivery_address: req.body.address,
    delivery_city: req.body.city,
    delivery_state: req.body.state,
    delivery_zip: req.body.zipCode,
    delivery_country: "India",
    delivery_tel: req.body.phoneNo,
    merchant_param1: req.body.formId,
    merchant_param2: req.body.email,
    merchant_param3: "additional + Info",
    merchant_param4: "additional + Info",
    merchant_param5: "additional + Info",
    promo_code: "",
    customer_identifier: ""
  };
  var body = "",
    workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
    accessCode = config.paymentGatewayAccessCode, //Put in the Access Code shared by CCAvenues.
    encRequest = "",
    formbody = "";
  body = `merchant_id = ${payObj.merchant_id}& order_id=${payObj.order_id}& currency=${payObj.currency}& amount=${
    payObj.amount
  }& redirect_url=${payObj.redirect_url}& cancel_url=${payObj.cancel_url}& language=${payObj.language}& billing_name=${
    payObj.billing_name
  }& billing_address=${payObj.billing_address}& billing_city=${payObj.billing_city}& billing_state=${
    payObj.billing_state
  }& billing_zip=${payObj.billing_zip}& billing_country=${payObj.billing_country}& billing_tel=${
    payObj.billing_tel
  }& billing_email=${payObj.billing_email}& delivery_name=${payObj.delivery_name}& delivery_address=${
    payObj.delivery_address
  }& delivery_city=${payObj.delivery_city}& delivery_state=${payObj.delivery_state}& delivery_zip=${
    payObj.delivery_zip
  }& delivery_country=${payObj.delivery_country}& delivery_tel=${payObj.delivery_tel}& merchant_param1=${
    payObj.merchant_param1
  }& merchant_param2=${payObj.merchant_param2}& merchant_param3=${payObj.merchant_param3}& merchant_param4=${
    payObj.merchant_param4
  }& merchant_param5=${payObj.merchant_param5}& promo_code=${payObj.promo_code}& customer_identifier=${
    payObj.customer_identifier
  } `;
  encRequest = encrypt(body, workingKey);
  formbody = `< form id = "nonseamless" method = "POST" name = "redirect" action = ${
    config.paymentGatewayURL
  }> <input type="hidden" id="encRequest" name="encRequest" value=${encRequest}><input type="hidden" name="access_code" id="access_code" value=${accessCode}><script language="javascript">document.redirect.submit();</script></form>`;
  res.writeHeader(200, { "Content-Type": "text/html" });
  res.write(formbody);
  res.end();
  return;
}

function ccavResponseHandlerSingleForm(req, res, next) {
  var ccavEncResponse = "",
    ccavResponse = "",
    workingKey = config.paymentGatewayWorkingKey, //Put in the 32-Bit key shared by CCAvenues.
    ccavPOST = "";
  var encryption = req.body.encResp;
  ccavResponse = decrypt(encryption, workingKey);
  var pData = qs.parse(ccavResponse);
  DynamicFormSchema.findOne({ formId: pData.merchant_param1 })
    .then(form => {
      form.formData.forEach((item, index) => {
        if (item.email == pData.merchant_param2) {
          item.paymentToken = pData.order_id;
          item.paymentSuccess = true;
          DynamicFormSchema.findOneAndUpdate({ formId: pData.merchant_param1 }, { $set: { formData: form.formData } })
            .then(savedForm => {
              if (form.formData.length - 1 == index) {
                res.redirect(
                  `${config.websiteUrl}/home/payment-successful.html?paymentSuccess=true&orderId=${
                    pData.order_id
                  }&amountPaid=INR1&regType=single&riderName=${emailData.riderName}`
                );
              }
            })
            .catch(error => {
              if (form.formData.length - 1 !== index) {
                res.redirect(`${config.websiteUrl}/home/payment-failed.html`);
              }
              console.log(error);
            });
        } else {
          if (form.formData.length - 1 == index) {
            res.redirect(`${config.websiteUrl}/home/payment-failed.html`);
          }
          console.log("Error Updating payment details");
        }
      });
    })
    .catch(error => {
      res.redirect(`${config.websiteUrl}/home/payment-failed.html`);
    });
}

function encrypt(plainText, workingKey) {
  var m = crypto.createHash("md5");
  m.update(workingKey);
  var key = m.digest("binary");
  var iv = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f";
  var keyBuffer = new Buffer(key, "binary");
  var cipher = crypto.createCipheriv("aes-128-cbc", keyBuffer, iv);
  var encoded = cipher.update(plainText, "utf8", "hex");
  encoded += cipher.final("hex");
  return encoded;
}

function decrypt(encText, workingKey) {
  var m = crypto.createHash("md5");
  m.update(workingKey);
  var key = m.digest("binary");
  var iv = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f";
  var keyBuffer = new Buffer(key, "binary");
  var decipher = crypto.createDecipheriv("aes-128-cbc", keyBuffer, iv);
  var decoded = decipher.update(encText, "hex", "utf8");
  decoded += decipher.final("utf8");
  return decoded;
}

export default {
  ccavRequestHandler,
  ccavResponseHandler,
  ccavRequestHandlerSingleForm,
  ccavResponseHandlerSingleForm,
  encrypt
};
