import express from 'express';
import bikeCtrl from '../controllers/bike';

const router = express.Router();

/** POST /api/bikes/register - create new Bike and return corresponding bike object and token */
router.route('/register')
  .post(bikeCtrl.register);

router.route('/addmotorcycle')
  .post(bikeCtrl.addMotorcycle);

router.route('/getMotorcycleModelCodes')
  .get(bikeCtrl.getMotorcycleModelCodes);

router.route('/getMotorcycleFamilyCodes')
  .get(bikeCtrl.getMotorcycleFamilyCodes);

router.route('/getMotorcycleList')
  .get(bikeCtrl.getMotorcycleList);
router.route('/getDetailMotorcycleList')
  .get(bikeCtrl.getMotorcycleListMobile);


router.route('/getMotorcycleIdCodes')
  .get(bikeCtrl.getMotorcycleIdCodes);

router.route('/getOnlineMotorcycleColorCodes')
	.post(bikeCtrl.getMotorcycleModelCodesForOnlineBooking)

router.route('/getExshowRoomPrice')
		.post(bikeCtrl.getExshowRoomPrice)

router.route('/getAccessoryList')
  .post(bikeCtrl.getAccessoryListForBike);

router.route('/makingTheOnlineBooking')
  .post(bikeCtrl.makingTheOnlineBooking);

router.route('/')
  /** GET /api/bikes - Get bike */
  .get(bikeCtrl.get)
  /** PUT /api/bikes - Update bike */
  .put(bikeCtrl.update)
  /** DELETE /api/bikes - Delete bike */
  .delete(bikeCtrl.remove);

export default router;
