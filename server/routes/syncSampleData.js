import express from 'express';
import ctrlSyncSampleData from '../controllers/syncSampleData';

const router = express.Router();

// POST /api/social/twitter
router.route('/city-states')
  .post(ctrlSyncSampleData.syncCityStates);

router.route('/dealers')
  .post(ctrlSyncSampleData.syncDealers);

export default router;
