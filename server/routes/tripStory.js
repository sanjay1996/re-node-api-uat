import passport from 'passport';
import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import tripStoryCtrl from '../controllers/tripStory';
import config from '../../config/env';


const router = express.Router();

router.route('/getthestory').post(tripStoryCtrl.getmystory);
/** POST /api/stories/create - create new Trip Story and return corresponding story object */

router.route('/all')
  .post(validate(paramValidation.getStories), tripStoryCtrl.getStories);


router.route('/getstory')
  /** POST /api/stories/ - Get story */
  .post(tripStoryCtrl.getStory)
  .get(tripStoryCtrl.getStory)
  /** PUT /api/stories/ - Update Story */
  .put(tripStoryCtrl.update)
  /** DELETE /api/stories/ - Delete Story */
  .delete(tripStoryCtrl.remove);

/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/

router.use((req, res, next) => {
  passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => { 
    if (userDtls) {
      req.user = userDtls;
      return next();
    } else {
      console.log('===============error=====================');
      console.log(error);
      console.log('===============error=====================');
      console.log('================info====================');
      console.log(info);
      console.log('===============info=====================');
      // const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
      return next(info);
    }
  })(req, res, next);
});

router.route('/create')
  .post(tripStoryCtrl.create);

  

export default router;
