import express from 'express';
import passport from 'passport';
import config from '../../config/env';
import APIError from '../helpers/APIError';
import httpStatus from 'http-status';
import logger from '../../config/winston'

import bookTestRideCtrl from '../controllers/book-test-ride';

const router = express.Router();

/** POST /api/comments/create - create new Trip Story and return corresponding story object */
router.route('/create')
    .post(bookTestRideCtrl.create);

/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/
router.use((req, res, next) => {
    passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => {
        //eslint-disable-line
        if (userDtls) {
            // console.log('================userDtls====================');
            // console.log(userDtls);
            // console.log('================userDtls====================');
            req.user = userDtls;
            return next();
        } else {
            console.log('===============error=====================');
            console.log(error);
            console.log('===============error=====================');
            console.log('================info====================');
            console.log(info);
            console.log('===============info=====================');
            // const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
            return next(info);
        }
    })(req, res, next);
});

export default router;
