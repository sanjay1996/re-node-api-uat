import express from 'express';
import dealersCtrl from '../controllers/dealers';

const router = express.Router();

/** POST /api/dealers/register - create new Dealer and return corresponding dealer object and token */
router.route('/register')
  .post(dealersCtrl.create);

router.route('/rideOuts')
  .get(dealersCtrl.rideOuts);

router.route('/')
  /** GET /api/dealers - Get dealers */
  .get(dealersCtrl.get)
  /** PUT /api/dealers - Update dealers */
  .put(dealersCtrl.update)
  /** DELETE /api/bikes - Delete dealers */
  .delete(dealersCtrl.remove);

router.route('/createdealerpage').get(dealersCtrl.createPage);
export default router;
