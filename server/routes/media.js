import express from 'express';
import mediaCtrl from '../controllers/media';

const router = express.Router();

router.route('/')
  .post(mediaCtrl.submit);

//node/api/media/verifyusermediarole
router.route("/verifyusermediarole").post(mediaCtrl.verifyingUserMediaRole);

//node/api/media/approvingmediarole
router
  .route("/approvingmediarole")
  .post(mediaCtrl.verifyingUserFromUserDashboard);

export default router;


