import express from 'express';
import ctrl from '../controllers/recaptcha';

const router = express.Router();

// POST /api/recaptcha/store
router.route('/store')
    .post(ctrl.recaptchastore)
    .get(ctrl.recaptchastore);

router.route('/verify')
    .post(ctrl.recaptchaverify)
    .get(ctrl.recaptchaverify);

export default router;