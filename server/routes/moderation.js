import express from 'express';
import moderationCtrl from '../controllers/moderation';

const router = express.Router();

router.route('/twitterdata')
  /** GET /api/moderation/twitterdata - Get twitterdata */
  .get(moderationCtrl.getTwitterData)

router.route('/moderatetwitterdata')
  /** GET /api/moderation/moderatetwitterdata - Moderate twitterdata */
  .post(moderationCtrl.moderateTwitterData)


export default router;
