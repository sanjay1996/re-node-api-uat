import express from 'express';
import validate from 'express-validation';
import ownersManual from '../controllers/owners-manual';

const router = express.Router();

router.route('/create')
  .post(validate(ownersManual.create), ownersManual.create);

export default router;