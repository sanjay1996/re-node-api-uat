import express from 'express';
import { bookTestRide } from '../service/dmsApi';

const router = express.Router();

/** POST /api/dealers/register - create new Dealer and return corresponding dealer object and token */
router.route('/book-test-ride')
  .post(bookTestRide);
export default router;
