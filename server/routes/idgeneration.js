import express from 'express';
import idgenerationcontroller from '../controllers/idgeneration'

const router = express.Router();

router.route('/').post(idgenerationcontroller.generateId);

export default router;