import express from 'express';
import forumCtrl from '../controllers/forums';
import forumCategoryCtrl from "../controllers/forumCategory";
import passport from 'passport';
import config from '../../config/env';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';

const router = express.Router();

router.route('/forum-detail')
  .post(forumCtrl.getFourmDetail);

router.route('/increment-topic-view')
  .post(forumCtrl.incrementTopicViews);

router.route('/all')
  .post(forumCtrl.getForumPosts);

router.route('/category-details')
  .post(forumCategoryCtrl.getCategoryDetials);


router.route('/subscribe')
  .post(forumCategoryCtrl.subscribeToForumCategory);

/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/

router.use((req, res, next) => {
  passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => { //eslint-disable-line
     if (userDtls) {
      req.user = userDtls;
      return next();
    } else {
      console.log('================info====================');
      console.log(info);
      console.log('===============info=====================');
      // const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
      return next(info);
    }
  })(req, res, next);
});

router.route('/create-forum-post')
  .post(validate(paramValidation.createTopic),forumCtrl.create);


router.route('/create-category')
  .post(forumCategoryCtrl.create);


export default router;
