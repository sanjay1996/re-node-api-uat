import express from 'express';
import newsCtrl from '../controllers/news';

const router = express.Router();

/** PUT /node/api/ */
router.route('/')
    .put(newsCtrl.update);

/** POST /node/api/ */
router.route('/create')
    .post(newsCtrl.create);

/** POST /node/api/stories/ - Get story */
router.route('/getNews')
    .post(newsCtrl.getNews)

export default router;
