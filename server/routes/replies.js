import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import passport from 'passport';
import config from '../../config/env';
import repliesCtrl from '../controllers/replies';

const router = express.Router();

/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/

router.use((req, res, next) => {
    passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => { //eslint-disable-line
        //eslint-disable-line
        if (userDtls) {
            req.user = userDtls;
            return next();
        } else {
            console.log('================info====================');
            console.log(info);
            console.log('===============info=====================');
            // const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
            return next(info);
        }
    })(req, res, next);
});
router.route('/create')
    .post(validate(paramValidation.reviews), repliesCtrl.createReplies);

export default router;
