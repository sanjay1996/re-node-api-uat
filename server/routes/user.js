import express from 'express';
import validate from 'express-validation';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import passport from 'passport';
import config from '../../config/env';
import paramValidation from '../../config/param-validation';
import userCtrl from '../controllers/user';
import userVerify from '../controllers/verify';

const router = express.Router();

router.route('/')
  /** GET /node/api/users - Get user */
  .get(validate(paramValidation.getUserDetails), userCtrl.getUserDetails);

router.route('/user-details')
  /** GET /node/api/users - Get user */
  .get(validate(paramValidation.getUserDetails), userCtrl.getUserDetailsMobile);

/** POST /api/users/register - create new user and return corresponding user object and token */
router.route('/register')
  .post(validate(paramValidation.createUser), userCtrl.create);

router.route('/forgot')
  .post(validate(paramValidation.forgotPassword), userCtrl.forgot);
  
router.route('/send-otp')
  .post(userCtrl.sendOtp);

//api/users/verifyOtp
router.route('/verifyOtp')
  .post(validate(paramValidation.verifyOtp), userVerify.verifyOtp);

//api/users/resetPassword using mobile no
router.route('/reset-password-phone')
  .post(validate(paramValidation.resetPassword), userCtrl.resetPassword);

//api/users/resetPassword using mobile no
router.route('/reset-password')
  .post(validate(paramValidation.resetPassword), userCtrl.resetPassword);

router.route('/reviewForUser')
  .get(validate(paramValidation.reviewForUser), userCtrl.reviewForUser);

/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/

router.use((req, res, next) => {
  passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => {
    if (userDtls) {
      req.user = userDtls;
      return next();
    } else {
var returnObj={
    "success": false,
    "message": "Invalid Token",
    "stack": {} 
}
res.status(401).send(returnObj)
      console.log('================info====================');
      console.log(info);
      console.log('===============info=====================');
      //return next(info);
    }
  })(req, res, next);
});


router.route('/updateProfile')
  .post(validate(paramValidation.updateProfile), userCtrl.update);

//api/users/deleteprofileimage
router.route('/deleteprofileimage')
  .post(validate(paramValidation.deleteprofileimage), userCtrl.deleteProfileImage);

//api/users/userInterests
router.route('/userInterests')
  .post(validate(paramValidation.userInterest), userCtrl.userInterests);

//api/users/updateprofileimage
router.route('/updateprofileimage')
  .post(validate(paramValidation.updateprofileimage), userCtrl.updateProfileImage);

//api/users/updatecoverimage
router.route('/updatecoverimage')
  .post(validate(paramValidation.updatecoverimage), userCtrl.updateCoverImage);

//api/users/deletecoverimage
router.route('/deletecover')
  .post(validate(paramValidation.deletecoverimage), userCtrl.deleteCoverImage);
/*node/api/users/suggestedTripStories*/

router.route('/suggestedTripStories')
  .get(userCtrl.suggestedTripStories);


export default router;
