import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import subscriptionCtrl from '../controllers/subscribe';

const router = express.Router();

/* POST /node/api/susbcribe*/
router.route('/')
  .post(validate(paramValidation.subscribe), subscriptionCtrl.subscribed)

export default router;