import express from 'express';
import cityStatesCtrl from '../controllers/cityStates';

const router = express.Router();


router.route('/getLocaleMaster')
    .get(cityStatesCtrl.getAllCountryStateCity);

router.route('/getcity')
    .post(cityStatesCtrl.getcity);

router.route('/getstate')
    .post(cityStatesCtrl.getState);

router.route('/getdealers')
    .post(cityStatesCtrl.getDealers);

router.route('/fetchData')
    .post(cityStatesCtrl.fetchCityStates);

export default router;
