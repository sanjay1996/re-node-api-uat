import express from "express";
import payment from "../controllers/payment";
import staticPaymentCtrl from "../controllers/paymentStaticForms";
import staticFormCtrl from "../controllers/staticform";
import MotoHimalaya from "../models/motohimalaya";
import multer from "multer";

const router = express.Router();

router.route("/ccavRequestHandler").post(payment.ccavRequestHandler);

router.route("/ccavResponseHandler").post(payment.ccavResponseHandler);

router.route("/ccavRequestHandlerSingleForm").post(payment.ccavRequestHandlerSingleForm);

router.route("/ccavResponseHandlerSingleForm").post(payment.ccavResponseHandlerSingleForm);

/**
 * static Payment forms routes
 */

router.route("/payEscapadeWayanad").post(staticPaymentCtrl.escapadeWayanadRequestPayment);
router.route("/responseHandlerEscapadeWayanad").post(staticPaymentCtrl.escapadeWayanadResponseHandler);

router.route("/pay-reunion-nepal").post(staticPaymentCtrl.reUnionNepalRequestPayment);
router.route("/response-handler-reunion-nepal").post(staticPaymentCtrl.reUnionNepalResponseHandeler);

router.route("/pay-tour-of-rajasthan").post(staticPaymentCtrl.tourOfRajasthanRequestPayment);
router.route("/response-handler-tour-of-rajasthan").post(staticPaymentCtrl.tourOfRajasthanResponseHandeler);

/**
 * static form controllers
 */

//Route to check whether registration is open
router.route("/validatingDynamicForms").post(staticFormCtrl.validatingDynamicForms);

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./node/assets/Forms");
  },
  filename: function(req, file, cb) {
    var fileName = Date.now() + file.originalname.replace(/\s/g, "");
    cb(null, fileName);
    var filePath = "./node/assets/Forms" + fileName;
  }
});

const uploadObj = multer({ storage: storage });

router.route("/pay-motohimalaya").post(uploadObj.array("my_files", 7), staticFormCtrl.motohimalayaRequestPayment);
router.route("/response-handler-motohimalaya").post(staticFormCtrl.motohimalayaResponseHandler);

router.route("/getAllMotohimalaya").get(staticFormCtrl.getAllSuccessfulMotohimalaya);
router.route("/getAllReunionWest").get(staticFormCtrl.getAllSuccessfulReunionWest);

router.route("/upload-motohimalaya").post(uploadObj.array("my_files", 7), staticFormCtrl.motohimalayaUpload);

router.route("/pay-reunionwest").post(staticFormCtrl.reunionwestRequestPayment);

router.route("/response-handler-reunionwest").post(staticFormCtrl.reunionwestResponseHandler);


router.route("/pay-tourindonesia").post(staticFormCtrl.tourindonesiaRequestPayment);

router.route("/response-handler-tourindonesia").post(staticFormCtrl.tourindonesiaResponseHandler);

export default router;


