import express from 'express';
import storectrl from '../controllers/storelocator';

const router = express.Router();

router.route('/')
    .post(storectrl.locatestores);

export default router;