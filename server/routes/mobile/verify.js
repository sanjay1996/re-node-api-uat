import express from "express";
import verifyCtrl from "../../controllers/verify";

const router = express.Router();

router
  .route("/email")
  .post(verifyCtrl.verifyEmail)
  .put(verifyCtrl.verifyEmail)
  .get(verifyCtrl.verifyEmail);

router
  .route("/mobile")
  .get(verifyCtrl.verifyMobile)
  .post(verifyCtrl.verifyMobile);

export default router;
