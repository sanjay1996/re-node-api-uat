import express from 'express';
import commentsCtrl from '../controllers/comments';
import passport from 'passport';
import config from '../../config/env';

const router = express.Router();

router.route('/getComments')
    .post(commentsCtrl.getComments);

/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/

router.use((req, res, next) => {
    passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => { //eslint-disable-line
        if (userDtls) {
            req.user = userDtls;
            return next();
        } else {
            console.log('================info====================');
            console.log(info);
            console.log('===============info=====================');
            // const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
            return next(info);
        }
    })(req, res, next);
});

/** POST /api/comments/create - create new Trip Story and return corresponding story object */
router.route('/create')
    .post(commentsCtrl.createComment);

export default router;
