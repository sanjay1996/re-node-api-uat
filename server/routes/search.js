import express from 'express';
import solrCtrl from '../controllers/solr';

const router = express.Router();

router.route('/')
    .get(solrCtrl.searchSolr)
    .post(solrCtrl.searchSolrCategory);

router.route('/searchride')
    .post(solrCtrl.searchRide);

router.route('/forumTopicSearchOnEnter')
    .post(solrCtrl.forumTopicSearchOnEnter);

router.route('/testSms')
    .post(solrCtrl.sendSms);

router.route('/tripStorySearchCount')
    .post(solrCtrl.tripStorySearchCount);

router.route('/tripStorySearch')
    .post(solrCtrl.tripStorySearch);

router.route('/searchTripOnClick')
    .post(solrCtrl.tripStorySearchOnClick);

router.route('/forumTopicSearchOnClick')
    .post(solrCtrl.forumTopicSearchOnClick);

router.route('/forumTopicSearch')
    .post(solrCtrl.forumTopicSearch);

router.route('/newsSearch')
    .post(solrCtrl.NewsSearch);

router.route('/bannerDataSearch')
    .post(solrCtrl.bannerDataSearch);

router.route('/newsEvent')
    .post(solrCtrl.newsEvent);

router.route('/careerSearch')
    .post(solrCtrl.CareerSearch);

router.route('/pressRelease')
    .post(solrCtrl.PressReleaseSearch);

router.route('/GMAProductSearch')
    .post(solrCtrl.GMAProductSearch);

export default router;
