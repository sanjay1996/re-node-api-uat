import express from 'express';
import gmaForm from '../controllers/gmaForm';
import paramValidation from '../../config/param-validation';
import validate from 'express-validation';

const router = express.Router();

router.route('/create')
  .post(validate(paramValidation.gmaForm),gmaForm.create);

export default router;
