import express from 'express';
import uploadCtrl from '../controllers/uploadAsset';


const router = express.Router();
router.route('/upload')
	.post(uploadCtrl.upload);

router.route('/fetch')
	.post(uploadCtrl.fetch);

export default router;
