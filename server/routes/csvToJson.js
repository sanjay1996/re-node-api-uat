import express from "express";
import csvToJsonCtrl from "../controllers/csvToJson";

//import SchedulerCtrl from "../controllers/allSchedulers";
const router = express.Router();

router.route("/connect").get(csvToJsonCtrl.connectSftp);

// router.route("/bikecsvToJson").get(csvToJsonCtrl.bikeCsvToJson);


// router.route("/dealerxlstojson").get(csvToJsonCtrl.dealerDataToDB); //to Insert Dealer into Mongo 

// router.route("/dealerPageCreation").get(csvToJsonCtrl.dealerPageCreation); // to Create Dealer Detail Pages On Author 

// router.route("/test").get(csvToJsonCtrl.cityStatesData);
// router.route("/test1").get(csvToJsonCtrl.testMail);

export default router;
