import express from 'express';
import validate from 'express-validation';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import passport from 'passport';
import config from '../../config/env';
import paramValidation from '../../config/param-validation';
import rideCtrl from '../controllers/ride';
import multer from 'multer';

const router = express.Router();

router.route('/filter-rides')
  .post(rideCtrl.filterRides);

router.route('/riders-joined')
  .get(rideCtrl.ridersJoined);

router.route('/all')
  .post(validate(paramValidation.getRides), rideCtrl.getRides);

router.route('/getridesaroundme')
  .post(rideCtrl.getRidesAroundMe);

router.route('/')
  /** POST /api/rides/ - Get ride */
  .post(validate(paramValidation.getRide), rideCtrl.getRide)
  /** PUT /api/rides/ - Update ride */
  .put(rideCtrl.update)
  /** DELETE /api/rides/ - Delete ride */
  .delete(rideCtrl.remove)
  .get(rideCtrl.getRide);
/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/

router.use((req, res, next) => {
  passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => { 
    if (userDtls) {
      req.user = userDtls;
      return next();
    } else {
      var resObj = {};
      resObj.success= false;
      resObj.message= "unauthorized";
      res.status(401).send(resObj);
    }
  })(req, res, next);
});

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './node/assets/Rides');
  },
  filename: function (req, file, cb) {
    var imageName = Date.now()+ file.originalname.replace(/\s/g, '');
    cb(null, imageName);
    var imagePath = './node/assets/Rides' + imageName;
  }
});

const uploadObj = multer({ storage: storage })
/** POST /api/rides/create - create new Ride and return corresponding ride object */
router.route('/create')
  .post(uploadObj.array('rideImages', 10), validate(paramValidation.createRide),rideCtrl.create);

router.route('/join')
  .post(validate(paramValidation.joinRide),rideCtrl.joinRide);



export default router;
