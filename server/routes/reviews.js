import express from 'express';
import reviewsCtrl from '../controllers/reviews';
import passport from 'passport';
import config from '../../config/env';

const router = express.Router();


// post  /api/reviews/reviewsummary
router.route('/reviewsummary')
  .get(reviewsCtrl.getReviews)

//post /api/reviews/reviewdetails
router.route('/reviewdetails').
  post(reviewsCtrl.fetch)


/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/

router.use((req, res, next) => {
  passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => { //eslint-disable-line
    //eslint-disable-line
    if (userDtls) {
      req.user = userDtls;
      return next();
    } else {
      console.log('================info====================');
      console.log(info);
      console.log('===============info=====================');
      // const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
      return next(info);
    }
  })(req, res, next);
});


// POST /api/reviews/create
router.route('/create')
  .post(reviewsCtrl.create)

export default router;


