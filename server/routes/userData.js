import express from "express";
import passport from "passport";
import config from "../../config/env";
import logger from "../../config/winston";

const router = express.Router();

router.use((req, res, next) => {
  passport.authenticate("jwt", config.passportOptions, (error, userDtls, info) => {
    //eslint-disable-line
    if (userDtls) {
      req.user = userDtls;
      return next();
    } else {
      var resObj = {};
      resObj.success = false;
      resObj.message = "unauthorized";
      res.status(401).send(resObj);
      // logger.log({
      //   level: "info",
      //   message: `This is info in middelware`,
      //   fileName: "userData.js",
      //   functionName: "inside middelware",
      //   data: info
      // });
    }
    req.user = userDtls;
    //   return next();
  })(req, res, next);
});

router.post("/userData", (req, res, next) => {
  var resObj = {};
  resObj.success = true;
  resObj.data = req.user;
  res.send(resObj);
});

export default router;
