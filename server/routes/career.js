import express from 'express';
import careerCtrl from '../controllers/career';

const router = express.Router();

router.route('/create')
  .post(careerCtrl.createCareer);

export default router;

