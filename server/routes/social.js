
import express from 'express';
import ctrlSocial from '../controllers/social';

const router = express.Router();

router.route('/twitter')
  .post(ctrlSocial.twitterdata)
  .get(ctrlSocial.twitterdata);

router.route('/dealerreviews')
  .post(ctrlSocial.dealerReviews);

router.route('/reviews')
  .post(ctrlSocial.fetch);

router.route('/reviews')
  .get(ctrlSocial.fetch);

router.route('/googleplus')
  .post(ctrlSocial.googleplusdata);

router.route('/googleplus')
  .get(ctrlSocial.googleplusdata);

router.route('/weather')
  .get(ctrlSocial.getWeather);

export default router;
