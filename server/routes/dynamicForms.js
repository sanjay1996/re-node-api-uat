import express from 'express';
import dynamicFormController from "../controllers/dynamicForm";
//import oldDynamicFormControllerfrom from "../controllers/oldDynamicForm.js";
import multer from "multer";

const router = express.Router();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './node/assets/DynamiFormAssets');
    },
    filename: function (req, file, cb) {
        var imageName = Date.now() + file.originalname;
        cb(null, imageName);
        var imagePath = './node/assets/DynamiFormAssets' + imageName;
    }
});

const uploadObj = multer({ storage: storage });

//router.route('/v1/create')
  //  .post(oldDynamicFormControllerfrom.createDynamicForm);
router.route('/create')
    .post(dynamicFormController.createDynamicForm);

//router.route('/v1/submit')
  //  .post(uploadObj.array('formAssets', 15), oldDynamicFormControllerfrom.submitDataToForm);
router.route('/submit')
    .post(uploadObj.array('formAssets', 15), dynamicFormController.newSubmitDataToForm);

//router.route('/v1/verify-payment')
  //  .post(oldDynamicFormControllerfrom.verifyPayment);
router.route('/verify-payment')
    .post(dynamicFormController.verifyPayment);

router.route('validatingDynamicForms').post(dynamicFormController.validatingDynamicForms)

export default router;
