import moment from "moment";
import config from "../../config/env";
import fs from "fs";
import { Client } from "ssh2";
import MotoHimalaya from "../models/motohimalaya";
import Reunionwest from "../models/reunionwest";
import json2xls from "json2xls";
import { emailWithAttachmentApi } from "../service/emailApi";
import schedule from "node-schedule";

function getmotodata(req, res) {
  console.log("====================================");
  console.log("Moto Himalaya Schedular");
  console.log("====================================");
  schedule.scheduleJob({ hour: 17, minute: 5 }, function() {
    MotoHimalaya.find({ "paymentObject.order_status": "Success" }).then(json => {
      if (json) {
        var finalArr = [];
        json.map(val => {
          var obj = {
            expectFromRide: val.expectFromRide,
            clubs: val.clubs,
            distanceTravelled: val.distanceTravelled,
            instagramHandle: val.instagramHandle,
            twitterHandle: val.twitterHandle,
            facebook: val.facebook,
            emergencyContactEmail: val.emergencyContactEmail,
            emergencyContactMobileNumber: val.emergencyContactMobileNumber,
            emergencyContactFirstName: val.emergencyContactFirstName,
            dob: val.dob,
            gender: val.gender,
            countryName: val.countryName,
            mobileNumber: val.mobileNumber,
            email: val.email,
            lname: val.lname,
            fname: val.fname,
            regType: val.regType,
            order_id: val.order_id,
            formName: val.formName,
            language: val.locality.language,
            country: val.locality.country,
            payment_mode: val.payment_mode,
            order_status: val.order_status,
            bank_ref_no: val.bank_ref_no,
            tracking_id: val.tracking_id,
            trans_date: val.paymentObject.trans_date
          };
          val.my_files.map((file, i) => {
            obj[`filePath${i + 1}`] = `${config.websiteUrl}/${file.path}`;
          });
          finalArr.push(obj);
        });
        var xls = json2xls(finalArr);
        fs.writeFileSync("data.xlsx", xls, "binary");
        var attachments = [{ filename: "data.xlsx", path: "data.xlsx", contentType: "application/pdf" }];
        emailWithAttachmentApi(
          "All Joined Riders Motohimalaya",
          "Please find attachment below.",
          "",
          //"sanjay.sharma@techchefz.com",
          "gautham@22feettribalww.com",
          "noreply@royalenfield.com",
          attachments,
          "error sending mail",
          "Mail sent successfully!!!!"
        );
      }
    });
  });
}

function getReunionWest() {
  schedule.scheduleJob({ hour: 17, minute: 5 }, function() {
    Reunionwest.find({ "paymentObject.order_status": "Success" }).then(json => {
      var finalArr = [];
      json.map(val => {
        var obj = {
          formName: val.formName,
          orderId: val.orderId,
          regType: val.regType,
          fname: val.fname,
          lname: val.lname,
          email: val.email,
          mobileNumber: val.mobileNumber,
          address: val.address,
          country: val.country,
          state: val.state,
          city: val.city,
          postalCode: val.postalCode,
          gender: val.gender,
          tShirtSize: val.tShirtSize,
          ridingWith: val.ridingWith,
          motorcycleOwned: val.motorcycleOwned,
          manufacturedYear: val.manufacturedYear,
          registrationNumber: val.registrationNumber,
          partfname: val.partfname,
          partlname: val.partlname,
          partemail: val.partemail,
          partmobile: val.partmobile,
          partOrderId: val.partOrderId,
          partgender: val.partgender,
          partShirtSize: val.partShirtSize,
          partdob: val.partdob,
          language: val.locality.language,
          country: val.locality.country,
          trans_date: val.paymentObject.trans_date,
          tracking_id: val.tracking_id,
          bank_ref_no: val.bank_ref_no,
          order_status: val.order_status,
          eventPayment: val.eventPayment,
          payment_mode: val.payment_mode
        };
        finalArr.push(obj);
      });
      var xls = json2xls(finalArr);
      fs.writeFileSync("reuniondata.xlsx", xls, "binary");
      var attachments = [{ filename: "reuniondata.xlsx", path: "reuniondata.xlsx", contentType: "application/pdf" }];
      emailWithAttachmentApi(
        "All Joined Riders Reunion West",
        "Please find attachment below.",
        "",
        //"sanjay.sharma@techchefz.com",
        "gautham@22feettribalww.com",
        "noreply@royalenfield.com",
        attachments,
        "error sending mail",
        "Mail sent successfully!!!!"
      );
    });
  });
}

function RideoutsCsv() {
  const time = 1000 * 60 * 30;
  const sftpRedFilePath = "DMS-Web3/Production/RideEnrollmentList";
  const archiveSftpFilePath = "";
  console.log("====================================");
  console.log(`Schedular will upload the file after ${time / (60 * 1000)} mins.`);
  console.log("====================================");
  setInterval(function() {
    var finalData = [];
    var conn = new Client();
    conn
      .on("ready", () => {
        conn.sftp(function(err, sftp) {
          if (err) {
            conn.end();
            throw err;
          } else {
            fs.readdir("archivedFiles/RidesJoined", (err, files) => {
              if (err) {
                conn.end();
                throw err;
              } else if (files.length == 0) {
                conn.end();
                console.log("====================================");
                console.log("Ride out csv files not found to upload");
                console.log("====================================");
              } else {
                const localFileName = files[0];
                const finalFileName = `VW_WEBSITE_RIDEENROLLMENTDETAIL_${moment().format("DDMMYYYY_HHMMSS")}.csv`;
                fs.readFile(`archivedFiles/RidesJoined/${localFileName}`, (err, readedData) => {
                  if (err) {
                    conn.end();
                    throw err;
                  } else {
                    sftp.readdir(`${sftpRedFilePath}`, (err, sftpFiles) => {
                      if (err) {
                        conn.end();
                        throw err;
                      } else {
                        console.log("====================================");
                        console.log(sftpFiles.length);
                        console.log(sftpFiles);
                        console.log("====================================");
                        if (sftpFiles.length == 1 && sftpFiles[0].filename == "Processed") {
                          //upload file directlly
                          sftp.writeFile(
                            `${sftpRedFilePath}/${finalFileName}`,
                            readedData.toString().trim(),
                            (err, result) => {
                              if (err) {
                                conn.end();
                                throw err;
                              } else {
                                fs.unlink(`archivedFiles/RidesJoined/${localFileName}`, err => {
                                  if (err) {
                                    conn.end();
                                    throw err;
                                  } else {
                                    console.log("=============File Unlinked=======================");
                                    console.log("SFTP returned with 0 file soo file is uploaded directly");
                                    console.log("====================================");
                                  }
                                });
                                conn.end();
                              }
                            }
                          );
                        } else {
                          for (var i = 0; i < sftpFiles.length; i++) {
                            const sftpfilename = sftpFiles[i].filename.substring(0, 40);
                            const localfile = localFileName.substring(0, 40);
                            if (sftpfilename === localfile) {
                              console.log("====================================");
                              console.log(sftpfilename);
                              console.log(localfile);
                              console.log("====================================");
                              sftp.readFile(`${sftpRedFilePath}/${sftpFiles[i].filename}`, (err, sftpReadedData) => {
                                if (err) {
                                  conn.end();
                                  throw err;
                                } else {
                                  finalData.push(sftpReadedData.toString());
                                  const modData = readedData.toString();
                                  if (
                                    modData.includes(
                                      `ContactName|CustomerContactNo|EmailID|RideID|Remarks|RideCategory|RideSubCategory|Gender|Size`
                                    )
                                  ) {
                                    var dumpData = modData.replace(
                                      "ContactName|CustomerContactNo|EmailID|RideID|Remarks|RideCategory|RideSubCategory|Gender|Size",
                                      ""
                                    );
                                    var finalDump = `${finalData[0].trim()}\r\n` + dumpData.trim();
                                    finalData = [];
                                    finalData.push(finalDump);
                                    sftp.writeFile(`${sftpRedFilePath}/${finalFileName}`, finalData, (err, result) => {
                                      if (err) {
                                        conn.end();
                                        throw err;
                                      } else {
                                        sftp.unlink(`${sftpRedFilePath}/${sftpFiles[i].filename}`, err => {
                                          if (err) {
                                            conn.end();
                                            throw err;
                                          } else {
                                            conn.end();
                                            fs.unlink(`archivedFiles/RidesJoined/${localFileName}`, err => {
                                              if (err) {
                                                conn.end();
                                                throw err;
                                              } else {
                                                console.log("=============File Unlinked=======================");
                                                console.log("Local & Server Files Unlinked Successfully");
                                                console.log("====================================");
                                              }
                                            });
                                          }
                                        });
                                      }
                                    });
                                  } else {
                                    conn.end();
                                    console.log("====================================");
                                    console.log("Files matched but headders are different no data inserted/uploaded");
                                    console.log("====================================");
                                  }
                                }
                              });
                              break;
                            } else {
                              if (sftpFiles.length == i + 1) {
                                sftp.writeFile(
                                  `${sftpRedFilePath}/${finalFileName}`,
                                  readedData.toString().trim(),
                                  (err, result) => {
                                    if (err) {
                                      conn.end();
                                      throw err;
                                    } else {
                                      fs.unlink(`archivedFiles/RidesJoined/${localFileName}`, err => {
                                        if (err) {
                                          conn.end();
                                          throw err;
                                        } else {
                                          console.log("=============File Unlinked=======================");
                                          console.log("Files Not Matched with SFTP files And Local CSV file unlinked.");
                                          console.log("====================================");
                                        }
                                      });
                                      conn.end();
                                    }
                                  }
                                );
                              }
                            }
                          }
                        }
                      }
                    });
                  }
                });
              }
            });
          }
        });
      })
      .connect(config.sftpConnectionCredentials);
  }, time);
}

function TestRides() {
  var flag = false;
  console.log("Test Ride Schedular is up and runnig !!! :))");
  setInterval(() => {
    var finalData = [];
    var conn = new Client();
    console.log("===flag initial=================================");
    console.log(flag);
    console.log("================================================");
    conn.end();

    conn
      .on("ready", function() {
        conn.sftp(function(err, sftp) {
          if (err) throw (err, conn.end());

          fs.readdir("archivedFiles/TestRide", (err, files) => {
            if (err) {
              console.log("==============err======================");
              console.log(err);
              console.log("=============err=======================");
            } else {
              if (files.length !== 0) {
                const fileName = files[0];
                const finalFileName = `Test_Ride_Requests_${moment().format("YYYYMMDD")}.csv`;

                fs.readFile(`archivedFiles/TestRide/${fileName}`, (err, newData) => {
                  if (err) {
                    console.log("==========err==========================");
                    console.log(err);
                    console.log("==========err==========================");
                  } else {
                    sftp.readdir(`DMS-Web3/Testing/`, (err, files) => {
                      if (err) {
                        console.log("===============err in sftp read dir=====================");
                        console.log(err);
                        console.log("===============err in sftp read dir=====================");
                      } else {
                        files.forEach((file, index) => {
                          // awaitt()
                          if (file.filename == finalFileName) {
                            console.log("==========inside for loop==========================");
                            console.log("Inside if loop ");
                            console.log(file.filename);
                            console.log("======================if wala==============");
                            sftp.readFile(`DMS-Web3/Testing/outputcsv_tcz/TestRides/${fileName}`, (err, readedData) => {
                              finalData.push(readedData.toString());
                              const modData = newData.toString();
                              console.log("====================================");
                              console.log("INside tha read file");
                              console.log("====================================");
                              if (
                                modData.includes(
                                  "testrideId|country|state|city|name|email|strphone|address|reusedbefore|ownRe|bikemodel|year|modelId|modelname|dealername|dealerCode|createdon|source|Web Page Source"
                                )
                              ) {
                                var dumpData = modData.replace(
                                  "testrideId|country|state|city|name|email|strphone|address|reusedbefore|ownRe|bikemodel|year|modelId|modelname|dealername|dealerCode|createdon|source|Web Page Source",
                                  ""
                                );
                                console.log("====================================");
                                console.log(finalData[0]);
                                console.log("====================================");
                                var finalDump = `${finalData[0]}\r\n` + dumpData.trim();
                                console.log("===========finalDump to be inserted=========================");
                                console.log(finalDump);
                                console.log("===========finalDump to be inserted=========================");
                                finalData = [];
                                finalData.push(finalDump);
                                console.log("==============read file Data======================");
                                console.log(finalData);
                                console.log("====================================");
                              }

                              if (err) {
                                console.log("=================err on reading files===================");
                                console.log(err);
                                console.log("=================err on reading files===================");
                              }
                              sftp.writeFile(
                                `DMS-Web3/Testing/outputcsv_tcz/TestRides/${finalFileName}`,
                                finalData,
                                (err, result) => {
                                  if (err) {
                                    console.log("=============err=======================");
                                    console.log(err);
                                    console.log("====================================");
                                    conn.end();
                                  } else {
                                    sftp.unlink(`DMS-Web3/Testing/outputcsv_tcz/TestRides/${file.filename}`, err => {
                                      if (err) {
                                        throw err;
                                      } else {
                                        console.log("Uploaded Successfully :) :)");
                                        conn.end();
                                        fs.unlink(`archivedFiles/TestRide/${fileName}`, err => {
                                          if (err) {
                                            console.log("===unable to unlink file=================================");
                                            console.log(err);
                                            console.log("====================================");
                                          } else {
                                            console.log("=============File Unlinked=======================");
                                            console.log("File Unlinked Successfully");
                                            console.log("====================================");
                                            flag = false;
                                          }
                                        });
                                      }
                                    });
                                  }
                                }
                              );
                            });
                          }
                          if (files.length - 1 == index) {
                            flag = true;
                          }
                        });
                      }
                    });

                    console.log("===flag=================================");
                    console.log(flag);
                    console.log("====================================");
                    if (flag == true) {
                      flag = false;

                      console.log("====================================");
                      // console.log(files.length - 1);
                      // console.log(index);
                      console.log("====================================");
                      sftp.writeFile(
                        `DMS-Web3/Testing/outputcsv_tcz/TestRides/${finalFileName}`,
                        newData,
                        (err, result) => {
                          if (err) {
                            console.log("=============err=======================");
                            console.log(err);
                            console.log("====================================");
                            conn.end();
                          } else {
                            console.log("Uploaded2 Successfully :) :)");
                            conn.end();
                            fs.unlink(`archivedFiles/TestRide/${fileName}`, err => {
                              if (err) {
                                console.log("===unable to unlink file=================================");
                                console.log(err);
                                console.log("====================================");
                              } else {
                                console.log("=============File Unlinked=======================");
                                console.log("File Unlinked2 Successfully");
                                console.log("====================================");
                              }
                            });
                          }
                        }
                      );
                    }
                  }
                });
              } else {
                console.log("====================================");
                console.log("No TestRide Data found to upload");
                console.log("====================================");
              }
            }
          });
        });
      })
      .connect({
        host: "ftpL.royalenfield.com",
        port: 22,
        username: "interface",
        password: "Interface@re",
        privateKey: fs.readFileSync("config/id_rsa.ppk"),
        passphrase: "Interface@re",
        readyTimeout: 99999
      });
    conn.on("err", error => {
      console.log("================error====================");
      console.log(error);
      console.log("====================================");
    });
  }, 1000 * 60 * 1);
}

export default {
  RideoutsCsv,
  TestRides,
  getmotodata,
  getReunionWest
};
