import { Client } from "ssh2";
import fs from "fs";
import csv from "csvtojson";
import BikeCsvSchema from "../models/bikeCsv";
import isNumber from 'is-number';
import asyncc from "asyncawait/async";
import awaitt from "asyncawait/await";
import config from "../../config/env";
import schedule from "node-schedule";


export default function motoSchedular() {
    schedule.scheduleJob({ hour: 1, minute: 0, dayOfWeek: 7 }, function () {
        motorcycleSchedular();
    });
}

var arr = [];

function motorcycleSchedular() {
    arr = [];
    var resObj = {};
    const connection = new Client();
    var fileName;
    connection.on('ready', function () {
        console.log('Client :: ready');
        connection.sftp(function (err, sftp) {
            if (err) throw err;
            sftp.readdir('DMS-Web3/Testing/Model', function (err, list) {
                if (err) throw err;
                console.log(list);
                fileName = list[1].filename;
                console.log(fileName);
                sftp.readFile(`DMS-Web3/Testing/Model/${fileName}`, (err, readedModelData) => {
                    var bufferToString = readedModelData.toString('utf8');
                    fs.writeFile(`archivedFiles/Bikes/${fileName}`, bufferToString, (err, status) => {
                        csv({ delimiter: "|" }).fromFile(`archivedFiles/Bikes/${fileName}`).then((convertedDataArr) => {
                            var foo = asyncc(() => {
                                for (var convertedData of convertedDataArr) {
                                    awaitt(pushData(convertedData))
                                }
                                awaitt(pushingData(arr, resObj))
                            });
                            foo();
                        }).catch((e) => {
                            console.log('====================================');
                            console.log(e);
                            console.log('====================================');
                        });
                    });
                });
            });
        });

    })
    connection.connect(config.sftpConnectionCredentials);
}

function pushData(convertedData) {
    return new Promise((resolve, reject) => {
        arr.push({
            familyName: isNumber(convertedData.ModelGroupName) ? convertedData.ModelGroupName.toString() : convertedData.ModelGroupName,
            familyCode: isNumber(convertedData.ModelGroupCode) ? convertedData.ModelGroupCode.toString() : convertedData.ModelGroupCode,
            bikeModelCode: isNumber(convertedData.motorcycle_code) ? convertedData.motorcycle_code.toString() : convertedData.motorcycle_code,
            bikeName: isNumber(convertedData.motorcycle_name) ? convertedData.motorcycle_name.toString() : convertedData.motorcycle_name,
            bikeStatus: isNumber(convertedData.Status) ? convertedData.Status.toString() : convertedData.Status
        });
        resolve(arr);
    });
}

function pushingData(arr, resObj) {
    BikeCsvSchema.insertMany(arr, (err, docs) => {
        if (docs) {
            resObj.success = true;
            resObj.message = "inserted successfully";
            resObj.data = docs.length;
            console.log('====================================');
            console.log(resObj.message);
            console.log('====================================');
        } else {
            resObj.success = false;
            resObj.message = "error inserting docs";
            resObj.data = err;
            console.log('====================================');
            console.log(resObj.message);
            console.log('====================================');
        }
    })

}