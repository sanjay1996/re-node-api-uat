import { Client } from "ssh2";
import fs from "fs";
import mongoose from "mongoose";
import csv from "csvtojson";
import asyncc from "asyncawait/async";
import awaitt from "asyncawait/await";
import DealerSchema from "../models/dealers";
import RideSchema from "../models/ride";
import request from "request";
import moment from "moment";
import config from "../../config/env";
import schedule from "node-schedule";

var timeInterval = 1000 * 60 * 60;

function getRideoutsFromSFTP() {
  console.log("GET RIDEOUT SCHEDULAER UP AND RUNNING ");
  schedule.scheduleJob({ hour: 0, minute: 0 }, function() {
    var tempRideoutArr = [];
    const conn = new Client();
    conn.on("ready", () => {
      conn.sftp(function(err, sftp) {
        if (err) {
          conn.end();
          throw err;
        } else {
          sftp.readdir(`DMS-Web3/Production/RideList`, (err, rideOutFiles) => {
            if (err || rideOutFiles.length == 0) {
              console.log("======================================");
              console.log("No Ride Out Files Found");
              console.log("======================================");
              if (err) {
                conn.end();
                throw err;
              }
            } else {
              sftp.readFile(`DMS-Web3/Production/RideList/${rideOutFiles[0].filename}`, (err, readedRideOutData) => {
                if (err) {
                  conn.end();
                  throw err;
                } else {
                  fs.writeFile(
                    `archivedFiles/rideoutsfromsftp/${rideOutFiles[0].filename}`,
                    readedRideOutData.toString().trim(),
                    err => {
                      if (err) {
                        conn.end();
                        throw err;
                      } else {
                        csv({ delimiter: "|" })
                          .fromFile(`archivedFiles/rideoutsfromsftp/${rideOutFiles[0].filename}`)
                          .then(rideOutsJsonFromCSVFile => {
                            // console.log(rideOutsJsonFromCSVFile)
                            // console.log("===========rideOutsJsonFromCSVFile===============")
                            rideOutsJsonFromCSVFile.forEach((element, index) => {
                              console.log("index======" + index);
                              let newObj = {
                                locality: {
                                  country: "in",
                                  language: "en"
                                },
                                rideCategory: "ride-out",
                                rideName: element.Name,
                                startPoint: {
                                  name: element.StartPoint,
                                  latitude: parseFloat(
                                    element.RideSourceLatitude == "NaN" ? 0 : element.RideSourceLatitude
                                  ),
                                  longitude: parseFloat(
                                    element.RideSourceLongitude == "NaN" ? 0 : element.RideSourceLongitude
                                  )
                                },
                                endPoint: {
                                  name: element.Destination,
                                  latitude: parseFloat(
                                    element.RideDestinationLatitude == "NaN" ? 0 : element.RideDestinationLatitude
                                  ),
                                  longitude: parseFloat(
                                    element.RideDestinationLongitude == "NaN" ? 0 : element.RideDestinationLongitude
                                  )
                                },
                                rideStartDateIso: moment(element.StartDate, "YYYYMMDD").toISOString(),
                                rideEndDateIso: moment(element.EndDate, "YYYYMMDD").toISOString(),
                                RideId: parseInt(element.RideID == "NaN" ? 0 : element.RideID),
                                createdOn: moment(element.DocDate, "YYYYMMDD").toISOString(),
                                startDate: moment(element.StartDate, "YYYYMMDD").format("DD-MM-YYYY"),
                                endDate: moment(element.EndDate, "YYYYMMDD").format("DD-MM-YYYY"),
                                terrain: element.TerrainType,
                                totalDistance: parseInt(element.TotalDistance == "NaN" ? 0 : element.TotalDistance),
                                geo: {
                                  type: "Point",
                                  coordinates: [
                                    parseFloat(element.RideSourceLatitude == "NaN" ? 0 : element.RideSourceLatitude),
                                    parseFloat(element.RideSourceLongitude == "NaN" ? 0 : element.RideSourceLongitude)
                                  ]
                                },

                                //===================To Be Added TO Officle Ride Schema=====================

                                BranchCode: element.CompanyCode,
                                CountryCode: element.CountryCode,
                                CompanyName: element.CompanyName
                              };
                              tempRideoutArr.push(newObj);
                              if (rideOutsJsonFromCSVFile.length - 1 == index) {
                                RideSchema.insertMany(tempRideoutArr)
                                  .then(insertedRideouts => {
                                    var foo = asyncc(function() {
                                      var count = 0;
                                      // console.log("=============newObj=================")
                                      // console.log(insertedRideouts)
                                      console.log("=============newObj=================" + insertedRideouts.length);
                                      for (var ride of insertedRideouts) {
                                        awaitt(mappingRideOutsToDealers(ride));
                                        if (count == insertedRideouts.length - 1) {
                                          sftp.writeFile(
                                            `DMS-Web3/Production/RideList/Processed/${rideOutFiles[0].filename}`,
                                            readedRideOutData,
                                            err => {
                                              if (err) {
                                                conn.end();
                                                throw err;
                                              } else {
                                                sftp.unlink(
                                                  `DMS-Web3/Production/RideList/${rideOutFiles[0].filename}`,
                                                  err => {
                                                    if (err) {
                                                      conn.end();
                                                      throw err;
                                                    } else {
                                                      fs.unlink(
                                                        `archivedFiles/rideoutsfromsftp/${rideOutFiles[0].filename}`,
                                                        err => {
                                                          if (err) {
                                                            conn.end();
                                                            throw err;
                                                          } else {
                                                            console.log("===========Both FIles Unlinked==============");
                                                            conn.end();
                                                          }
                                                        }
                                                      );
                                                    }
                                                  }
                                                );
                                              }
                                            }
                                          );
                                        }
                                        count++;
                                      }
                                    });
                                    foo();
                                  })
                                  .catch(exception => {
                                    conn.end();
                                    throw exception;
                                  });
                              }
                            });
                          })
                          .catch(exception => {
                            conn.end();
                            throw exception;
                          });
                      }
                    }
                  );
                }
              });
            }
          });
        }
      });
    });
    conn.connect(config.sftpConnectionCredentials);
  });
}

function mappingRideOutsToDealers(RideObj) {
  return new Promise((resolve, reject) => {
    DealerSchema.findOne({ BranchCode: RideObj.BranchCode })
      .then(foundDealer => {
        console.log(RideObj);

        console.log("====================================");
        console.log(foundDealer);
        console.log("====================================");
        if (foundDealer != null) {
          foundDealer.rideOutId.push(RideObj._id);
          foundDealer
            .save()
            .then(savedDealer => {
              RideSchema.findOne({ _id: RideObj._id })
                .then(foundRide => {
                  foundRide.DealerId = savedDealer._id;
                  foundRide.rideImages.push({ srcPath: "/node/assets/Rides/ridePlaceholder.jpg" });
                  foundRide.save().then(savedRide => {
                    console.log("================savedDealer====================");
                    console.log(savedDealer);
                    console.log("================savedDealer====================");

                    var waypointsData = "";
                    for (var i = 0; i < savedRide.waypoints.length; i++) {
                      var waypointsname = savedRide.waypoints[i].name;
                      var waypointslat = savedRide.waypoints[i].latitude;
                      var waypointslng = savedRide.waypoints[i].longitude;
                      waypointsData += waypointsname + "," + waypointslat + "," + waypointslng + "|";
                    }
                    var username = config.aemcredentials.user;
                    var password = config.aemcredentials.password;

                    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
                    console.log("==============auth======================");
                    console.log(username);
                    console.log(password);
                    console.log("==============auth======================");
                    request.post(
                      {
                        headers: {
                          "content-type": "application/x-www-form-urlencoded",
                          Authorization: auth
                        },
                        url: `${config.aemAuthorUrl}/bin/createPage`,
                        form: {
                          entity: "ride-out",
                          pageId: savedRide._id.toString(),
                          title: savedRide.rideName,
                          country: savedRide.locality.country,
                          language: savedRide.locality.language,
                          pageProperties: JSON.stringify({
                            startPointPlaceName: savedRide.startPoint.name,
                            startPointLongitude: savedRide.startPoint.latitude,
                            startPointLatitude: savedRide.startPoint.longitude,
                            endPointPlaceName: savedRide.endPoint.name,
                            endPointLongitude: savedRide.endPoint.latitude,
                            endPointLatitude: savedRide.endPoint.longitude,
                            startDate: savedRide.startDate ? savedRide.startDate : Date.now(),
                            endDate: savedRide.endDate ? savedRide.endDate : Date.now(),
                            terrain: savedRide.terrain,
                            totalDistance: savedRide.totalDistance.toString(),
                            author: savedDealer._id ? savedDealer.DealerPrincipalName : "",
                            rideDescription: savedRide.rideDetails ? savedRide.rideDetails : "",
                            thumbnailImagePath: savedRide.rideImages.length != 0 ? foundRide.rideImages[0].srcPath : "",
                            wayPoints: waypointsData
                          })
                        }
                      },
                      function(error, response, data) {
                        var resData = JSON.parse(data);
                        console.log("===============errorr=====================");
                        console.log(error);
                        console.log("===============errorr=====================");

                        console.log("==============response======================");
                        console.log(response);
                        console.log("===============response=====================");

                        console.log("---------------------resData======-----------------------------------");
                        console.log(resData);
                        console.log("--------------resData-----------------===================");
                        foundRide.ridePageUrl = resData.pagePath;
                        foundRide
                          .save()
                          .then(ride => {
                            console.log("++++++++++++++++++rideUrl+++++++++++++++++++++");
                            console.log("yo hego url " + ride.ridePageUrl);
                            console.log("====================rideUrl=====================");
                            resolve(foundRide);
                          })
                          .catch(e => {
                            console.log("================foundRide Exception====================");
                            console.log(e);
                            console.log("===============foundRide Exception=====================");
                          });
                      }
                    );
                  });
                })
                .catch(exception => {
                  conn.end();
                  throw exception;
                });
            })
            .catch(exception => {
              conn.end();
              throw exception;
            });
        } else {
          resolve(RideObj);
        }
      })
      .catch(e => {
        console.log("================Exception====================");
        console.log(e);
        console.log("===============Exception=====================");
      });
  });
}

export default {
  getRideoutsFromSFTP
};
