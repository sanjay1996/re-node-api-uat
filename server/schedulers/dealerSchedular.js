import { Client } from "ssh2";
import fs from "fs";
import mongoose from "mongoose";
import csv from "csvtojson";
import asyncc from "asyncawait/async";
import awaitt from "asyncawait/await";
import Dealer from "../models/dealers";
import request from "request";
import moment from "moment";
import config from "../../config/env";
import jsdom, { JSDOM } from "jsdom";
import schedule from "node-schedule";


var singleInterfaceSchema = new mongoose.Schema({
  BranchCode: { type: String, default: null, trim: true },
  StoreManagerDesignation: { type: String, default: null, trim: true },
  Country: { type: String, default: null, trim: true },
  StoreManagerPhoneNo: { type: String, default: null, trim: true },
  StoreManagerEmailID: { type: String, default: null, trim: true },
  StoreEmailID: { type: String, default: null, trim: true },
  WeeklyOff: { type: String, default: null, trim: true },
  BusinessHours: { type: String, default: null, trim: true },
  Latitude: { type: String, default: null, trim: true },
  Longitude: { type: String, default: null, trim: true },
  StoreManagerName: { type: String, default: null, trim: true },
  DealerMobileNo: { type: String, default: null, trim: true },
  AlternateStoreNo: { type: String, default: null, trim: true },
  MainPhoneNo: { type: String, default: null, trim: true },
  GooglePlaceID: { type: String, default: null, trim: true },
  Status: { type: String, default: null, trim: true },
});

var singleInterface = mongoose.model('singleInterface', singleInterfaceSchema);

var branchMasterSchema = new mongoose.Schema({
  DealerID: { type: String, default: null, trim: true },
  DealerName: { type: String, default: null, trim: true },
  BranchID: { type: String, default: null, trim: true },
  BranchCode: { type: String, default: null, trim: true },
  BranchName: { type: String, default: null, trim: true },
  Actual_Address1: { type: String, default: null, trim: true },
  Actual_Address2: { type: String, default: null, trim: true },
  Actual_Address3: { type: String, default: null, trim: true },
  Actual_City: { type: String, default: null, trim: true },
  Actual_State: { type: String, default: null, trim: true },
  CityID: { type: String, default: null, trim: true },
  StateID: { type: String, default: null, trim: true },
  Actual_Pincode: { type: String, default: null, trim: true },
  BranchTypeIdentifier: { type: String, default: null, trim: true },
  IsServiceApplicable: { type: String, default: null, trim: true },
  IsSalesApplicable: { type: String, default: null, trim: true },
  IsActive: { type: String, default: null, trim: true },
  StartTime: { type: String, default: null, trim: true },
  EndTime: { type: String, default: null, trim: true },
  CityType: { type: String, default: null, trim: true },
  ASMContactCode: { type: String, default: null, trim: true },
  TSMContactCode: { type: String, default: null, trim: true },
  RMContactCode: { type: String, default: null, trim: true },
  ZMContactCode: { type: String, default: null, trim: true },
  RSMContactCode: { type: String, default: null, trim: true },
  TPMContactCode: { type: String, default: null, trim: true },
  GearASMContactCode: { type: String, default: null, trim: true },
  GearRMContactCode: { type: String, default: null, trim: true },
  DealerPrincipalName: { type: String, default: null, trim: true },
  StoreManagerName: { type: String, default: null, trim: true },
  StoreManagerDesignation: { type: String, default: null, trim: true },
  StoreManagerPhoneNo: { type: String, default: null, trim: true },
  StoreManagerEmailID: { type: String, default: null, trim: true },
  WeeklyOff: { type: String, default: null, trim: true },
  BusinessHours: { type: String, default: null, trim: true },
  Locality: { type: String, default: null, trim: true },
  Landmark: { type: String, default: null, trim: true },
  StatusCode: { type: String, default: null, trim: true },
  Status: { type: String, default: null, trim: true },
  Dealer_Source: { type: String, default: null, trim: true },
});

var branchMaster = mongoose.model('branchMaster', branchMasterSchema);

function dealersFromSftpToMongo() {
  let tempDealerObj = {};
  var timeInterval = 1000 * 60 * 60;
  var env = "Production";
  console.log(`Dealer Sftp Data Schedular is up and will upload the file on every saturday @ 1:00AM`);

  schedule.scheduleJob({ hour: 1, minute: 0, dayOfWeek: 7 }, function () {
    console.log('Time for tea!');
    var conn = new Client();
    conn.on('ready', () => {
      conn.sftp(function (err, sftp) {
        if (err) {
          conn.end();
          throw err;
        } else {
          console.log("Inside else");
          sftp.readdir(`DMS-Web3/${env}/Dealer/single_interface`, (err, singleInterfaceFiles) => {
            console.log("Inside branch master ");
            if (singleInterfaceFiles.length == 0 || err) {
              console.log('====================================');
              console.log("There is no  single inteface file to get or something went seriously wrong!!!");
              console.log('====================================');
              conn.end();
              throw err;
            } else {
              sftp.readdir(`DMS-Web3/${env}/Dealer/branch_master`, (err, branchMasterFiles) => {
                if (branchMasterFiles.length == 0 || err) {
                  console.log('====================================');
                  console.log("There is no  branch master file to get or something went seriously wrong!!!");
                  console.log('====================================');
                  conn.end();
                  // throw err;
                } else {
                  sftp.readFile(`DMS-Web3/${env}/Dealer/single_interface/${singleInterfaceFiles[0].filename}`, (err, singleInterfaceFileData) => {
                    if (err) {
                      conn.end();
                      console.log('====================================');
                      console.log("There is no data in branch master file");
                      console.log('====================================');
                      throw err;
                    } else {
                      sftp.readFile(`DMS-Web3/${env}/Dealer/branch_master/${branchMasterFiles[0].filename}`, (err, branchMasterFileData) => {
                        if (err) {
                          conn.end();
                          console.log('====================================');
                          console.log("There is no data in single interface file");
                          console.log('====================================');
                          throw err;
                        } else {
                          fs.writeFile(`archivedFiles/dealerCsvs/singleInterface.csv`, singleInterfaceFileData, (err) => {
                            if (err) { throw err; } else {
                              fs.writeFile(`archivedFiles/dealerCsvs/branchMaster.csv`, branchMasterFileData, (err) => {
                                if (err) { throw err; } else {
                                  csv({ delimiter: "|" }).fromFile(`archivedFiles/dealerCsvs/singleInterface.csv`)
                                    .then(singleInterfaceJsonArray => {
                                      singleInterface.insertMany(singleInterfaceJsonArray).then(() => {
                                        csv({ delimiter: "|" }).fromFile(`archivedFiles/dealerCsvs/branchMaster.csv`)
                                          .then(branchMasterJsonArray => {
                                            branchMaster.insertMany(branchMasterJsonArray).then(() => {
                                              singleInterface.find().then(asyncc((foundSingleInterfaceData) => {
                                                // tempData1 = foundSingleInterfaceData;
                                                branchMaster.find().then(asyncc((foundBranchMasterData) => {
                                                  console.log('====================================');
                                                  console.log(`Both files are now in database single=${foundSingleInterfaceData.length} master=${foundBranchMasterData.length}`);
                                                  console.log('====================================');
                                                  // tempData2 = foundBranchMasterData;
                                                  var i = 0;
                                                  for (var tempData1 of foundSingleInterfaceData) {
                                                    for (var tempData2 of foundBranchMasterData) {
                                                      if (tempData1.BranchCode == tempData2.BranchCode) {
                                                        let tempOneSingle = {
                                                          Status: tempData1.Status,
                                                          GooglePlaceId: tempData1.GooglePlaceID,
                                                          MainPhoneNo: tempData1.MainPhoneNo,
                                                          AlternateStoreNo: tempData1.AlternateStoreNo,
                                                          DealerMobileNo: tempData1.DealerMobileNo,
                                                          StoreManagerName: tempData1.StoreManagerName,
                                                          Longitude: parseFloat(tempData1.Longitude),
                                                          Latitude: parseFloat(tempData1.Latitude),
                                                          BusinessHours: tempData1.BusinessHours,
                                                          WeeklyOff: tempData1.WeeklyOff,
                                                          StoreEmailId: tempData1.StoreEmailID,
                                                          StoreManagerEmailID: tempData1.StoreManagerEmailID,
                                                          StoreManagerPhoneNo: tempData1.StoreManagerPhoneNo,
                                                          Country: tempData1.Country,
                                                          StoreManagerDesignation: tempData1.StoreManagerDesignation,
                                                          BranchCode: tempData1.BranchCode,
                                                        };
                                                        let tempTwoMaster = {
                                                          Dealer_Source: tempData2.Dealer_Source,
                                                          StatusCode: tempData2.StatusCode,
                                                          DealerPrincipalName: tempData2.DealerPrincipalName,
                                                          GearRMContactCode: tempData2.GearRMContactCode,
                                                          GearASMContactCode: tempData2.GearASMContactCode,
                                                          TPMContactCode: tempData2.TPMContactCode,
                                                          RSMContactCode: tempData2.RSMContactCode,
                                                          ZMContactCode: tempData2.ZMContactCode,
                                                          RMContactCode: tempData2.RMContactCode,
                                                          TSMContactCode: tempData2.TSMContactCode,
                                                          ASMContactCode: tempData2.ASMContactCode,
                                                          CityType: tempData2.CityType,
                                                          EndTime: tempData2.EndTime,
                                                          StartTime: tempData2.StartTime,
                                                          IsActive: tempData2.IsActive,
                                                          Gallery: [],
                                                          DealerPrincipalName: tempData2.DealerPrincipalName == "NULL" ? "" : tempData2.DealerPrincipalName,
                                                          Cover_image: tempData2.Cover_image ? `/node/assets/Dealer/CoverImage/${tempData2.Cover_image}` : "/node/assets/Dealer/CoverImage/Cover_image_placeholder.jpg",
                                                          Cover_image_mobile: tempData2.Cover_image_mobile ? `/node/assets/Dealer/CoverImage/mobile/${tempData2.Cover_image_mobile}` : "/node/assets/Dealer/CoverImage/mobile/Cover_image_mobile_placeholder.jpg",
                                                          Thumbnail_Image: tempData2.Thumbnail_Image ? `/node/assets/Dealer/ThumbnailImages/${tempData2.Thumbnail_Image}` : "/node/assets/Dealer/ThumbnailImage/Thumbnail_Image_placeholder.jpg",
                                                          PrimaryImage: tempData2.PrimaryImage ? `/node/assets/Dealer/PrimaryImages/${tempData2.PrimaryImage}` : "/node/assets/Dealer/PrimaryImage/PrimaryImage_placeholder.jpg",
                                                          PrimaryImage_mobile: tempData2.PrimaryImage_mobile ? `/node/assets/Dealer/PrimaryImages/mobiles/${tempData2.PrimaryImage_mobile}` : "/node/assets/Dealer/PrimaryImage/mobile/PrimaryImage_mobile_placeholder.jpg",
                                                          DealerImage: tempData2.DealerImage ? `/node/assets/Dealer/DealerImages/${tempData2.DealerImage}` : "/node/assets/Dealer/DealerImage/DealerImage_placeholder.jpg",
                                                          ifSales: tempData2.IsSalesApplicable == 1 ? true : false,
                                                          ifService: tempData2.IsServiceApplicable == 1 ? true : false,
                                                          BranchTypeIdentifier: tempData2.BranchTypeIdentifier,
                                                          Pincode: tempData2.Actual_Pincode,
                                                          StateID: tempData2.StateID,
                                                          CityID: tempData2.CityID,
                                                          State: tempData2.Actual_State,
                                                          City: tempData2.Actual_City,
                                                          AddressLine3: tempData2.Actual_Address3,
                                                          AddressLine2: tempData2.Actual_Address2,
                                                          AddressLine1: tempData2.Actual_Address1,
                                                          BranchName: tempData2.BranchName,
                                                          BranchID: tempData2.BranchID,
                                                          Locality: tempData2.Locality,
                                                          Landmark: tempData2.Landmark,
                                                          DealerName: tempData2.DealerName,
                                                          DealerID: tempData2.DealerID,
                                                          locality: { language: "en", country: "in" }
                                                        };
                                                        tempDealerObj = Object.assign(tempOneSingle, tempTwoMaster);
                                                        console.log('================tempDealerObj====================');
                                                        console.log(tempDealerObj);
                                                        console.log('===============tempDealerObj=====================');
                                                        if (tempDealerObj.Status == "New") {
                                                          awaitt(inserteNewDealer(tempDealerObj));
                                                        } else if (tempDealerObj.Status == "Modified") {
                                                          awaitt(updateDealer(tempDealerObj));
                                                        } else if (tempDealerObj.Status == "Remove") {
                                                          awaitt(removeDealer(tempDealerObj));
                                                        } else {
                                                          console.log('====================================');
                                                          console.log("No Such Status Found status = " + tempDealerObj.Status);
                                                          console.log('====================================');
                                                        }
                                                      }
                                                    }
                                                    if (foundSingleInterfaceData.length - 1 == i) {
                                                      singleInterface.collection.drop().then((dropStatusSI) => {
                                                        if (dropStatusSI == true) {
                                                          branchMaster.collection.drop().then((dropStatusBM) => {
                                                            if (dropStatusBM) {
                                                              fs.readFile(`archivedFiles/dealerCsvs/singleInterface.csv`, (err, loaclSIData) => {
                                                                if (err) { conn.end(); throw err; }
                                                                sftp.writeFile(`DMS-Web3/${env}/archiveDealers/${singleInterfaceFiles[0].filename}`, loaclSIData, (err) => {
                                                                  if (err) { conn.end(); throw err; }
                                                                  sftp.unlink(`DMS-Web3/${env}/Dealer/single_interface/${singleInterfaceFiles[0].filename}`, (err) => {
                                                                    if (err) { conn.end(); throw err; }
                                                                    console.log('=============ServerFIle Unlinked=======single_interface================');
                                                                    fs.unlink(`archivedFiles/dealerCsvs/singleInterface.csv`, (err) => {
                                                                      if (err) { conn.end(); throw err; }
                                                                      console.log('=============Local  Unlinked=======single_interface================');
                                                                      fs.readFile(`archivedFiles/dealerCsvs/branchMaster.csv`, (err, localBMData) => {
                                                                        if (err) { conn.end(); throw err; }
                                                                        sftp.writeFile(`DMS-Web3/${env}/archiveDealers/${branchMasterFiles[0].filename}`, localBMData, (err) => {
                                                                          if (err) { conn.end(); throw err; }
                                                                          sftp.unlink(`DMS-Web3/${env}/Dealer/branch_master/${branchMasterFiles[0].filename}`, (err) => {
                                                                            if (err) { conn.end(); throw err; }
                                                                            console.log('=============ServerFIle Unlinked=======branch_master================');
                                                                            fs.unlink(`archivedFiles/dealerCsvs/branchMaster.csv`, (err) => {
                                                                              if (err) { conn.end(); throw err; }
                                                                              console.log('=============Local  Unlinked=======branch_master================');
                                                                              conn.end();
                                                                            });
                                                                          });
                                                                        });
                                                                      });
                                                                    });
                                                                  });
                                                                });
                                                              });
                                                            }
                                                          }).catch((e) => {
                                                            console.log(e);
                                                          });
                                                        }
                                                      }).catch((e) => {
                                                        console.log(e);
                                                      });
                                                    }
                                                    i++;
                                                  }
                                                })).catch((e) => {
                                                  console.log(e);
                                                });
                                              })).catch((e) => {
                                                console.log(e);
                                              });
                                            }).catch((e) => {
                                              console.log(e);
                                            });
                                          }).catch((e) => {
                                            console.log(e);
                                          });
                                      });
                                    }).catch((e) => {
                                      console.log(e);
                                    });
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          })
        }
      });
    });
    conn.connect(config.sftpConnectionCredentials);
  });

}


function inserteNewDealer(newDealerData) {
  return new Promise((resolve, reject) => {
    const newDealer = new Dealer(newDealerData);
    newDealer.save().then(savedDealer => {
      var username = config.aemcredentials.user;
      var password = config.aemcredentials.password;
      var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
      request.post({
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          "Authorization": auth
        },
        url: `${config.aemAuthorUrl}/bin/createPage`,
        form: {
          entity: 'dealers',
          pageId: savedDealer._id.toString(),
          title: savedDealer.DealerName,
          country: savedDealer.locality.country,
          language: savedDealer.locality.language,
          pageProperties: JSON.stringify({
            "storeManagerName": savedDealer.DealerPrincipalName,
            "phoneNo": savedDealer.MainPhoneNo,
            "summary": savedDealer.DealerDescription ? savedDealer.DealerDescription : '',
            "thumbnailImagePath": savedDealer.Thumbnail_Image,
            "googlePlaceId": savedDealer.GooglePlaceId,
            "email": savedDealer.StoreEmailId,
            "cover_image": savedDealer.Cover_image,
            "BranchTypeIdentifier": savedDealer.BranchTypeIdentifier,
            "dateSort": moment().toISOString(),
            "BranchCode": savedDealer.BranchCode,
            "pincode": savedDealer.Pincode,
            "DealerImage": savedDealer.DealerImage,
            "PrimaryImage_mobile": savedDealer.PrimaryImage_mobile,
            "PrimaryImage": savedDealer.PrimaryImage,
            "Cover_image_mobile": savedDealer.Cover_image_mobile,
            "AddressLine1": savedDealer.AddressLine1,
            "AddressLine2": savedDealer.AddressLine2,
            "AddressLine3": savedDealer.AddressLine3,
            "City": savedDealer.City,
            "State": savedDealer.State,
            "StoreManager": savedDealer.StoreManagerName
          })
        }
      }, function (error, response, data) {
        var data = JSON.parse(data);
        savedDealer.DealerUrl = data.pagePath
        savedDealer.save().then(() => {
          resolve(savedDealer);
          console.log('==============savedDealer======================');
          console.log(savedDealer);
          console.log('================savedDealer====================');
        }).catch((err) => {
          reject(err)
        });
      })
    }).catch((err) => {
      reject(err)
    });
  })
}

function updateDealer(updatedData) {


  return new Promise((resolve, reject) => {

    Dealer.findOne({ DealerID: updatedData.DealerID })
      .then(foundDealer => {
        // foundDealer = updatedData;
        foundDealer.locality = updatedData.locality,
          foundDealer.BranchCode = updatedData.BranchCode,
          foundDealer.AddressLine1 = updatedData.AddressLine1,
          foundDealer.AddressLine2 = updatedData.AddressLine2,
          foundDealer.AddressLine3 = updatedData.AddressLine3,
          foundDealer.Pincode = updatedData.Pincode,
          foundDealer.City = updatedData.City,
          foundDealer.State = updatedData.State,
          foundDealer.Country = updatedData.Country,
          foundDealer.MainPhoneNo = updatedData.MainPhoneNo,
          foundDealer.StoreEmailId = updatedData.StoreEmailId,
          foundDealer.WeeklyOff = updatedData.WeeklyOff,
          foundDealer.StoreManagerName = updatedData.StoreManagerName,
          foundDealer.BusinessHours = updatedData.BusinessHours,
          foundDealer.Longitude = updatedData.Longitude,
          foundDealer.GooglePlaceId = updatedData.GooglePlaceId,
          foundDealer.DealerID = updatedData.DealerID,
          foundDealer.BranchName = updatedData.BranchName,
          foundDealer.CityID = updatedData.CityID,
          foundDealer.StateID = updatedData.StateID,
          foundDealer.Locality = updatedData.Locality,
          foundDealer.ifSales = updatedData.ifSales,
          foundDealer.ifService = updatedData.ifService,
          foundDealer.BranchTypeIdentifier = updatedData.BranchTypeIdentifier,
          foundDealer.DealerPrincipalName = updatedData.DealerPrincipalName,
          foundDealer.Status = updatedData.Status,
          foundDealer.Dealer_Source = updatedData.Dealer_Source,
          foundDealer.Cover_image = updatedData.Cover_image,
          foundDealer.Cover_image_mobile = updatedData.Cover_image_mobile,
          foundDealer.Thumbnail_Image = updatedData.Thumbnail_Image,
          foundDealer.PrimaryImage = updatedData.PrimaryImage,
          foundDealer.PrimaryImage_mobile = updatedData.PrimaryImage_mobile,
          foundDealer.DealerImage = updatedData.DealerImage,
          foundDealer.DealerName = updatedData.DealerName,
          foundDealer.Landmark = updatedData.Landmark,
          foundDealer.DealerOneLiner = updatedData.DealerOneLiner,
          foundDealer.DealerDescription = updatedData.DealerDescription,
          foundDealer.Gallery = updatedData.Gallery,
          foundDealer.rideOutId = updatedData.rideOutId

        console.log("=============foundDealer===Jo Save hone jaa rh hai========================")
        console.log(foundDealer)
        console.log("=============foundDealer===========================")
        foundDealer.save().then((updatedDealer) => {
          console.log("=======================updated Dealer====================")
          console.log(updatedDealer);
          console.log("=======================updated Dealer====================")
          var username = config.aemcredentials.user;
          var password = config.aemcredentials.password;
          var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
          request.post({
            headers: {
              'content-type': 'application/x-www-form-urlencoded',
              "Authorization": auth
            },
            url: `${config.aemAuthorUrl}/bin/modifyDealers`,
            form: {
              dealerId: updatedDealer._id.toString(),
              country: updatedDealer.locality.country,
              language: updatedDealer.locality.language,
              AddressLine1: updatedDealer.AddressLine1,
              AddressLine2: updatedDealer.AddressLine2,
              AddressLine3: updatedDealer.AddressLine3,
              BranchCode: updatedDealer.BranchCode,
              BranchTypeIdentifier: updatedDealer.BranchTypeIdentifier,
              Cover_image_mobile: updatedDealer.Cover_image_mobile,
              City: updatedDealer.City,
              DealerImage: updatedDealer.DealerImage,
              PrimaryImage: updatedDealer.PrimaryImage,
              PrimaryImage_mobile: updatedDealer.PrimaryImage_mobile,
              State: updatedDealer.State,
              StoreManager: updatedDealer.StoreManagerName,
              cover_image: updatedDealer.Cover_image,
              email: updatedDealer.StoreEmailId,
              entity: 'dealers',
              googlePlaceId: updatedDealer.GooglePlaceId,
              phoneNo: updatedDealer.MainPhoneNo,
              pincode: updatedDealer.Pincode,
              storeManagerName: updatedDealer.DealerPrincipalName,
              summary: updatedDealer.DealerDescription ? updatedDealer.DealerDescription : '',
              thumbnailImagePath: updatedDealer.Thumbnail_Image,
            }
          }, function (error, response, data) {
            console.log(data)
            var data = JSON.parse(data);
            if (data.statusCode == 200) {
              resolve(updatedDealer)
            }
          })
        })
      })
  })
}

function removeDealer(dealerToBeRemoved) {
  return new Promise((resolve, reject) => {
    Dealer.findOne({ BranchCode: dealerToBeRemoved.BranchCode }).then((foundDealerToBeRemoved) => {
      // console.log('====================================');
      // console.log(foundDealerToBeRemoved);
      // console.log('====================================');
      var username = config.aemcredentials.user;
      var password = config.aemcredentials.password;
      var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
      request.post({
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          "Authorization": auth
        },
        url: `${config.aemAuthorUrl}/bin/replicate.json`,
        form: {
          "cmd": "deactivate",
          "force": false,
          "path": foundDealerToBeRemoved.DealerUrl
        }
      }, asyncc(function (error, response, data) {
        console.log("DEACTIVATING");
        const dom = new JSDOM(data);
        console.log(dom.window.document.getElementById("Status").innerHTML);
        if (dom.window.document.getElementById("Status").innerHTML == "200") {
          request.post({
            headers: {
              'content-type': 'application/x-www-form-urlencoded',
              "Authorization": auth
            },
            url: `${config.aemAuthorUrl}/bin/wcmcommand`,
            form: {
              "cmd": "deletePage",
              '_charset_': "utf-8",
              "force": false,
              "path": foundDealerToBeRemoved.DealerUrl
            }
          }, function (error, response, data) {
            console.log("DELETING");
            const dom = new JSDOM(data);
            // console.log(data)
            console.log(dom.window.document.getElementById("Status").innerHTML);
            if (dom.window.document.getElementById("Status").innerHTML == "200") {
              Dealer.findOneAndRemove({ DealerID: dealerToBeRemoved.DealerID })
                .then((deletedData) => {
                  resolve(dealerToBeRemoved)
                  console.log("==============bal bla bla======================");
                  console.log(deletedData);
                  console.log("==============bla bal bla======================");
                }).catch((err) => {
                  reject(err)
                });
            } else {
              console.log('====================================');
              console.log("No such page found to delete");
              console.log('====================================');
            }
          });
        }
      }));
    })
  });
}

export default {
  dealersFromSftpToMongo
}
