import config from "../../config/env";
import jsonexport from "jsonexport";
import { Client } from "ssh2";

export default function uploadToSftp(fileName, dataArr, pathToWriteFile) {
    var conn = new Client();
    conn.on('ready', function () {
        conn.sftp(function (err, sftp) {
            if (err) throw err, conn.end();

            jsonexport(dataArr, { rowDelimiter: '|' }, (err, savedData) => {
                if (err) {
                    conn.end();
                    console.log('====================================');
                    console.log(err);
                    console.log('====================================');
                } else {
                    sftp.writeFile(pathToWriteFile + fileName, savedData, (err, result) => {
                        if (err) {
                            console.log('====================================');
                            console.log(err);
                            console.log('====================================');
                            conn.end();
                        } else {
                            console.log("Uploaded Successfully :) :)");
                            conn.end();
                        }
                    });
                }
            });
        });
    }).connect({
   	host: "ftpL.royalenfield.com",
    	port: 22,
    	username: "interface",
    	password: "Interface@re",
    	privateKey: fs.readFileSync("config/id_rsa.ppk"),
    	passphrase: "Interface@re",
    	readyTimeout: 99999
       });
//.connect(config.sftpConnectionCredentials);
}
