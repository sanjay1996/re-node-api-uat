/* eslint-disable */
import nodemailer from "nodemailer";
import config from "../../config/env";
import smtpTransport from "nodemailer-smtp-transport";
import path from "path";
import UserSchema from "../models/user";
import mailClient from "mail";

// const EmailTemplate = require('email-templates').EmailTemplate;

// const emailDir = path.resolve(__dirname, '../templates', 'emailVerify');
// const emailVerify = new EmailTemplate(path.join(emailDir));

// const registerDir = path.resolve(__dirname, '../templates', 'register');
// const register = new EmailTemplate(path.join(registerDir));

// const forgotDir = path.resolve(__dirname, '../templates', 'forgotPassword');
// const forgot = new EmailTemplate(path.join(forgotDir));

function sendEmailApi(subject, text, html, receiver, errorMessage, successMessage, senderEmail) {
  console.log("inside email api");
  var transporter = nodemailer.createTransport(config.email);

  let mailOptions = {
    from: senderEmail || config.emailSender, // sender address
    to: receiver, // list of receivers
    subject: subject, // Subject line
    text: text, // plain text body
    html: html // html body
  };

  transporter.sendMail(mailOptions, function(error, info) {
    var returnObj = {};
    console.log("=============info=======================");
    console.log(info);
    console.log("=============info=======================");
    if (error) {
      returnObj.success = false;
      returnObj.message = errorMessage;
      returnObj.data = error;
      return returnObj;
    } else {
      returnObj.success = true;
      returnObj.message = successMessage;
      return returnObj;
    }
  });
}

function emailWithAttachmentApi(subject, text, html, receiver, sender, attachment, errorMessage, successMessage) {
  console.log("inside attachments email api");
  var transporter = nodemailer.createTransport(config.email);

  let mailOptions = {
    from: sender, // sender address
    to: receiver, // list of receivers
    subject: subject, // Subject line
    text: text, // plain text body
    attachments: attachment
    // html: html // html body
  };

  transporter.sendMail(mailOptions, function(error, info) {
    var returnObj = {};
    console.log("=============info=======================");
    console.log(info);
    console.log("=============info=======================");
    if (error) {
      returnObj.success = false;
      returnObj.message = errorMessage;
      returnObj.data = error;
      return returnObj;
    } else {
      returnObj.success = true;
      returnObj.message = successMessage;
      return returnObj;
    }
  });
}

function sendEmail(email) {
  // mailClient.Mail({
  //   host: 'smtp.gmail.com',
  //   username: 'divanshu.goyal@techchefz.com',
  //   password: 'Divi@1996'
  // });
  // mailClient.message({
  //   from: 'divanshu.goyal@techchefz.com',
  //   to: [email],
  //   subject: 'Hello from Node.JS'
  // })
  // .body('Node speaks SMTP!')
  // .send(function(err) {
  //   if (err) throw err;
  //   console.log('Sent!');
  // });
  // UserSchema.findOneAsync({ _id: userId }).then((userObj) => {
  //   const details = {
  //     host: "smtp.mailtrap.io",
  //     port: 2525,
  //     username: "74b65bc36e217c",
  //     password: "453fbd4ef2ce51"
  //   }
  //   const transporter = nodemailer.createTransport(
  //     smtpTransport({
  //       host: details.host,
  //       port: details.port,
  //       secure: details.secure, // secure:true for port 465, secure:false for port 587
  //       auth: {
  //         user: details.username,
  //         pass: details.password
  //       }
  //     })
  //   );
  //   const locals = Object.assign({}, { data: responseObj });
  //   if (type === 'emailVerify') {
  //     emailVerify.render(locals, (err, results) => { //eslint-disable-line
  //       if (err) {
  //         return err; //eslint-disable-line
  //       }
  //       const mailOptions = {
  //         from: details.username, // sender address
  //         to: userObj.email, // list of receivers
  //         subject: 'Verify your Account with RoyalEnfield', // Subject line
  //         text: results.text, // plain text body
  //         html: results.html, // html body
  //       };
  //       transporter.sendMail(mailOptions, (error, info) => {
  //         if (error) {
  //           return error;
  //         }
  //         return info;
  //       });
  //     });
  //   }
  //   if (type === 'register') {
  //     register.render(locals, (err, results) => { //eslint-disable-line
  //       if (err) {
  //         return err; //eslint-disable-line
  //       }
  //       const mailOptions = {
  //         from: details.username, // sender address
  //         to: userObj.email, // list of receivers
  //         subject: 'Your Account with RoyalEnfield is created', // Subject line
  //         text: results.text, // plain text body
  //         html: results.html // html body
  //       };
  //       transporter.sendMail(mailOptions, (error, info) => {
  //         if (error) {
  //           return error;
  //         }
  //         return info;
  //       });
  //     });
  //   }
  //   if (type === 'forgot') {
  //     forgot.render(locals, (err, results) => {
  //       if (err) {
  //         return err;
  //       }
  //       const mailOptions = {
  //         from: details.username, // sender address
  //         to: userObj.email, // list of receivers
  //         subject: 'Your Account Password for RoyalEnfield', // Subject line
  //         text: results.text, // plain text body
  //         html: results.html // html body
  //       };
  //       transporter.sendMail(mailOptions, (error, info) => {
  //         if (error) {
  //           return error;
  //         }
  //         return info;
  //       });
  //     });
  //   }
  // });
}
export default {
  sendEmail,
  sendEmailApi,
  emailWithAttachmentApi
};
