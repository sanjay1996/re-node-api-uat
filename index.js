import Promise from 'bluebird';
import mongoose from 'mongoose';
import config from './config/env';
import logger from './config/winston';
import app from './config/express';

// promisify mongoose
Promise.promisifyAll(mongoose);


// connect to mongo db
mongoose.connect(config.db, { server: { socketOptions: { keepAlive: 1 } }, useNewUrlParser: true },
  () => {
console.log("Database connected is = "+config.db)
    if (config.env === 'test') {
      console.log(config.solr)
      // mongoose.connection.db.dropDatabase();
      logger.log({
        level: "data",
        message: `database connected`,
        data: config.solr,
        fileName: "index.js",
        functionName: "app.listen"
      });
      logger.log({
        level: "info",
        message: `Database connected in test env`,
        fileName: "index.js",
        functionName: "app.listen"
      });
    }
  });


mongoose.connection.on('connected', function () {
  logger.log({
    level: "info",
    message: `Server Connected to Mongoose @ ${config.db}`,
    fileName: "index.js",
    functionName: "mongoose"
  });
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
  logger.log({
    level: "error",
    message: `Failed to Connect to Mongoose @ ${config.db}`,
    fileName: "index.js",
    functionName: "mongoose"
  });
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  logger.log({
    level: "info",
    message: `Server and Mongoose Disconnected from @ ${config.db}`,
    fileName: "index.js",
    functionName: "mongoose"
  });
});


// const debug = require('debug')('Re-Node-Platform-Web-Api:index');

// listen on port config.port
app.listen(process.env.PORT || config.port, () => {
  // debug(`server started on port ${config.port} (${config.env})`);
  logger.log({
    level: "info",
    message: `Server Started On Port ${config.port} (${config.env})`,
    fileName: "index.js",
    functionName: "app.listen"
  });
});

export default app;
