import passportJWT from 'passport-jwt';
import configEnv from './env';
import UserSchema from '../server/models/user';

const ExtractJwt = passportJWT.ExtractJwt;
const jwtStrategy = passportJWT.Strategy;

function passportConfiguration(passport) {
var currentTime = new Date().getTime() / 1000;
  const opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
  opts.secretOrKey = configEnv.jwtSecret;
  passport.use(new jwtStrategy(opts, (jwtPayload, cb) => {
     UserSchema.findOne({ _id: jwtPayload._doc._id }).select({
      password: 0, addressInfo: 0,
      mobileVerified: 0,
      emailVerified: 0,
      otp: 0,
      emailToken: 0,
      ownedBikeInfo: 0,
      userType: 0,
      loginStatus: 0,
      jwtAccessToken: 0,
    }).then(user => {

if(user != null){
      cb(null, user);
  }else{
 cb(err, false)
}
  }).catch(err => {
      cb(err, false)
    });
  }));
}

export default passportConfiguration;
