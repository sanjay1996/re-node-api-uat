import fs from 'fs';

export default {
  aemcredentials: {
    user: "admin",
    password: "admin"
  },
  websiteUrl: 'https://uat.royalenfield.com',
  aemAuthorUrl: 'http://10.162.134.89:4502/',
  aemPublish: 'http://10.162.134.79:4503/',
  aemPublish2: 'http://10.162.134.80:4503/',
  paymentGatewayURL: "https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction",
  paymentGatewayWorkingKey: "AE721EF883F9C68977603D9D87B0C243",
  paymentGatewayAccessCode: "AVUK02FH25CL44KULC",
  dmsApiUrl: 'http://refineb2cdvp.excelloncloud.com/exapis//api/portalentity',
  solr: '10.162.134.90',
  varnish: 'http://10.162.134.75/',
  env: 'test',
  jwtSecret: '0a6b944d-d2fb-46fc-a85e-0295c986cd9f',
  db: 'mongodb://10.162.134.91/re-node-db',
  port: 3000,
  passportOptions: {
    session: false
  },
  facebookclientsecret: "523a8c74c50a3877c12c2a953aa6f19b",
  googleclientid: "800698914082-gshvmfmti62vt5ltm49ibgn7g2s43rkb.apps.googleusercontent.com",
  googleclientsecret: "Ux4kQpJp96pyxObSSaa6XK_2",
  googleredirecturi: "https://uat.royalenfield.com",
  twitterconfig: {
    consumerKey: 'fGxqYNW0wmHGCkANFWJWuY159',
    consumerSecret: 'KHP5fa20Ql4eZqRIoNxB47MHZhMkZgMFxHp08BUC7fzvSQzl1R',
    callbacksignup: 'https://uat.royalenfield.com/in/en/home/forms/sign-up.html?Signup=Signup',
    callbackLogin: 'https://uat.royalenfield.com/in/en/home.html?login1=login'
  },
  recaptchaSecretKey: "6Lc740cUAAAAAJU7GBrPPIgPRQqFeE_wy6iwHdP6",
  googlereviewkey: "AIzaSyBh-iZwrQajBhWk5wzIn7D6y9xiWiuzbCY",
  email: {
    host: 'smtp.pepipost.com',
    port: 587,
    auth: {
      user: 'royalenfield',
      pass: 'Smtp@2018'
    }
  },
  emailSender: "noreply@royalenfield.com",

  schedulerTimes: {
    rides: 300000,
    testRides: 300000,
    subscrible: 300000,
    registeredUser: 300000,
  },
  sftpConnectionCredentials: {
    // debug: function (s) {
    //     console.log(s)
    // },
    host: "ftpL.royalenfield.com",
    port: 22,
    username: "interface",
    password: "Interface@re",
    privateKey: fs.readFileSync("config/id_rsa.ppk"),
    passphrase: "Interface@re",
    readyTimeout: 99999
    // You can use a key file too, read the ssh2 documentation
  }
};
