import Joi from 'joi';

export default {
  // ================================================= USER =============================================
  //POST /node/api/users/verifyOtp
  verifyOtp: {
    body: {
      phoneNo: Joi.string().required(),
      otp: Joi.number().required()
    }
  },

  //POST /node/api/users/changePassword
  changePassword: {
    body: {
      userId: Joi.string().required(),
      OldPassword: Joi.string().required(),
      newPassword: Joi.string().required()
    }
  },

  //POST /node/api/users/forgot
  forgotPassword: {
    body: {
      phoneNo: Joi.string().required()
    }
  },

  //POST /node/api/users/reset-password-phone
  resetPassword: {
    body: {
      phoneNo: Joi.string().required(),
      password: Joi.string().required(),
      otp: Joi.string().required()
    }
  },

  //POST /node/api/users/userInterests
  userInterest: {
    body: {
      userId: Joi.string().required(),
      interests: Joi.array().required()
    }
  },

  //POST /node/api/users/updateprofileimage
  updateprofileimage: {
    body: {
      userId: Joi.string().required(),
      image: Joi.string().required()
    }
  },

  //POST /node/api/users/updatecoverimage
  updatecoverimage: {
    body: {
      userId: Joi.string().required(),
      image: Joi.string().required()
    }
  },

  //POST /node/api/users/resetPassword
  resetPassword: {
    body: {
      password: Joi.string().required()
    }
  },

  //POST /node/api/users/deletecoverimage
  deletecoverimage: {
    body: {
      userId: Joi.string().required()
    }
  },

  //POST /node/api/users/deleteprofileimage
  deleteprofileimage: {
    body: {
      userId: Joi.string().required()
    }
  },

  //GET /node/api/users/suggestedTripStories
  suggestedTripStories: {
    query: {
      jsonString: Joi.string().required()
    }
  },

  //GET /node/api/users/reviewForUser
  reviewForUser: {
    query: {
      jsonString: Joi.string().required()
    }
  },

  //GET /node/api/users/
  getUserDetails: {
    query: {
      jsonString: Joi.string().required()
    }
  },


  //POST /node/api/users/updateProfile
  updateProfile: {
    body: {
      userId: Joi.string().required()
    }
  },

  // POST /node/api/users/register
  createUser: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required(),
      phoneNo: Joi.string().required(),
      fname: Joi.string().required(),
      lname: Joi.string().required(),
      gender: Joi.string().required(),
      city: Joi.string().required(),
      dob: Joi.string().required(),
      moto: Joi.string().required(),
    }
  },
  // ================================================= USER =============================================

  //POST /node/api/booktestride/create
  createTestRide: {
    body: {
      fName: Joi.string().required,
      lName: Joi.string().required,
      email: Joi.string().required,
      bikeName: Joi.string().required,
      countryName: Joi.string().required,
      stateName: Joi.string().required,
      cityName: Joi.string().required,
      dealerName: Joi.string().required,
      Date: Joi.string().required,
      mobile: Joi.number().required,
      Date: Joi.string().required,
      dealerCode: Joi.string().required,
      buyPlanDate: Joi.string().required,
      time: Joi.string().required
    }
  },
  // POST /api/auth/login
  login: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required(),
      userType: Joi.string().required()
    }
  },

  // POST /api/bikes/register
  createBike: {
    body: {
      bikeModel: Joi.string().required(),
      bikeName: Joi.string().required(),
      bikeCategory: Joi.string().required()
    }
  },

  // POST /api/stories/create
  createTripStory: {
    body: {
      storyTitle: Joi.string().required(),
      storySummary: Joi.string().required(),
      editordata: Joi.string().required(),
      storyCategory: Joi.string().required(),
      tags: Joi.string().required(),
      postedBy: Joi.string().required(),
      postedByUserName: Joi.string().required(),
    }
  },

  // POST /api/stories/all
  getStories: {
    body: {
      limit: Joi.string().required()
    }
  },

  // POST /api/stories/
  getStory: {
    body: {
    }
  },

  // POST /api/rides/create
  createRide: {
    body: {
      rideName: Joi.string().required(),//1//1
      startLongitude: Joi.number().required(),//1//1
      startLatitude: Joi.number().required(),//1//1
      difficulty: Joi.string().required(),//1
      endPoint: Joi.string().required(),//1//1
      destination: Joi.string().required(),//1
      destinationlatitude: Joi.number().required(),
      destinationlongitude: Joi.number().required(),
      durationInDays: Joi.number().required(),//1//1
      startDate: Joi.string().required(),//1//1
      endDate: Joi.string().required(),//1//1
      terrain: Joi.string().required(),//1
      userId: Joi.string().required(),//1
      startTimeHours: Joi.string().required(),//1//1
      startTimeMins: Joi.string().required(),//1//1
      startTimeZone: Joi.string().required(),//1//1
      totalDistance: Joi.number().required(),//1//1
      fname: Joi.string().required(),//1//1
      lname: Joi.string().required(),//1//1
      gender: Joi.string().required(),//1//1
      email: Joi.string().required(),//1//1
      rideDetails: Joi.string().required(),//1//1
      // isRoyalEnfieldOwner: Joi.string().required(),
      city: Joi.string().required(),//1//1
      // bikeOwned: Joi.string().required(),
      rideCategory: Joi.string().required()//1//1
    }
  },
  // POST /api/rides/join
  joinRide: {
    body: {
      userId: Joi.string().required(),
      rideId: Joi.string().required(),
    }
  },


  // POST /api/gma/create
  gmaForm: {
    body: {
      fname: Joi.string().required(),
      lname: Joi.string().required(),
      email: Joi.string().required(),
      phone: Joi.string().required(),
      age: Joi.string().required(),
      yearOfExperience: Joi.string().required(),
      highestQualification: Joi.string().required(),
      institute: Joi.string().required(),
      secondaryQualification: Joi.string().required(),
      currentDesignation: Joi.string().required(),
      Industry: Joi.string().required(),
      functionApplyingFor: Joi.string().required(),
      keySkills: Joi.string().required(),
      linkedIn: Joi.string().required(),
    }
  },


  // POST /api/careers/create
  gmaForm: {
    body: {
      firstName: Joi.string().required(),
      lname: Joi.string().required(),
      email: Joi.string().required(),
      mobile: Joi.string().required(),
      city: Joi.string().required(),
      pinCode: Joi.string().required(),
      productSKU: Joi.string().required(),
      enquiry: Joi.string().required(),
      productEmail: Joi.string().required(),
    }
  },

  // POST /api/forum/create-forum-post
  createTopic: {
    body: {
      title: Joi.string().required(),
      category: Joi.string().required(),
      Content: Joi.string().required(),
      postedBy: Joi.string().required(),
      postedByUserName: Joi.string().required()
    }
  },
  // POST /api/rides/getridesaroundme
  // getridesaroundme: {
  //   body: {
  //     latitude: Joi.number().required(),
  //     longitude: Joi.number().required(),
  //   }
  // },


  // POST /api/rides/all
  getRides: {
    body: {
      limit: Joi.string().required()
    }
  },

  // POST /api/rides/
  getRide: {
    body: {
      rideId: Joi.string().required()
    }
  },

  sendOtp: {
    body: {
      phoneNo: Joi.string().required()
    }
  },


  getBikeDetail: {
    body: {
      phoneNo: Joi.string().required()
    }
  },

  findState: {
    body: {
      state: Joi.string().required()
    }
  },

  findCity: {
    body: {
      city: Joi.string().required()
    }
  },
  reviews: {
    body: {
      commentid: Joi.string().required(),
      userid: Joi.string().required(),
      replyBody: Joi.string().required(),
    }
  },

  subscribe: {
    body: {
      email: Joi.string().required(),
    }
  },

  ownersManual: {
    body: {
      email: Joi.string().required(),
      phone: Joi.string().required(),
      city: Joi.string().required()
    }
  }

};
